@extends('ojt_page.layout2')
@section('mhs','active')
@section('rekap','active')
@section('header')
<h1>Data Mahasiswa + Kelompoknya</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Mahasiswa</li>
</ol>
@endsection

@section('body')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

<div class="row">
  <div class="col-lg-12">

    <div class="box box-primary direct-chat direct-chat-warning">
      <div class="box-body" style="padding: 10px;overflow: scroll;">
        <b>
          <h3>Riwayat Pengajuan Kelompok</h3>
        </b>
        <table id="mahasiswa" class="table table-bordered table-hover" style="width:130%">
          <thead>
          <tr>			
            <th style="width:10%" class="cari">NIM</th>
            <th style="width:20%" class="cari">Nama Mahasiswa</th>
            <th style="width:5%" class="cari">Kelas</th>
            <th style="width:5%" class="cari">GEL</th>
            <th style="width:10%" class="cari">Nomor Kelompok</th>
            <th style="width:15%" class="cari">Pembimbing 1</th>
            <th style="width:15%" class="cari">Pembimbing 2</th>
            <th style="width:15%" class="cari">Jenis Pencarian</th>
          </tr>
          </thead>
          <tbody>
         <?php $no = 1;?>
         @foreach($mahasiswa as $m)
         @if (count($m["getdetkelompokojt"]) < 1)
         <tr>
            <td>{{$m['NIM']}}</td>
            <td>{{$m['getmhsdaft']["NAMA"]}}</td>
            <td>{{$m["KELAS"]}}</td>
            <td>{{substr($m['GEL'], -1)}}</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
         </tr>
         @else
         @foreach ($m["getdetkelompokojt"] as $det)
         <tr>
            <td>{{$m['NIM']}}</td>
            <td>{{$m['getmhsdaft']["NAMA"]}}</td>
            <td>{{$m["KELAS"]}}</td>
            <td>{{substr($m['GEL'], -1)}}</td>
            <td> {{$det["no_kelompok"]}} </td>
            <td> {{$det['getkelompokojt']['getpembimbing1']["nama"]}} </td>
            <td> {{$det['getkelompokojt']['getpembimbing2']["nama"]}} </td>
            <td>
               @if ($det['getkelompokojt']["sts_pencarian"] == 0)
                 OJT (Tempat Magang Cari Sendiri)
               @elseif($det['getkelompokojt']["sts_pencarian"] == 1)
                 OJT (Tempat Magang Dicarikan Lembaga)
               @else
                 TA
               @endif
           </td>
           
         </tr>  
         @endforeach
         @endif
         @endforeach
          </tbody>
        </table>
   
      
   
      </div>
   </div>

  </div>

  <div class="col-lg-12">

    <div class="box box-primary direct-chat direct-chat-warning">
      <div class="box-body" style="padding: 10px;overflow: scroll;">
        <b>
          <h3>Kelompok Disetujui</h3>
        </b>
        <table id="mahasiswa2" class="table table-bordered table-hover" style="width:130%">
          <thead>
          <tr>			
            <th style="width:10%" class="cari">NIM</th>
            <th style="width:20%" class="cari">Nama Mahasiswa</th>
            <th style="width:5%" class="cari">Kelas</th>
            <th style="width:5%" class="cari">GEL</th>
            <th style="width:10%" class="cari">Nomor Kelompok</th>
            <th style="width:5%" class="cari">STATUS</th>
            <th style="width:15%" class="cari">Nama Perusahaan</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($kelompok as $k)
            @foreach ($k->getdetail as $d)
            <tr>
              <td>{{$d->nim}}</td>
              <td>{{$d->getdu->getmhsdaft->NAMA}}</td>
              <td>{{$d->getdu->KELAS}}</td>
              <td>{{substr($k->no_kelompok, 5, 1)}}</td>
              <td>{{$k->no_kelompok}}</td>
              <td>
                @if ($k->sts_pencarian == 0 || $k->sts_pencarian == 1)
                  OJT
                @else
                  TA
                @endif
              </td>
              <td>{{$k->getperusahaan->nama_perusahaan}}</td>
            </tr>
            @endforeach
            @endforeach
          </tbody>
        </table>
   
      
   
      </div>
   </div>

  </div>

</div>




@endsection


@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/jszip.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.print.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
   $('#mahasiswa thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var mahasiswa = $('#mahasiswa').DataTable({
      dom: 'Bfrtip',
      buttons: [
            'csv', 'excel', 'pdf'
        ]
    });
    mahasiswa.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


    $('#mahasiswa2 thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var mahasiswa2 = $('#mahasiswa2').DataTable({
      dom: 'Bfrtip',
      orderCellsTop: true,
      buttons: [
            'csv', 'excel', 'pdf'
        ]
    });
    mahasiswa2.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

})
</script>
@endsection