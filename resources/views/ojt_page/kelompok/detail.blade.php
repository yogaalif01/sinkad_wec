@extends('ojt_page.layout2')
@section('kelompok','active')
@section('edit','active')
@section('header')
<h1>Edit Data Kelompok</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Edit</li>
</ol>
@endsection

@section('body')
<div class="row">
<div class="col-lg-12">
      
      <div class="box box-success direct-chat direct-chat-warning">
      <div class="box-body" style="padding:10px">
      <form class="form-horizontal" method="POST" action="{{url('ojt/new/kelompok/detail/simpan')}}">
      {{csrf_field()}}
      <input type="hidden" name="_method" value="POST">
      <div class="row">
         <div class="col-lg-7">

            <div class="form-group row">
            <label class="col-sm-4 form-control-label">No Kelompok</label>
            <div class="col-sm-8">
                  <input id="txtnokelompok" name="txtnokelompok" type="text" placeholder="Nomor Kelompok" class="form-control form-control-success" readonly value="{{ $kelompok["no_kelompok"] }}">
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 form-control-label">Tanggal Pengajuan</label>
            <div class="col-sm-8">
               <input id="txttglpengajuan" name="txttglpengajuan" type="text" placeholder="Tanggal Pengajuan" class="form-control form-control-warning" readonly value="{{$kelompok["created_at"]}}">
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 form-control-label">Jenis Pengajuan</label>
            <div class="col-sm-8">
               <select name="jenis_pengajuan" id="jenis_pengajuan" class="form-control form-control-warning">
                  <option value="">[ PILIH SATU ]</option>
                  <option value="0" {{$kelompok["sts_pencarian"] == 0 ? "selected" : ""}}>Cari Tempat OJT Sendiri</option>
                  <option value="1" {{$kelompok["sts_pencarian"] == 1 ? "selected" : ""}}>Tempat OJT Dicarikan Lembaga</option>
                  <option value="2" {{$kelompok["sts_pencarian"] == 2 ? "selected" : ""}}>TA</option> 
               </select>
               @if ($errors->has("jenis_pengajuan"))
                  <small class="text-danger">{{ $errors->first("jenis_pengajuan") }}</small>
               @endif
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 form-control-label"><b>Lama {{ $kelompok["sts_pencarian"] == 2 ? "TA" : "OJT" }}</b></label>
            <div class="col-sm-8">
               <input id="inputHorizontalWarning" type="number" name="lamaojt"  class="form-control" style="width:15%;float:left" value="{{ $kelompok['lama'] }}" {{ $kelompok["sts_pencarian"] == 2 ? "readonly" : "" }}> &nbsp;&nbsp;Bulan <br>
               @if ($errors->has("lamaojt"))
                  <small class="text-danger">{{ $errors->first("lamaojt") }}</small>
               @endif
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 form-control-label">Nama Perusahaan</label>
            <div class="col-sm-8">
               <select {{ $kelompok["sts_pencarian"] == 2 ? "disabled" : "" }} name="txtkdperusahaan" id="txtkdperusahaan" class="form-control form-control-warning">
                  <option value="">[ PILIH SATU ]</option>
                  @php
                      $alamatPerusahaan = "";
                  @endphp
                  @foreach ($tbperusahaan as $tbperusahaan)
                  <option value="{{$tbperusahaan['kode_perusahaan']}}" {{ $kelompok["kd_perusahaan"] == $tbperusahaan['kode_perusahaan'] ? "selected" : "" }}>{{$tbperusahaan['nama_perusahaan']}}</option>
                  @php
                        if($kelompok["kd_perusahaan"] == $tbperusahaan["kode_perusahaan"]){
                        $alamatPerusahaan = $tbperusahaan["alamat"]." ".$tbperusahaan["kota"];
                        }
                  @endphp
                  @endforeach
               </select>
               @if ($kelompok["sts_pencarian"] <> 2)
               <a href="{{url('/mahasiswa/tambah perusahaan.html')}}" class="text-info" id="btnTambahPerusahaan">+ Klik disini jika nama perusahaan yang anda inginkan tidak ada</a><br>
               @endif
               <b><div class="text-danger" id="jumlah"></div></b>
               @if ($errors->has("txtkdperusahaan"))
                  <small class="text-danger">{{ $errors->first("txtkdperusahaan") }}</small>
               @endif
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 form-control-label">Alamat Perusahaan</label>
            <div class="col-sm-8">
               <textarea id="txtalamatperusahaan" name="" class="form-control form-control-warning" readonly id="" cols="30" rows="2"> {{ $alamatPerusahaan }} </textarea>
               {{-- <input  name="txtalamatperusahaan" type="text" placeholder="Alamat Perusahaan" class="form-control form-control-warning" readonly value="kosong"> --}}
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 form-control-label">{{ $kelompok["sts_pencarian"] == 2 ? "TA" : "OJT" }} di Bulan</label>
            <div class="col-sm-8">
               <select name="txtbulan" id="txtbulan" class="form-control form-control-warning">
                  <option value="">[ PILIH SATU ]</option>
                  @php
                     $no = 1;
                  @endphp
                  @foreach ($bulan as $bulan)
                  <option value="{{ $no }}" {{ $kelompok["bulan"] == $no ? "selected" : "" }} >{{ $bulan }}</option>  
                  @php $no++; @endphp
                  @endforeach
               </select>
               @if ($errors->has("txtbulan"))
               <small class="text-danger">{{ $errors->first("txtbulan") }}</small>
            @endif
            </div>
            </div>

            <div class="form-group row">
            <label class="col-sm-4 form-control-label">Status Kelompok</label>
            <div class="col-sm-8">
               <input id="txtstatus" type="text" readonly class="form-control form-control-warning text-info" value="{{$kelompok["sts_kelompok"]}}">
            </div>
            </div>

         </div>
         <div class="col-lg-5">
            <br>
            <br>
            <div class="text-center text-primary">
            <strong><h1 id="nokelompokmu">{{ $kelompok["no_kelompok"] }}</h1></strong>
            </div>
            <strong><div style="margin-left:70px;" class="text-uppercase"> adalah nomor kelompoknya. Harap "dicatat" dan diingat baik-baik.</div></strong>
            <br><br>
            <div class="box box-danger direct-chat direct-chat-warning pull-right" style="margin:10px;width:90%">
            <div class="box-body" style="padding:5px">
               <h4>Untuk Diperhatikan</h4>
               <b style="margin-bottom:10px;">Untuk MENCARI nama perusahaan dengan benar, Tulis nama perusahaan tanpa menyertakan jenis badan usahanya. Contoh "PT. Sejahtera Raya", tulis "Sejahtera Raya".</b> <br><br>
               <b style="margin-bottom:5px;">Pastikan alamat perusahaan yang muncul sudah sesuai dengan alamat tempat anda akan melaksanakan OJT/TA.</b> <br><br>
               <b style="margin-bottom:5px;">Pastikan semua isian telah diisi dengan benar.</b>
            </div>
            </div>
            

         </div>
      </div>
            <br>
            {{-- <hr> --}}
            {{-- <p><h4>Daftar Anggota Kelompok :</h4></p> --}}
            <div class="table-responsive">                       
            <table class="table table-striped table-hover">
                  <thead>
                  <tr>
                     <th width="5%">#</th>
                     <th width="15%">NIM</th>
                     <th width="40%">NAMA</th>
                     <th width="10%">KELAS</th>
                     <th width="10%">GEL</th>
                     <th width="10%"><i>opsi</i></th>
                  </tr>
                  </thead>
                  <tbody>
                  @php
                        $no=1;
                  @endphp
                  @foreach ($kelompok["getdetail"] as $item)
                     <tr>
                        <td>{{$no}}</td>
                        <td>{{$item["nim"]}}</td>
                        <td>{{$item["getdu"]["getmhsdaft"]["NAMA"]}}</td>
                        <td>{{$item["getdu"]["KELAS"]}}</td>
                        <td>{{substr($item["getdu"]["GEL"], -1)}}</td>
                        <td>
                        @if (count($kelompok["getdetail"]) > 1)
                        <a href="{{url("/ojt/hapusanggota/".$item['nim']."/".$item["no_kelompok"])}}" class="btn btn-danger btn-sm" >hapus
                        @endif
                        </a>
                        </td>
                     </tr>
                     @php
                        $no++;
                     @endphp
                  @endforeach  
                  </tbody>
            </table>
            </div>
            <button class="btn btn-primary btn-sm" id="btntambah">TAMBAH ANGGOTA</button>
            <a href="{{ url("ojt/pengajuanBatal/{$kelompok['no_kelompok']}") }}" class="btn btn-danger btn-sm" id="btnbatal">BATALKAN PENGAJUAN</a>
            <button type="submit" class="btn btn-primary btn-sm" id="btnsimpan">SIMPAN PERUBAHAN</button>
            {{-- @if (!empty($kelompokojt[0]["kd_perusahaan"]) OR $kelompokojt[0]["sts_pencarian"] == 2) --}}

            {{-- <a href="{{ url("mahasiswa/pengajuanCetak/{$kelompokojt[0]['no_kelompok']}") }}" class="btn btn-warning btn-sm" id="btncetak" target="_blank">Cetak Formulir Pengajuan</a>
            
            <a href="{{ url("download/+berkas+PROPOSAL OJT WEC.pdf") }}" class="btn btn-warning btn-sm" id="btncetak" target="_blank">Download Proposal Pengajuan</a> --}}

            {{-- @endif --}}
            
         </form>
      

      </div>
      </div>
</div>
</div>








{{-- modal tambah anggota kelompok --}}
<div class="modal fade" id="modal_tambahanggota" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
       <div class="modal-content">
       <div class="modal-header">
           <h5 class="modal-title" id="exampleModalLabel">Tambah Anggota Kelompok</h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
           </button>
       </div>
       <div class="modal-body">
         
           <form class="form-horizontal" method="POST" action="{{url('/ojt/tambahAnggotaPost')}}">
 
             {{ csrf_field() }}
             <input type="hidden" name="_method" value="POST">
             <input type="hidden" name="no_kelompok" value="{{ $kelompok['no_kelompok'] }}">
               <div class="form-group row">
                 <label class="col-sm-4 form-control-label">NIM</label>
                 <div class="col-sm-8">
                     <input id="txtnimadd" type="text" placeholder="NIM" class="form-control form-control-success" name="txtnimadd" onkeypress="return isNumber(event)">
                     <div id="load_cek" style="margin-top:8px"></div>
                     <button class="btn btn-secondary btn-sm" id="btnCekNIM" style="margin-top:8px;">CEK</button>
                 </div>
               </div>
               <div id="data_mhs">
                 <div class="form-group row">
                   <label class="col-sm-4 form-control-label">NAMA</label>
                   <div class="col-sm-8">
                       <input id="txtnamaadd" type="text" placeholder="Nama" class="form-control form-control-success" >
                   </div>
                 </div>
                 <div class="form-group row">
                   <label class="col-sm-4 form-control-label">GELOMBANG</label>
                   <div class="col-sm-8">
                       <input id="txtgeladd" type="text" placeholder="Gelombang" class="form-control form-control-success" >
                   </div>
                 </div>
                 <div class="form-group row">
                   <label class="col-sm-4 form-control-label">KELAS</label>
                   <div class="col-sm-8">
                       <input id="txtkelasadd" type="text" placeholder="Kelas" class="form-control form-control-success"  >
                   </div>
                 </div>
                 <div class="form-group row">
                     <label class="col-sm-4 form-control-label"></label>
                     <div class="col-sm-8">
                       <div id="feedbackAdd">
                       </div>
                     </div>
                   </div>
                 <div class="form-group row">
                   <label class="col-sm-4 form-control-label"></label>
                   <div class="col-sm-8">
                     <button type="submit" class="btn btn-primary btn-sm" id="btnSimpanAdd" >TAMBAHKAN</button>
                   </div>
                 </div>
               </div>
           </form>
 
       </div>
       <div class="modal-footer">
           
   </div>
   </div>
</div>
</div>
@endsection

@section('script')

<script>
$(document).ready(function(){
   $('#txtkdperusahaan').select2();

   var base_url = "{{ url('/') }}";

   $('#data_mhs').hide();

   var sts_pemberangkatan = "{{ session('sts_pemberangkatan') }}";
   if(sts_pemberangkatan == 1){
    Swal.fire({
          title: 'Konfirmasi',
          text: 'Query Berhasil Dijalankan',
          type: 'success',
          confirmButtonText: 'OK'
      });
   }

   $('#btntambah').click(function(e){
      e.preventDefault();
      $('#modal_tambahanggota').modal();
   });

   $("#btnCekNIM").click(function(e){
      e.preventDefault();
      ceknim($("#txtnimadd").val());
   });

   $("#btnbatal").click(function(e){
      e.preventDefault()
      var url = $(this).attr("href")
      Swal.fire({
        title:"Yakin data kelompok ini akan dihapus ?",
        text:"",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        cancelButtonText:"TIDAK",
        confirmButtonText:"YA"
      }).then((result)=>{
        if(result.value){
          window.open(url);      
        }
      });
   })

   $('#txtkdperusahaan').change(function(){
      $.ajax({
            url : base_url+"/ojt/cekPerusahaan/"+$('#txtkdperusahaan').val()+"/"+$('#txtnokelompok').val(),
            type:'GET',
            success:function(r){
            // var result = JSON.parse(r);
            console.log(r);
            $('#jumlah').html(r["pesan"]);
            $('#txtalamatperusahaan').val(r["dataperusahaan"][0]["alamat"]+" "+r["dataperusahaan"][0]["kota"]);
            $("#txtupperusahaan").val(r["dataperusahaan"][0]["up"]);
            },
            error:function(e){
            console.log(e.responseText);
            }
      });
   });


   function ceknim(nim){
    $.ajax({
      url:base_url+"/ojt/tambahanggota/"+nim+"/{{$kelompok['no_kelompok']}}",
      type:'GET',
      beforeSend:function(){
        $('#load_cek').html("<div class='text-primary'>Tunggu Sebentar</div>");
      },
      success:function(r){
        console.log(r);
        var result = JSON.parse(r);
        $("#feedbackAdd").hide();
        $('#btnSimpanAdd').hide();
        $("#data_mhs").hide();
        $('#izinkanx').hide();
        if(result["status"] == "2"){
            $('#load_cek').html("<div class='text-primary'>NIM Sudah Memiliki Kelompok / NIM Salah</div>");
        }else{
            $("#feedbackAdd").show();
            $('#load_cek').html("");
            var r = JSON.parse(r);
            $("#data_mhs").show();

            $("#txtnamaadd").val(r['datamhs'][0]['NAMA']);
            $("#txtgeladd").val(r['datamhs'][0]['GEL_DAFTAR']);
            $("#txtkelasadd").val(r['datamhs'][0]['KELAS']);
            view = "<ul>";
            if(r["gel"] == 0){
            view += "<li class='text-danger'>Tidak pada gelombang yang sama.</li>"; 
            }else{
            view += "<li>Berada pada gelombang yang sama.</li>";
            }
            
            if(r["jurusan"] == 0){
            view += "<li class='text-danger'>Tidak pada jurusan yang sama.</li>";
            }else{
            view += "<li>Berada pada jurusan yang sama.</li>";
            }

            if(r["absensi"] == 0){
            view += "<li class='text-danger'>Prosentase Ketidakhadiran >= 25%.</li>";
            }else{
            view += "<li>Prosentase Ketidakhadiran < 25%.</li>";
            }

            if(r["adminis"] == 0){
            view += "<li class='text-danger'>Belum lunas administrasi dibulan ini.</li>";
            }else{
            view += "<li>Sudah melunasi administrasi bulan ini.</li>";
            }

            if(r["status"] == 0){
            view += "<li class='text-danger'>status : Tidak bisa masuk dalam kelompok</li>";
            $('#btnSimpanAdd').hide();
            $('#izinkanx').show();
            }else{
            view += "<li>status : Memenuhi syarat. (klik tambahkan)</li>";
            $('#btnSimpanAdd').show();
            }
            view += "</ul>";
            $('#feedbackAdd').html("<div class='alert alert-default'>Hasil pengecekan kami mengatakan bahwa calon anggota kelompok ini : <br><br>"+view+"</div>");
        } 
      },
      error:function(e){
        console.log(e.responseText);
        alert("Terjadi Kesalahan !");
      }
    });
  }



});



function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
   }
   return true;
}
</script>
@endsection