@extends('ojt_page.layout2')
@section('kelompok','active')
@section('edit','active')
@section('header')
<h1>
Data Pengajuan Kelompok OJT / TA
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Edit</li>
</ol>
@endsection
@section('body')
<div class="row">
<div class="col-lg-12">

   <div class="box box-primary direct-chat direct-chat-warning">
      <div class="box-body" style="padding:10px">
      
         <table class="table" id="tbkelompok">
            <thead>
              <tr>
                <th>#</th>
                <th class="cari">No Kelompok</th>
                <th class="cari">Nama Perusahaan</th>
                <th class="cari">Jumlah MHS</th>
                <th class="cari">Jenis Pengajuan</th>
                <th class="cari">Tanggal Pengajuan</th>
                <th>aksi</th>
              </tr>
            </thead>
            <tbody>
                <?php $no = 1;$jmlmhs = 0;?>
            @foreach($pengajuan as $tp)
                <tr>
                    <td>{{$no}}</td>
                    <td>{{$tp['no_kelompok']}}</td>
                    <td>{{$tp["getperusahaan"]['nama_perusahaan']}}</td>
                    <td>{{count($tp["getdetail"])}} orang</td>
                    <td>
                        @if ($tp['sts_pencarian'] == 0)
                          OJT (Tempat Magang Cari Sendiri)
                        @elseif($tp['sts_pencarian'] == 1)
                          OJT (Tempat Magang Dicarikan Lembaga)
                        @else
                          TA
                        @endif
                    </td>
                    <td>{{$tp['created_at']}}</td>
                    <td>
                        <a href="{{url('/ojt/new/kelompok/edit/detail/'.$tp['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>
                    </td>
                </tr>
                @php
                    $no++;
                    $jmlmhs = $jmlmhs + count($tp["getdetail"]);
                @endphp
                 @endforeach
            </tbody>
          </table>

          <div class="row text-danger">
              <div class="col-md-3">
                  <h4><strong>Total Jumlah Kelompok</strong></h4>
              </div>
              <div class="col-md-6">
                  <h4><strong>: {{$pengajuan->count()}}</strong></h4>
              </div>
          </div>
          <div class="row text-danger">
              <div class="col-md-3">
                 <h4><strong>Total Jumlah Mahasiswa</strong></h4>
              </div>
              <div class="col-md-6">
                  <h4><strong>: {{$jmlmhs}}</strong></h4>
              </div>
          </div>
      
      </div>
   </div>

</div>
</div>
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function(){

   $('#tbkelompok thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var tbkelompok = $('#tbkelompok').DataTable();
    tbkelompok.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    

});
</script>
@endsection