@extends('ojt_page.layout2')
@section('beranda','active')
@section('header')
<h1>Beranda</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
    {{-- <li class="active">Beranda</li> --}}
</ol>
@endsection

@section('body')
<div class="row">
    {{-- <div class="col-lg-3">
        <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{count($semua)}}</h3>
              <p>Total Pengajuan</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url("ojt/pengajuan.html")}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
    </div> --}}
    {{-- <div class="col-lg-3">
        <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$menungguAccOJT}}</h3>
              <p>Menunggu Persetujuan TIM OJT</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url("ojt/pengajuan.html")}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
    </div> --}}
    {{-- <div class="col-lg-3">
        <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$diterimaPerusahaan}}</h3>
              <p>Diterima oleh Perusahaan</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url("ojt/pemberangkatan.html")}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
    </div> --}}
    {{-- <div class="col-lg-3">
        <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$ditolakPerusahaan}}</h3>
              <p>Ditolak oleh Perusahaan</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url("ojt/pengajuan.html")}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
    </div> --}}
    {{-- <div class="col-lg-3">
        <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$setPembimbing}}</h3>
              <p>Sudah mendapatkan pembimbing</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url("ojt/pemberangkatan.html")}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
    </div> --}}
    {{-- <div class="col-lg-3">
        <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$berangkatOJT}}</h3>
              <p>Melaksanakan OJT</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url("ojt/kelompok fix.html")}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
    </div> --}}
    <!-- <div class="col-lg-3">
        <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$pulang}}</h3>
              <p>Selesai OJT</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url("ojt/kelompok fix.html")}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
    </div> -->
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary direct-chat direct-chat-warning">
      <div class="box-body" style="padding: 10px;overflow-x:scroll">
         <table id="kelompok" class="table table-bordered" style="width:120%">
            <thead>
              <tr>
                <th>aksi</th>			
                <th class="cari">No Kelompok</th>
                <th class="cari">Nama Perusahaan</th>
                <th class="cari">Status Kelompok</th>
                <th class="cari">Jenis Pengajuan</th>
              </tr>
            </thead>
            <tbody>
            <?php $no = 1?>
            @foreach($semua as $s)
              <tr>
                  <td width="28%">
                    <div class="btn-group">
                      <a target="_blank" class="btn btn-primary btn-sm" href="{{url('/ojt/all/'.$s->no_kelompok)}}">
                        Detail
                      </a>
                      <a class="btn btn-danger btn-sm" onclick="ubah('{{$s->no_kelompok}}')">
                        Ubah Ke TA
                      </a>
                      <a class="btn btn-default btn-sm" href=" {{url("ojt/surat+pembatalan/".$s->no_kelompok)}} " target="_blank">
                        Srt Pembatalan
                      </a>
                      <a class="btn btn-danger btn-sm" onclick="hapus('{{$s->no_kelompok}}')">
                        hapus
                      </a>
                    </div>
                  </td>
                  <td width="10%">{{$s->no_kelompok}}</td>
                  <td width="30%">{{$s->getperusahaan == null ? "" : $s->getperusahaan->nama_perusahaan}}</td>
                  <td width="20%">{{$s->sts_kelompok}}</td>
                  <td width="20%">
                      @if ($s->sts_pencarian == 0)
                         OJT (Tempat Magang Cari Sendiri)
                      @elseif($s->sts_pencarian == 1)
                         OJT (Tempat Magang Dicarikan Lembaga)
                      @else
                         TA
                      @endif
                  </td>
              </tr>
            @endforeach
            </tbody>
          </table>
      </div>
   </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function(){
$('#kelompok thead .cari').each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
} );
var kelompok = $("#kelompok").DataTable({
  scrollX:        true,
  scrollCollapse: true,
  paging:         true,
  fixedColumns:   {
      leftColumns: 1
  }
})

kelompok.columns().every( function () {
    var that = this;
    $( 'input', this.header() ).on( 'keyup change clear', function () {
        if ( that.search() !== this.value ) {
            that
                .search( this.value )
                .draw();
        }
    } );
});

});



function ubah(nokelompok){
  Swal.fire({
      title:"Anda yakin ingin merubah kelompok ini menjadi TA ?",
      text:"",
      icon:"warning",
      showCancelButton:true,
      confirmButtonColor:"#3085d6",
      cancelButtonColor:"#d33",
      cancelButtonText:"TIDAK",
      confirmButtonText:"YA"
    }).then((result)=>{
      if(result.value){
        $.get("{{url('ubahketa')}}/"+nokelompok, function(r){
          Swal.fire({
            title: 'Konfirmasi',
            text: 'Query Berhasil Dijalankan',
            type: 'success',
            confirmButtonText: 'OK'
          });
          window.location.reload(1);
        })      
      }
    });
}

function hapus(nokelompok){
  Swal.fire({
      title:"Anda yakin ingin menghapus data kelompok ini ? [data yang sudah terhapus tidak dapat dikembalikan lagi]",
      text:"",
      icon:"warning",
      showCancelButton:true,
      confirmButtonColor:"#3085d6",
      cancelButtonColor:"#d33",
      cancelButtonText:"TIDAK",
      confirmButtonText:"YA"
    }).then((result)=>{
      if(result.value){
        $.get("{{url('ojt/hapuskelompok')}}/"+nokelompok, function(r){
          Swal.fire({
            title: 'Konfirmasi',
            text: 'Query Berhasil Dijalankan',
            type: 'success',
            confirmButtonText: 'OK'
          });
          window.location.reload(1);
        })      
      }
    });
}
</script>
@endsection