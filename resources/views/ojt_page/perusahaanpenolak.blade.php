@extends('ojt_page.layout2')
@section('perusahaan2','active')
@section('rekap','active')
@section('header')
<h1>Data Perusahaan Menolak OJT</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Perusahaan menolak</li>
</ol>
@endsection

@section('body')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">


<div class="row">
   <div class="col-lg-12">
 
     <div class="box box-primary direct-chat direct-chat-warning">
       <div class="box-body" style="padding: 10px;">
         <table id="perusahaan" class="table table-bordered table-hover">
           <thead>
           <tr>			
             <th style="width:10%" class="cari">Kode</th>
             <th style="width:15%" class="cari">Nama Perusahaan</th>
             <th style="width:15%" class="cari">Alamat</th>
             <th style="width:8%" class="cari">Kota</th>
             <th style="width:10%" class="cari">Telp</th>
             <th style="width:15%" class="cari">Penghubung</th>
           </tr>
           </thead>
           <tbody>
          @foreach($dperusahaan as $p)
          <tr>
             <td> {{$p->kode_perusahaan}} </td>
             <td> {{$p->nama_perusahaan}} </td>
             <td> {{$p->alamat}} </td>
             <td> {{$p->kota}} </td>
             <td> {{$p->telp}} </td>
             <td> {{$p->nama_Penghubung}} {{$p->telp_Penghubung <> "" ? "( ".$p->telp_Penghubung." )" : ""}} </td>
          </tr>
          @endforeach
           </tbody>
         </table>
    
       
    
       </div>
    </div>
 
   </div>
 
 </div>
 


@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/jszip.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.print.min.js')}}"></script>

<script>
$(document).ready(function(){

   $('#perusahaan thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var perusahaan = $('#perusahaan').DataTable({
      dom: 'Bfrtip',
      buttons: [
            'csv', 'excel', 'pdf'
        ]
    });
    perusahaan.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

})
</script>
@endsection