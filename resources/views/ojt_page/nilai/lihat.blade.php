@extends('ojt_page.layout2')
@section('nilai', 'active')
@section('lihat-nilai', 'active')
@section('header')
<h1>Nilai OJT/TA</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Nilai OJT/TA</li>
</ol>
@endsection

@section('body')
<div class="row">

<div class="col-lg-12">
    <div class="box box-primary" >
        <div class="box-header">
          <h5 class="text-bold">Data Nilai Per Kelompok</h5>
        </div>
        <div class="box-body" style="overflow: scroll">
            <table class="table table-responsive table-bordered table-striped" width="101%" id="tbnilai">
                <thead>
                    <tr>
                        <th style="width:5%">NO KELOMPOK</th>
                        <th style="width:5%">NAMA PERUSAHAAN</th>
                        <th style="width:5%">JUDUL LAPORAN</th>
                        <th style="width:2%" class="cari">PEMBIMBING 1</th>
                        <th style="width:2%" class="cari">PEMBIMBING 2</th>
                        <th style="width:10%" class="cari">NIM</th>
                        <th style="width:25%" class="cari">NAMA MAHASISWA</th>
                        <th style="width:8%" class="cari">KELAS</th>
                        <th style="width:8%" class="cari">GEL</th>
                        <th style="width:5%" class="cari">NA</th>
                        <th style="width:10%;display:none" >NO KELOMPOK</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($kelompokojt as $k)
                    @foreach ($k->getdetail as $d)
                    <tr>
                        <td style="widtd:5%">{{$k->no_kelompok}} {{$k->sts_pencarian == 2 ? "( TA )" : " ( OJT ) "}}</td>
                        <td style="width:5%">{{$k->sts_pencarian == 2 ? "Wearnes Education Center" : $k->getperusahaan->nama_perusahaan}}</td>
                        <td style="width:5%">{{$k->gettbkelompok->judul_lap}}</td>
                        <td style="widtd:2%">{{$k->getpembimbing1['nama']}}</td>
                        <td style="widtd:2%">{{$k->getpembimbing2['nama']}}</td>
                        <td style="width:10%">{{$d->nim}}</td>
                        <td style="width:20%">{{$d->getdu->getmhsdaft->NAMA}}</td>
                        <td style="width:8%">{{$d->getdu->KELAS}}</td>
                        <td style="width:8%">{{$d->getdu->GEL}}</td>
                        <td style="width:8%">{{count($d->getdu->getnilaiOJTA) < 1 ? "" : $d->getdu->getnilaiOJTA[0]["nilaiakhir"] }}</td>
                        <td style="width:8%;display:none">{{$k->no_kelompok}}</td>
                    </td>
                    @endforeach    
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/buttons.dataTables.min.css')}}">

@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/jszip.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/buttons.print.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    var groupColumn = 0;
    $('#tbnilai thead .cari').each( function () {
        var title = $(this).text()
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%;font-size:10px;" class="form-control txtCari" placeholder="'+title+'" />' )
    });

    var tbnilaix = $("#tbnilai").DataTable({
        "columnDefs": [
            { 
                "visible": false, "targets": [groupColumn, 1, 2] 
            },
        ],
        "dom": 'Bfrtip',
        "buttons": [
                'csv', 'excel', 'pdf'
            ],
        "paging":true,
        "order": [[ groupColumn, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            var subTotal = new Array();
            var groupID = -1;
            var aData = new Array();
            var index = 0;

            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {

              var vals = api.row(api.row($(rows).eq(i)).index()).data();
              var judul = vals[2];
              var perusahaan = vals[1];

                
              if ( last !== group ) {
                  $(rows).eq( i ).before(
                      '<tr class="group" style="background-color:#1890db;"><td style="border:0;"><strong>'+group+'<br>'+perusahaan+'</strong></td><td colspan="6" style="border:0;">Judul Laporan : <strong>'+judul+'</strong></td></tr>'
                  );
                  last = group;
              }
            });

        }
    })

    tbnilaix.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
})
</script>
@endsection
