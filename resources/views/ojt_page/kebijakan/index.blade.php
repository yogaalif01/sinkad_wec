@extends('ojt_page.layout2')
@section('kebijakan', 'active')
@section('pengaturan', 'active')
@section('header')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<h1>Kebijakan</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Kebijakan</li>
</ol>
@endsection

@section('body')
<div class="row">
<div class="col-lg-12">

    <div class="box box-primary">
        <div class="box-body" style="overflow-x:scroll">
            <a href="{{ url("/ojt/kebijakan/tambah.html") }}" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> Tambah Data</a>
            <br>
            <br>
            <table width="100%" id="tbkebijakan" class="table table-responsive table-bordered table-striped">
                <thead>
                <tr>
                    <th width="8%">#</th>
                    <th width="15%">NIM</th>
                    <th width="25%">Nama</th>
                    <th width="10%">Kelas</th>
                    <th width="15%">Jenis Kebijakan</th>
                    <th width="15%">Batas Akhir</th>
                    <th width="20%">aksi</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $no = 1;
                    
                @endphp
                @foreach ($kebijakan as $itm)
             
                <tr id="{{ $itm->id }}">
                    <td>{{ $no++ }}</td>
                    <td>{{ $itm->getdu->NIM }}</td>
                    <td>{{ $itm->getmhsdaft->NAMA }}</td>
                    <td>{{ $itm->getdu->KELAS }}</td>
                    <td>
                       @php
                           switch ($itm->jenis_kebijakan) {
                               case 1:
                                   echo "Kebijakan administrasi";
                                   break;
                                case 2:
                                   echo "Kebijakan nilai";
                                   break;
                                case 3:
                                   echo "Kebijakan absensi";
                                   break;
                               
                               default:
                                   echo "undefined";
                                   break;
                           }
                       @endphp
                    </td>
                    <td>{{ date("d F Y", strtotime($itm->bts_kebijakan)) }}</td>
                    <td>
                        <a href="{{ url("/ojt/kebijakan/edit-".$itm->id.".html") }}" class="btn btn-info btn-sm"> <i class="fa fa-pencil"></i> </a>
                        <button class="btn btn-danger btn-sm" onclick="deletex({{ $itm->id }})"> <i class="fa fa-trash"></i> </button>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#tbkebijakan").DataTable();

    var stsKebijakan = "{{ session()->get('stsKebijakan') }}";
    if(stsKebijakan == 1){
        Swal.fire(
        'Konfirmasi',
        'Query berhasil dijalankan',
        'success'
        )
        sessionStorage.removeItem("stsKebijakan");
    }

});

function deletex(id){
    
    Swal.fire({
        title:"apakah anda yakin ?",
        text:"data yang sudah dihapus tidak bisa dikembalikan lagi.",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        confirmButtonText:"Ya, saya yakin"
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url:"{{url('ojt/kebijakan/delete-')}}"+id,
                type:"DELETE",
                success:function(r){
                    var result = JSON.parse(r);
                    // console.log(result);
                    if(result["sts"] == 1){
                        $("#"+id).hide();
                        Swal.fire(
                            "Berhasil !","Query berhasil dijalankan","success"
                        );
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            })
        }
    });
}
</script>

@endsection
