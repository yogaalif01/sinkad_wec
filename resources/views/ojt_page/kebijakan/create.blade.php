@extends('ojt_page.layout2')
@section('kebijakan','active')
@section('pengaturan', 'active')
@section('header')
<h1>Kebijakan</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="{{url('/ojt/kebijakan/data.html')}}"><i class="fa fa-dashboard"></i> Kebijakan</a></li>
    <li class="active">Tambah</li>
</ol>
@endsection

@section('body')

<div class="row">
<div class="col-lg-8">

    <div class="box box-primary">
    
    <div class="box-body">
    <p class="text-primary">*Isikan data dengan benar</p>
    <form action="{{ url('/ojt/kebijakan/store') }}" method="POST">
    {{  csrf_field() }}
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_method" value="POST">
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">NIM</label>
        <div class="col-sm-8">
            <select name="npm" id="npm" class="form-control">
                <option value="">- Pilih NIM -</option>
                @foreach ($du as $du)
                <option value="{{ $du->NPM }}" {{ old("npm") == $du->NPM ? "selected" : "" }}>{{ $du->NIM }}</option>
                @endforeach
            </select>
            @if ($errors->has("npm"))
                <small class="text-danger">{{ $errors->first("npm") }}</small>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Jenis Kebijakan</label>
        <div class="col-sm-8">
            <select name="jenis" class="form-control">
                <option value="">- Pilih Jenis Kebijakan -</option>
                @php
                    $list = ["1"=>"Kebijakan Administrasi", "2"=>"Kebijakan Nilai", "3"=>"Kebijakan Absensi"];
                @endphp
                @for ($i = 1; $i <= 3; $i++)
                <option value="{{ $i }}" {{ old("npm") == $i ? "selected" : "" }}>{{ $list[$i] }}</option>
                @endfor
                
            </select>
            @if ($errors->has("jenis"))
                <small class="text-danger">{{ $errors->first("jenis") }}</small>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Batas Waktu</label>
        <div class="col-sm-8">
            <input type="text" name="batas" class="form-control" id="batas" value="{{ old("batas") }}">
            @if ($errors->has("batas"))
                <small class="text-danger">{{ $errors->first("batas") }}</small>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Keterangan</label>
        <div class="col-sm-8">
            <textarea name="keterangan" id="" cols="30" rows="5" class="form-control">{{ old("keterangan") }}</textarea>
            @if ($errors->has("keterangan"))
                <small class="text-danger">{{ $errors->first("keterangan") }}</small>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-4 col-form-label"></label>
        <div class="col-sm-8">
           <button type="submit" class="btn btn-primary btn-sm" > <i class="fa fa-save"></i> SIMPAN</button>
           <a href="{{ url("/ojt/kebijakan/data.html") }}" class="btn btn-danger btn-sm"><i class="fa fa-mail-reply"></i> Batal</a>
        </div>
    </div>
    </div>
    </div>

</div>
</div>

@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/select2/dist/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script>
$(document).ready(function(){
    $("#npm").select2();
    $("#batas").datepicker({
        autoclose:true,
        format:"yyyy-mm-dd"
    });
})
</script>
@endsection