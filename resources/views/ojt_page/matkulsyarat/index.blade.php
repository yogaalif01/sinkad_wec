@extends('ojt_page.layout2')
@section('matkulojt', 'active')
@section('pengaturan', 'active')
@section('header')
<h1>Matakuliah Syarat OJT</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Matkul Syarat</li>
</ol>
@endsection

@section('body')
<meta name="csrf-token" content="{{ csrf_token() }}" />
{{-- @php
    dd($tbmatkul);
@endphp --}}
<div class="row">
@foreach ($tb_jurusan as $item)
<div class="col-lg-6">

    <div class="box box-success" style="height:370px;">
        <div class="box-header">
          <h5 class="text-bold">{{ $item->jurusan }}</h5>
        </div>
        <div class="box-body chat" id="chat-box">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Pilih Mata Kuliah</label>
                        <select multiple class="form-control" style="height:250px;" id="{{ "combo".$item->KD_JURUSAN }}">
                            @foreach ($tbmatkul as $itmMatkul)
                            @if ($itmMatkul['kd_jurusan'] == $item->KD_JURUSAN)
                            <option value="{{ $itmMatkul['kd_jurusan']."/".$itmMatkul['kd_matkul'] }}">{{ $itmMatkul['matakuliah'] }}</option>
                            @endif
                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="col-md-1">
                    <div style="margin-top:40px;">
                         <button class="btn btn-primary " style="margin-bottom:10px;" onclick="simpan('{{ $item->KD_JURUSAN }}')"> > </button>
                         <button class="btn btn-primary" style="margin-bottom:10px;" onclick="hapus('{{ $item->KD_JURUSAN }}')"> < </button>
                    </div>
                </div>
                <div class="col-md-5" style="padding-left:30px;">
                    <div class="form-group">
                        <label>Mata Kuliah Syarat OJT</label>
                        <select multiple class="form-control" style="height:250px;" id="{{ "combox".$item->KD_JURUSAN }}">
                            @foreach ($matkulojt as $itmMatkulOjt)
                            @if ($itmMatkulOjt->kd_jurusan == $item->KD_JURUSAN)
                            <option value="{{ $itmMatkulOjt['kd_jurusan']."/".$itmMatkulOjt['kd_matkul'] }}">{{ $itmMatkulOjt->getmatkul->matakuliah }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" >
                </div>
            </div>
        </div>
        <!-- /.chat -->
      </div>

</div>
@endforeach
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){

$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});


});

function simpan(KD_JURUSAN){
    var data = $("#combo"+KD_JURUSAN).val();
    if(data != ""){
        $.ajax({
            url:"{{ url('/ojt/matkul+syarat/add') }}",
            type:"POST",
            data:{data:data},
            success:function(r){
                console.log(r);
                window.location.reload(1);
            },
            error:function(e){
                console.log(e.responseText);
                alert("Terjadi Kesalahan !");
            }
        })
    }
}

function hapus(KD_JURUSAN){
    var data = $("#combox"+KD_JURUSAN).val();
    if(data != ""){
        $.ajax({
            url:"{{ url('/ojt/matkul+syarat/hapus') }}",
            type:"POST",
            data:{data:data},
            success:function(r){
                console.log(r);
                window.location.reload(1);
            },
            error:function(e){
                console.log(e.responseText);
                alert("Terjadi Kesalahan !");
            }
        })
    }
}
</script>
@endsection
