<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pembatalan</title>

      <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
      <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('lte2/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <style>
    body{
     /* color:blue;    */
    }
    .bold{
        font-weight: bold !important;
    }

    #body{
       margin:0px 70px;
    }

    @page {
    header: page-header;
    footer: page-footer;
    }

    #datamhs td, th {
        border:1px solid black;
        padding-left: 5px;
        font-size: 12px;
    }

    
    
    </style>
</head>
<body>

<htmlpageheader name="page-header">
   <img class="img img-responsive" style="padding-top:30px;width:100%;" src="{{ url("storage/app/berkas/header-surat.png") }}" alt="" srcset="">
</htmlpageheader>
<htmlpagefooter name="page-footer">
   <img class="img img-responsive" style="padding-bottom:30px;width:100%;" src="{{ url("storage/app/berkas/footer-surat.png") }}" alt="" srcset="">
</htmlpagefooter>

@php
    $bulanIndo = ["Januari", "Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
@endphp
<div id="body">
<div class="pull-right" style="width:100%;text-align: right">Malang, {{ date("d")." ".$bulanIndo[intval(date("m")-1)]." ".date("Y") }}</div>
<table style="width:60%">
   @php
      $kampus = substr(session("th_ajaran"), -3) == "roi" ? "ROI" : "WEC";
   @endphp
   <tr>
      <td style="vertical-align: top">No</td>
      <td style="vertical-align: top">:</td>
      <td>&nbsp; {{ $kelompok->no_kelompok."/".$kampus."/".$kelompok->kd_perusahaan."/OJT/".$bulan."/".date("Y") }} </td>
  </tr> 
   <tr>
        <td style="vertical-align: top">Perihal</td>
        <td style="vertical-align: top">:</td>
        <td>&nbsp; <span class="bold"> Permohonan Pembatalan OJT </span></td>
    </tr>
</table>
<br>
<br>

Kepada <br>
Yth. Bapak/Ibu Pimpinan<br>
{{ $kelompok["getperusahaan"]["nama_perusahaan"] }} <br>
{!! $kelompok["getperusahaan"]["up"] <> null ? "u.p. ".$kelompok["getperusahaan"]["up"]."<br>" : ""  !!}
{{ $kelompok["getperusahaan"]["alamat"] }} <br>
<span style="text-transform:uppercase">{{ $kelompok["getperusahaan"]["kota"] }} </span>
<br><br>

Dengan Hormat, 
<br><br>
<p style="text-align: justify">
Sehubungan dengan surat kami tentang permohonan OJT pada perusahaan yang Bapak/Ibu pimpin, sebelumnya kami mengucapkan terima kasih atas kesempatan yang telah diberikan kepada mahasiswa/wi Wearnes Education Center  untuk melaksanakan OJT (On the Job Training) di {{ $kelompok["getperusahaan"]["nama_perusahaan"] }} dan semoga kerjasama yang telah terjalin selama ini  bermanfaat bagi upaya peningkatan kualitas sumber daya manusia Indonesia di era Globalisasi.
</p>

<p style="text-align: justify">
Dengan semakin Masiv nya Pandemi Covid-19 dan menindak lanjuti instruksi Pemerintah untuk menekan Penyebaran Virus Covid-19  dengan melakukan Social Distancing dengan Bekerja serta belajar di rumah, maka sehubungan dengan hal tersebut diatas, kami bermaksud untuk melakukan Pembatalan pelaksanakan OJT (On The Job Training), yang telah atau akan mendapatkan Persetujuan untuk melaksanakan OJT (On The Job Training) pada Bulan <span class="bold"> {{ $kelompok["lama"] > 1 ? $bulanIndo[$kelompok["bulan"]-1]." - ".$bulanIndo[($kelompok["bulan"]+($kelompok["lama"]-1))-1] : $bulanIndo[$kelompok["bulan"]-1] }} tahun {{ date("Y") }}  </span> di {{ $kelompok["getperusahaan"]["nama_perusahaan"] }} Adapaun Mahasiswa/wi tersebut adalah sebagi berikut : 
</p>

<table id="datamhs" style="width:100%" cellspacing="0">
   <tr style="background-color: #cbd0d6">
       <th style="width:20%;text-align: center">NIM</th>
       <th style="text-align: center">NAMA MAHASISWA</th>
       <th style="text-align: center">JURUSAN</th>
   </tr>
   @php
       $no = 1;
   @endphp
   @foreach ($kelompok["getdetail"] as $itm)
   <tr>
       <td> {{ $itm['nim'] }} </td>
       <td> {{ $itm['getdu']["getmhsdaft"]["NAMA"] }} </td>
       <td> KOMPUTER APLIKASI BISNIS PERKANTORAN EKSPOR IMPOR </td>
   </tr>
   @endforeach
   
</table>

<br>

<p style="text-align: justify">
Demikianlah Surat permohonan ini kami sampaikan sebagai informasi dan permohonan maaf, selanjutnya atas kesempatan dan kebijaksanaan yang Bapak/Ibu berikan kami ucapkan terima kasih.
</p>

<br><br>
<img class="img pull-right" style="padding-top:10px;width:45%;" src="{{ url("storage/app/berkas/ttd-pakhor.png") }}" alt="" srcset="">

</body>
</html>