@extends('ojt_page.layout2')
@section('kelompokfix','active')
@section('rekap','active')
@section('header')
<h1>Kelompok OJT</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Kelompok OJT</li>
</ol>
@endsection
@section('body')

<div class="box box-primary direct-chat direct-chat-warning">
        <h4 class="box-header"> <strong>Data Kelompok Yang Sedang OJT</strong></h4>
    <div class="box-body" style="padding: 10px;">
     <table id="kelompok" class="table table-bordered table-hover">
       <thead>
       <tr>
         <th>#</th>			
         <th>No Kelompok</th>
         <th>Nama Perusahaan</th>
         <th>Jumlah MHS</th>
         <th>Pembimbing</th>
         <th>Tanggal Pengajuan</th>
         <th>aksi</th>
       </tr>
       </thead>
       <tbody>
           <?php 
            $no = 1;
            $jmlmhs = 0;
            ?>
     @foreach($kelompok as $itm)
         <tr>
             <td>{{$no++}}</td>
             <td>{{$itm['no_kelompok']}}</td>
             <td>{{$itm["getperusahaan"]['nama_perusahaan']}}</td>
             <td>{{count($itm['getdetail'])}} orang</td>
             <td>{{$itm["getpembimbing1"]['nama']}}</td>
             <td>{{$itm['created_at']}}</td>
             <td>
                 <a href="{{url('/ojt/kelompok/'.$itm['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>
             </td>
         </tr>
         @php
             $jmlmhs = $jmlmhs + count($itm['getdetail']);
         @endphp
           @endforeach
       </tbody>
     </table>
     
    <div class="row text-danger">
        <div class="col-md-3">
            <h4><strong>Total Jumlah Kelompok</strong></h4>
        </div>
        <div class="col-md-6">
            <h4><strong>: {{$kelompok->count()}}</strong></h4>
        </div>
    </div>
    <div class="row text-danger">
        <div class="col-md-3">
           <h4><strong>Total Jumlah Mahasiswa</strong></h4>
        </div>
        <div class="col-md-6">
            <h4><strong>: {{$jmlmhs}}</strong></h4>
        </div>
    </div>

     

   </div>
</div>


<div class="box box-primary direct-chat direct-chat-warning">
    <h4 class="box-header"> <strong>Data Kelompok Yang Sudah Menyelesaikan OJT</strong></h4>
    <div class="box-body" style="padding: 10px;">
        <table id="kelompok2" class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>			
            <th>No Kelompok</th>
            <th>Nama Perusahaan</th>
            <th>Jumlah MHS</th>
            <th>Pembimbing</th>
            <th>Tanggal Pengajuan</th>
            <th>aksi</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            $no = 1;
            $jmlmhs = 0;
            ?>
        @foreach($kelompok2 as $itm)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$itm['no_kelompok']}}</td>
                <td>{{$itm["getperusahaan"]['nama_perusahaan']}}</td>
                <td>{{count($itm['getdetail'])}} orang</td>
                <td>{{$itm["getpembimbing1"]['nama']}}</td>
                <td>{{$itm['created_at']}}</td>
                <td>
                    <a href="{{url('/ojt/kelompok/'.$itm['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>
                </td>
            </tr>
            @php
                $jmlmhs = $jmlmhs + count($itm['getdetail']);
            @endphp
            @endforeach
        </tbody>
        </table>

        <div class="row text-danger">
            <div class="col-md-3">
                <h4><strong>Total Jumlah Kelompok</strong></h4>
            </div>
            <div class="col-md-6">
                <h4><strong>: {{$kelompok2->count()}}</strong></h4>
            </div>
        </div>
        <div class="row text-danger">
            <div class="col-md-3">
               <h4><strong>Total Jumlah Mahasiswa</strong></h4>
            </div>
            <div class="col-md-6">
                <h4><strong>: {{$jmlmhs}}</strong></h4>
            </div>
        </div>

    </div>
</div>

@endsection
@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#kelompok').DataTable();
    $('#kelompok2').DataTable();

    var sts_kelompok = "{{ session('sts_kelompok') }}";
    if(sts_kelompok == 1){
        Swal.fire({
            title: 'Konfirmasi',
            text: 'Query Berhasil Dijalankan',
            type: 'success',
            confirmButtonText: 'OK'
        });
    }

});
</script>
@endsection