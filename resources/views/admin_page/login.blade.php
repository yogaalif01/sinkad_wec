<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminSINKAD | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('lte2/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('lte2/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('lte2/dist/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('lte2/plugins/iCheck/square/blue.css')}}">
  <link rel="stylesheet" href="{{asset('swal/sweetalert2.css')}}">
  <link rel="shortcut icon" href="{{asset('images/head logo.png')}}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<input type="hidden" id="sts_login" data-value="{{Session::get("sts_login")}}" />

<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Admin</b>SINKAD</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="{{url('admin/loginPost')}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="POST">
        <div class="form-group has-feedback">
            <select name="slc_th" id="slc_th" class="form-control">
                <option value="">[ PILIH TAHUN AJARAN ]</option>
                <option value="2019">2019 - WEC</option>
                <option value="2019roi">2019 - ROI</option>
                <option value="2020">2020 - WEC</option>
                <option value="2020roi">2020 - ROI</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
            </select>
            @if ($errors->has("slc_th"))
                <small class="text-danger">{{ $errors->first("slc_th") }}</small>
            @endif
        </div>
        <div class="form-group has-feedback">
            <select name="slc_admin" id="slc_admin" class="form-control">
                <option value="">[ PILIH ADMIN ]</option>
                <option value="fo">FO</option>
                <option value="ecc">ECC Admin</option>
                <option value="ojt">OJT Admin</option>
                <option value="pengajar">Dosen / Asisten</option>
            </select>
            @if ($errors->has("slc_admin"))
                <small class="text-danger">{{ $errors->first("slc_admin") }}</small>
            @endif
        </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        @if ($errors->has("username"))
                <small class="text-danger">{{ $errors->first("username") }}</small>
            @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has("password"))
            <small class="text-danger">{{ $errors->first("password") }}</small>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">M A S U K</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


    <!-- /.social-auth-links -->



  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{asset('lte2/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('lte2/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('lte2/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('swal/sweetalert2.js')}}"></script>
<script>
  $(function () {

    if($('#sts_login').data("value") == "0"){
        Swal.fire({
            title: 'Konfirmasi',
            text: 'Gagal Login | Tidak ada akses',
            type: 'error',
            confirmButtonText: 'OK'
        });
    }

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
