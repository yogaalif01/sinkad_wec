@extends('pengajar_page.layout2')
@section('wisuda','active')
@section('wisuda-bayar','active')
@section('header')
<h1>Pembayaran</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/pengajar')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="{{url('/pengajar/wisuda/data.html')}}"><i class="fa fa-dashboard"></i> Wisuda</a></li>
    <li class="active">Edit</li>
</ol>
@endsection

@section('body')

<div class="row">
    <div class="col-lg-8">
    
        <div class="box box-primary">
        
        <div class="box-body">
        <p class="text-primary">*Isikan data dengan benar</p>
        <form action="{{ url('/pengajar/wisuda/update/'.$wisuda->id) }}" method="POST">
        {{  csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">NIM</label>
            <div class="col-sm-8">
                <select name="npm" id="npm" class="form-control">
                    <option value="">- Pilih NIM -</option>
                    @php
                    $npm = old("npm") <> "" ? old("npm") : $wisuda->NPM;
                    @endphp
                  @foreach ($du as $du)
                <option value="{{ $du->NPM }}" {{ $npm == $du->NPM ? "selected" : "" }}>{{ $du->NIM }}</option>
                @endforeach
                </select>
                @if ($errors->has("npm"))
                    <small class="text-danger">{{ $errors->first("npm") }}</small>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Jumlah Bayar</label>
            <div class="col-sm-8">
                <input type="text" name="jmlbayar" id="" class="form-control" value=" {{ number_format(env("BIAYA_WISUDA")) }} " disabled>
                @if ($errors->has("jenis"))
                    <small class="text-danger">{{ $errors->first("jenis") }}</small>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Tanggal Bayar</label>
            <div class="col-sm-8">
                <input type="text" name="tglbayar" autocomplete="off" class="form-control" id="batas" value="{{ old("tglbayar") <> "" ? old("tglbayar") : $wisuda->tgl_bayar }}">
                @if ($errors->has("batas"))
                    <small class="text-danger">{{ $errors->first("batas") }}</small>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 col-form-label"></label>
            <div class="col-sm-8">
               <button type="submit" class="btn btn-primary btn-sm" > <i class="fa fa-save"></i> SIMPAN</button>
               <a href="{{ url("/pengajar/wisuda/bayar.html") }}" class="btn btn-danger btn-sm"><i class="fa fa-mail-reply"></i> Batal</a>
            </div>
        </div>
        </div>
        </div>
    
    </div>
    </div>
    

@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/select2/dist/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>

<script>
$(document).ready(function(){
    $("#npm").select2();
    $("#batas").datepicker({
        autoclose:true,
        format:"yyyy-mm-dd"
    });
})
</script>
@endsection