@extends('pengajar_page.layout2')
@section('wisuda','active')
@section('wisuda-setting','active')
@section('header')
<h1>Pengaturan</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/pengajar')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="{{url('/pengajar/wisuda/setting.html')}}"><i class="fa fa-dashboard"></i> Wisuda</a></li>
    <li class="active">Pengaturan</li>
</ol>
@endsection

@section('body')
<div class="row">
    <div class="col-lg-8">
    
        <div class="box box-primary">
        <div class="box-body">
            <form action="{{ url('/pengajar/wisuda/updatesetting') }}" method="POST">
                {{  csrf_field() }}
                <input type="hidden" name="_method" value="POST">
            <div class="form-group row">
            <label class="col-sm-4 col-form-label">Biaya Wisuda</label>
            <div class="col-sm-8">
                <input type="text" name="biaya" id="" class="form-control" value=" {{ number_format(env("BIAYA_WISUDA")) }} " onkeypress="return isNumber(event)">
                @if ($errors->has("biaya"))
                    <small class="text-danger">{{ $errors->first("biaya") }}</small>
                @endif
            </div>
            </div>
            <div class="form-group row">
            <label class="col-sm-4 col-form-label"></label>
            <div class="col-sm-8">
                <button class="btn btn-primary btn-sm" type="submit">SIMPAN</button>
            </div>
            </div>
            </form>
        </div>
        </div>

@endsection
@section('script')
<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
@endsection