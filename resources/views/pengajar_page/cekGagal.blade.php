@extends('pengajar_page.layout2')
@section('pemberangkatan', 'active')
{{-- @section('statusKelompok','active') --}}
@section('header')
<h1>
Konfirmasi Pengecekan Kelompok
</h1>
{{-- <ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="{{ $kelompok[0]["sts_pencarian"] == 2 ? url('/ojt/Data TA.html') : url('/ojt/kelompok fix.html') }}">Data Kelompok {{$kelompok[0]["sts_pencarian"] == 2 ? "TA" : "OJT" }}</a></li>
    <li class="active">Detail Kelompok</li>
</ol> --}}
@endsection
@section('body')
<div class="row">
<div class="col-lg-4">
    <div class="box box-primary alert alert-danger">
        <div class="box-body" style="padding:10px">
            <strong class="text-white">PERSYARATAN BELUM TERPENUHI SEMUA</strong>
        </div>
    </div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
    <div class="box box-danger direct-chat direct-chat-warning">
        <div class="box-body" style="padding:10px">
            <h4>Berikut kami tampilkan hasil pemeriksaan masing-masing anggota kelompok ini : </h4>
            <ol>
                @foreach ($kelompok as $itmKel)
                    <li class="text-bold">{{ $itmKel["NAMA"] }}
                        <ul>
                            <li>{!! $itmKel["sts_absensi"] == 1 ? "<span class='text-primary'>Prosentase ketidakhadiran < 25%</span>" : "<span class='text-danger'>Prosentase ketidakhadiran >= 25%</span>" !!}</li>
                            <li>{!! $itmKel["sts_nilai"] == 1 ? "<span class='text-primary'>Semua nilai telah memenuhi syarat</span>" : "<span class='text-danger'>Terdapat nilai yang belum tuntas. Harap cek Nilai di SINKAD.</span>" !!}</li>
                            <li>{!! $itmKel["sts_adminis"] == 1 ? "<span class='text-primary'>Administrasi sampai bulan ini telah diselesaikan</span>" : "<span class='text-danger'>Administrasi sampai bulan ini belum diselesaikan</span>" !!}</li>
                        </ul>
                    </li>
                    <br>
                @endforeach
            </ol>
        </div>
    </div>
</div>
</div>
@endsection