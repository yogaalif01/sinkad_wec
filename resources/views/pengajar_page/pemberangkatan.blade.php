@extends('pengajar_page.layout2')
@section('pemberangkatan','active')
@section('ojt','active')
@section('header')
<h1>Data Kelompok OJT</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/pengajar')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Kelompok</li>
</ol>
@endsection
@section('body')
<div class="row">
    <div class="col-lg-12">
    
    <div class="box box-danger direct-chat direct-chat-warning">
        <div class="box-body" style="padding: 10px;">
        <h4><b>Data Kelompok Belum di Setujui</b></h4>
        <table id="datapemberangkatan" class="datakelompok table table-bordered table-hover">
            <thead>
            <tr>
              <th>#</th>            
              <th class="cari">No Kelompok</th>
              <th class="cari">Nama Perusahaan</th>
              <th class="cari">Status Kelompok</th>
              <th class="cari">Tanggal Pengajuan</th>
              <th>aksi</th>
            </tr>
            </thead>
            <tbody>
                    <?php $no = 1?>
          @foreach($kelompok as $tp)
              <tr>
                  <td>{{$no}}</td>
                  <td>{{$tp['no_kelompok']}}</td>
                  <td>{{$tp['nama_perusahaan']}}</td>
                  <td>{{ $tp['sts_pencarian'] ==  2 ? "Siap TA" : $tp['sts_kelompok']}}</td>
                  <td>{{$tp['created_at']}}</td>
                  <td>
                      <a href="{{url('/pengajar/detailpemberangkatan/'.$tp['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>
                  </td>
              </tr>
              @php
                  $no++;
              @endphp
               @endforeach
            </tbody>
          </table>
        </div>
    </div>
    
    </div>
    </div>
    
    
    <div class="row">
    <div class="col-lg-12">
    
    <div class="box box-primary direct-chat direct-chat-warning">
        <div class="box-body" style="padding: 10px;">
        <h4><b>Data Kelompok Sudah di Setujui</b></h4>
        <table id="datapemberangkatan2" class="datakelompok table table-bordered table-hover">
            <thead>
            <tr>
                <th>#</th>          
                <th class="cari">No Kelompok</th>
                <th class="cari">Nama Perusahaan</th>
                <th class="cari">Status Kelompok</th>
                <th class="cari">Tanggal Pengajuan</th>
                <th>aksi</th>
            </tr>
            </thead>
            <tbody>
                    <?php $no = 1?>
            @foreach($kelompok2 as $tp2)
                <tr>
                    <td>{{$no}}</td>
                    <td>{{$tp2['no_kelompok']}}</td>
                    <td>{{$tp2['nama_perusahaan']}}</td>
                    <td>{{ $tp2['sts_pencarian'] ==  2 ? "Siap TA" : $tp2['sts_kelompok']}}</td>
                    <td>{{$tp2['created_at']}}</td>
                    <td>
                        <a href="{{url('/pengajar/detailpemberangkatan/'.$tp2['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>
                    </td>
                </tr>
                @php
                    $no++;
                @endphp
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
    
    </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
        
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding: 10px;">
            <h4><b>Data Kelompok Yang Sudah Siap Ujian</b></h4>
            <table id="datapemberangkatan3" class="datakelompok table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>          
                    <th class="cari">No Kelompok</th>
                    <th class="cari">Nama Perusahaan</th>
                    <th class="cari">Tanggal Pengajuan</th>
                    <th>aksi</th>
                </tr>
                </thead>
                <tbody>
                        <?php $no = 1?>
                @foreach($kelompok3 as $tp)
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$tp['no_kelompok']}}</td>
                        <td>{{$tp['nama_perusahaan']}}</td>
                        <td>{{$tp['created_at']}}</td>
                        <td>
                            <a href="{{url('/pengajar/detailpemberangkatan/'.$tp['no_kelompok'])}}"><button class="btn btn-primary btn-sm">Detail</button>
                        </td>
                    </tr>
                    @php
                        $no++;
                    @endphp
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){

    $('#datapemberangkatan thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var datapemberangkatan = $('#datapemberangkatan').DataTable();
    datapemberangkatan.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    $('#datapemberangkatan2 thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var datapemberangkatan2 = $('#datapemberangkatan2').DataTable();
    datapemberangkatan2.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    $('#datapemberangkatan3 thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var datapemberangkatan3 = $('#datapemberangkatan3').DataTable();
    datapemberangkatan3.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    // $('.datakelompok').DataTable();

});
</script>
@endsection