<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
   
    <!-- Title -->
    <title>Sistem Informasi Akademik</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{asset('landing/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" type="image/ico" href="{{asset('landing/images/head logo.png')}}" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{asset('landing/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/animate.css')}}">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="{{asset('landing/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('landing/style.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/responsive.css')}}">
    {{-- font awesome --}}
    <link rel="stylesheet" href="{{('fa4/css/font-awesome.min.css')}}">
    <script src="{{asset('landing/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    {{-- datatable --}}
    <link rel="stylesheet" href="{{asset('dt/css/dataTables.bootstrap.css')}}">
   
    
    

 
</head>

<body data-spy="scroll" data-target="#primary-menu">
<input type="hidden" name="" id="base_url" value="{{url('/')}}">

    {{-- <div class="preloader">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div> --}}
    <!--Mainmenu-area-->
    <div class="mainmenu-area" data-spy="affix" data-offset-top="100">
        <div class="container">
            <!--Logo-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand logo">
                <img src="{{asset('landing/images/logo wec.png')}}" width="60%" alt="" srcset="">
                </a>
            </div>
            <!--Logo/-->
            <nav class="collapse navbar-collapse" id="primary-menu">
                <ul class="nav navbar-nav navbar-right" style="">
                    <li class="active"><a href="#home-page">Home</a></li>
                    <li><a href="#service-page">Download</a></li>
                    <li><a href="#feature-page">Galeri</a></li>
                    <li><a href="#price-page">Info Perusahaan OJT</a></li>
                    <li><a href="#team-page">Pengajar</a></li>
                    @if(session('login') == 1)
                    <li><a href="{{url('/mahasiswa/index.html')}}"><i class="fa fa-home"></i>{{ " SINKAD - ".session('nama_mhs2')}}</a></li>
                    @else    
                    <li><a href="#contact-page">Login</a></li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
    <!--Mainmenu-area/-->



    <!--Header-area-->
    <header class="header-area overlay full-height relative v-center" id="home-page">
        <div class="absolute anlge-bg"></div>
        <div class="container">
            <div class="row v-center">
                <div class="hidden-xs hidden-sm col-md-5 text-right">
                  
                </div>
            </div>
        </div>
    </header>
    <!--Header-area/-->



    <!--Feature-area-->
   

    <section class="angle-bg sky-bg section-padding" id="service-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="caption_slide" class="carousel slide caption-slider" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>Materi dan Soal Latihan KE, KS, KP</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p>Telah disediakan beberapa materi dan soal latihan yang bisa di download secara bebas oleh mahasiswa</p>
                                        </div>
                                       
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <div id="ke" style="width:65%;height:400px;background-color: aliceblue;padding:20px;overflow:scroll;">
                                            
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>Materi dan Soal Latihan Informatika</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                           <p>Telah disediakan beberapa materi dan soal latihan yang bisa di download secara bebas oleh mahasiswa</p>
                                        </div>
                                       
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <div id="ik" style="width:65%;height:400px;background-color: aliceblue;padding:20px;overflow:scroll;">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                   <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>Materi dan Soal Latihan Desain Grafis</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                           <p>Telah disediakan beberapa materi dan soal latihan yang bisa di download secara bebas oleh mahasiswa</p>
                                        </div>
                                       
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <div id="dk" style="width:65%;height:400px;background-color: aliceblue;padding:20px;overflow:scroll;">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>Dokumen Lain-lain</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                           <p>Disediakan dokumen pendukung lainnya selain materi dan soal latihan.</p>
                                        </div>
                                       
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <div id="lain" style="width:65%;height:400px;background-color: aliceblue;padding:20px;overflow:scroll;">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Indicators -->
                        <ol class="carousel-indicators caption-indector">
                            <li data-target="#caption_slide" data-slide-to="0" class="active">
                                <strong>KE, KS dan KP </strong>Klik untuk download.
                            </li>
                            <li data-target="#caption_slide" data-slide-to="1">
                                <strong>Informatika </strong>Klik untuk download.
                            </li>
                            <li data-target="#caption_slide" data-slide-to="2">
                                <strong>Desain Grafis </strong>Klik untuk download.
                            </li>
                            <li data-target="#caption_slide" data-slide-to="3">
                                <strong>Lain-lain </strong>Klik untuk download.
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="gray-bg section-padding" id="feature-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Galeri Kegiatan Mahasiswa</h2>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="row">
               
                {!! $galeri !!}
                
                
            </div>
        </div>
    </section>




    <section class="price-area section-padding" id="price-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Info Perusahaan OJT</h2>
                        <p>Berikut adalah daftar nama-nama perusahaan yang dapat anda jadikan sebagai referensi tempat OJT</p>
                    </div>
                </div>
            </div>
            <div class="row">

<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th width="5%">#</th>
                <th width="30%">Nama Perusahaan</th>
                <th width="25%">Alamat</th>
                <th width="10%">No Telp</th>
                
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($perusahaan->get() as $itemPerusahaan)
            <tr>
                <td>{{$no}}</td>
                <td>{{$itemPerusahaan['nama_perusahaan']}}</td>
                <td>{{$itemPerusahaan['alamat']}}</td>
                <td>{{$itemPerusahaan['telp']}}</td>
            </tr>
            @php
                $no++;
            @endphp
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Nama Perusahaan</th>
                <th>Alamat</th>
                <th>No Telp</th>
            </tr>
        </tfoot>
    </table>

                
            </div>
        </div>
    </section>



    <section class="section-padding gray-bg" id="team-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Galeri Pengajar</h2>
                        <p>Terdapat {{ count($pengajar->get()) }} pengajar di Wearnes Education Center.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($pengajar->get() as $itemPengajar)
                    <div class="col-xs-6 col-sm-3 col-md-2" style="padding-top:10px;">
                        <div class="single-team">
                            <div class="team-photo" style="height:230px;">
                                <img src="{{('storage/app/img_pengajar/'.$itemPengajar['nip'].'.jpg')}}" alt="">
                            </div>
                            <strong><h6>{{$itemPengajar['nama']}}</h6></strong>
                            <h6>{{$itemPengajar['jbt']}}</h6>
                            <ul class="social-menu">
                            <span class="fa fa-phone"></span> {{$itemPengajar['notelp']}}
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>


    <footer class="footer-area relative sky-bg" id="contact-page">
        <div class="absolute footer-bg"></div>
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                        <div class="page-title">
                            <h2>Login</h2>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <address class="side-icon-boxes">
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="images/location-arrow.png" alt="">
                                </div>
                                <p><strong>Belum punya akun ? </strong> <a href="{{ url('/daftar') }}">Klik Disini</a>  </p>
                            </div>

                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="images/phone-arrow.png" alt="">
                                </div>
                                <p><strong>Lupa Password ? </strong>
                                  <a href="#" data-toggle="modal" data-target="#confirmReset">Klik Disini</a>
                                </p>
                            </div>
                          
                        </address>
                    </div>
                    <div class="col-xs-12 col-md-8">
                       
                        <form id="login-form" method="post" class="contact-form">
                            <div class="row">
                            <div class="col-lg-6">
                                <input type="text" id="nim" name="nim" placeholder="Nomor Induk Mahasiswa" class="form-control" required="required">
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-6">
                                <input type="password" id="pwd" name="pwd" placeholder="Password" class="form-control" required="required">
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-6">
                               <div id="feedback-login"></div>
                            </div>
                            </div>
                            <button type="sibmit" class="button">Submit</button>
                        </form>
                    
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle">
            <div class="container">
                <div class="row">
                   
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p>&copy;Copyright 2019 Wearnes Education Center</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>



<div id="modal_galeri" class="modal fade" role="dialog" style="padding-top:100px;">
  <div class="modal-dialog">
       <img src="" alt="" srcset="" id="img_pop">
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmReset" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top:10em;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="col-lg-12">
              <input type="text" name="" class="form-control" id="nimLP" placeholder="Masukkan NIM anda">
              
              <div class="form-lupa">
                  Pertanyaan lupa password
                  <input type="text" class="form-control" name="" id="tanyaLP" value="">
                  Jawaban lupa password
                  <input type="text" class="form-control" name="" id="jawabLP">
                </div>
            <div class="text-danger"><b> * Tekan enter </b></div>
            
          </div>
          <div class="col-lg-12">
              <div id="feedback-lupa"></div>
          </div>
        {{-- <h5 class="text-primary">Silahkan hubungi admin untuk reset password di 085648290874.</h5> --}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
    
    
    <!--Vendor-JS-->
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> --}}
    <script src="{{asset('landing/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('landing/js/vendor/bootstrap.min.js')}}"></script>

    <!--Plugin-JS-->
    <script src="{{asset('landing/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('landing/js/contact-form.js')}}"></script>
    <script src="{{asset('landing/js/jquery.parallax-1.1.3.js')}}"></script>
    <script src="{{asset('landing/js/scrollUp.min.js')}}"></script>
    <script src="{{asset('landing/js/magnific-popup.min.js')}}"></script>
    <script src="{{asset('landing/js/wow.min.js')}}"></script>

    {{-- Datatable --}}
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

    
    {{-- filetree --}}
    <script src="{{asset('ft/jqueryFileTree.js')}}" type="text/javascript"></script>
    <link href="{{asset('ft/jqueryFileTree.css')}}" rel="stylesheet" type="text/css" media="screen" />

    <!--Main-active-JS-->
    <script src="{{asset('landing/js/main.js')}}"></script>

   
    <script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var base_url = $('#base_url').val();
        $('.form-lupa').hide();
        var jawaban = '';
        var pass = '';
        $('#login-form').on('submit', function(e){
            e.preventDefault();
            var nim = $('#nim').val();
            var pwd = $('#pwd').val();
            $.ajax({
                url:base_url+"/login",
                type:'POST',
                data:{nim:nim, pwd:pwd},
                success:function(result){
                    // $('#feedback-login').html(result);
                     switch (result) {
                        case "0":
                            $('#feedback-login').html("<div class='alert alert-danger'>NIM yang anda masukkan salah</div>")
                            break;
                        case "1":
                           window.location.href = base_url+"/mahasiswa/index.html";
                            break;
                        case "2":
                            $('#feedback-login').html("<div class='alert alert-danger'>Tahun akademik tidak ditemukan</div>")
                            break;
                        case "3":
                            $('#feedback-login').html("<div class='alert alert-danger'>Password yang anda masukkan salah</div>")
                            break;
                    
                        default:
                            // $('#feedback-login').html("<div class='alert alert-danger'>No Access !</div>")
                            break;
                    }
                },
                error:function(result){
                    alert("Terjadi Kesalahan !");
                    console.log(result.responseText);
                }
            });

        });
      
        $('#example').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
        });

       $('.popup').click(function(e){
           e.preventDefault();
           var src = $(this).attr("alt");
           $('#img_pop').attr("src", src);
           $('#modal_galeri').modal();
       })
       
       $('#ke').fileTree({ root: '../../storage/app/KE-KS-KP/', script: '../sinkad_wec/ft/connectors/jqueryFileTree.php' }, function(file) { 
		var panjang =file.length;
		var url = file.substr(17,panjang - 12);
		url = url.split("/");
		var path = "+";
		for(var i = 1; i<url.length;i++)
		{
			if(i == url.length-1)
			{
				path += url[i];
			}else{
				path += url[i]+"+";
			}
		}
		window.location.href=base_url+"/download/"+path;
        });
        
         $('#ik').fileTree({ root: '../../storage/app/Informatika/', script: '../sinkad_wec/ft/connectors/jqueryFileTree.php' }, function(file) { 
            var panjang =file.length;
            var url = file.substr(17,panjang - 12);
            url = url.split("/");
            var path = "+";
            for(var i = 1; i<url.length;i++)
            {
                if(i == url.length-1)
                {
                    path += url[i];
                }else{
                    path += url[i]+"+";
                }
            }
            window.location.href=base_url+"/download/"+path;
        });
        $('#dk').fileTree({ root: '../../storage/app/design/', script: '../sinkad_wec/ft/connectors/jqueryFileTree.php' }, function(file) { 
            var panjang =file.length;
            var url = file.substr(17,panjang - 12);
            url = url.split("/");
            var path = "+";
            for(var i = 1; i<url.length;i++)
            {
                if(i == url.length-1)
                {
                    path += url[i];
                }else{
                    path += url[i]+"+";
                }
            }
            window.location.href=base_url+"/download/"+path;
        });
        $('#lain').fileTree({ root: '../../storage/app/lain/', script: '../sinkad_wec/ft/connectors/jqueryFileTree.php' }, function(file) { 
            var panjang =file.length;
            var url = file.substr(17,panjang - 12);
            url = url.split("/");
            var path = "+";
            for(var i = 1; i<url.length;i++)
            {
                if(i == url.length-1)
                {
                    path += url[i];
                }else{
                    path += url[i]+"+";
                }
            }
            // alert(path);
            window.location.href=base_url+"/download/"+path;
        });
        
        $('#nimLP').keyup(function(e){
            if(e.keyCode == 13){
                var nim = $(this).val();
                $.ajax({
                   url:base_url+"/lupa",
                   type:'POST',
                   data:{nim:nim},
                   beforeSend:function(){
                       $('#feedback-lupa').html("<div class='alert alert-success'>Tunggu sebentar ..</div>");
                   },
                   success:function(result){
                       result = JSON.parse(result);
                       console.log(result);
                       switch (result.status) {
                           case 1:
                                setInterval(function(){$('#feedback-lupa').html("");}, 100);
                                $('.form-lupa').show();
                                $('#jawabLP').focus();
                                $('#tanyaLP').val(result.tanya);
                                jawaban = result.jawab;
                                pass = result.pass;
                               break;
                            case 0:
                                $('#feedback-lupa').html("<div class='alert alert-danger'>NIM tidak ditemukan</div>");
                                setInterval(function(){$('#feedback-lupa').html("");}, 1000);
                                break;
                            case 3:
                                $('#feedback-lupa').html("<div class='alert alert-danger'>Koneksi Database gagal</div>");
                                setInterval(function(){$('#feedback-lupa').html("");}, 1000);
                                break;

                           default:
                               break;
                       }
                   },
                   error:function(result){
                       alert('Terjadi Kesalahan !');
                       console.log(result.responseText);
                   }
                });
            }
        });

        $('#jawabLP').keyup(function(e){
            if(e.keyCode == 13){
                var jawabannya = $(this).val();
                var nim        = $("#nimLP").val();

                // alert(jawaban);
                // alert(jawabannya);
                // alert(pass);

                $.ajax({
                    url : base_url+"/jawab",
                    type:'POST',
                    data:{nim:nim, jawaban:jawabannya},
                    beforeSend:function(){
                        $('#feedback-lupa').html("<div class='alert alert-info'>Tunggu Sebentar ...</div>");   
                    },
                    success:function(r){
                        // console.log(r['status']);
                        var result = JSON.parse(r);
                        // console.log(result);
                        if(result['status'] == 1){
                            window.location.href = base_url+"/mahasiswa/index.html";
                        }else{
                            $('#feedback-lupa').html("<div class='alert alert-danger'>Jawaban Salah.</div>");
                        }
                    },
                    error:function(err){

                    }
                });
                
            }
        });
        

    });

  
    </script>
</body>

</html>
