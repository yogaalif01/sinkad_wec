<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
   
    <!-- Title -->
    <title>Sistem Informasi Akademik</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{asset('landing/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" type="image/ico" href="{{asset('landing/images/head logo.png')}}" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{asset('landing/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/animate.css')}}">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="{{asset('landing/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('landing/style.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/responsive.css')}}">
    {{-- font awesome --}}
    <link rel="stylesheet" href="{{('fa4/css/font-awesome.min.css')}}">
    <script src="{{asset('landing/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    {{-- datatable --}}
    <link rel="stylesheet" href="{{asset('dt/css/dataTables.bootstrap.css')}}">
   
    
    

 
</head>

<body data-spy="scroll" data-target="#primary-menu">
<input type="hidden" name="" id="base_url" value="{{url('/')}}">

    {{-- <div class="preloader">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div> --}}
    <!--Mainmenu-area-->
    <div class="mainmenu-area" data-spy="affix" data-offset-top="100">
        <div class="container">
            <!--Logo-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand logo">
                <img src="{{asset('landing/images/logo wec.png')}}" width="60%" alt="" srcset="">
                </a>
            </div>
            <!--Logo/-->
            <nav class="collapse navbar-collapse" id="primary-menu">
                <ul class="nav navbar-nav navbar-right" style="">
                    <li class="active"><a href="{{url('/')}}">Home</a></li>
                    <li><a href="#contact-page">Pendaftaran Akun Sinkad</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <!--Mainmenu-area/-->





    <!--Feature-area-->








    <footer class="footer-area relative sky-bg" id="contact-page">
        <div class="absolute footer-bg"></div>
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                        <div class="page-title">
                            <h2>Pendaftaran SINKAD</h2>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        {{-- <address class="side-icon-boxes">
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="images/location-arrow.png" alt="">
                                </div>
                                <p><strong>Belum punya akun ? </strong> <a href="{{ url('/daftar') }}">Klik Disini</a>  </p>
                            </div>

                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="images/phone-arrow.png" alt="">
                                </div>
                                <p><strong>Lupa Password ? </strong>
                                  <a href="#" data-toggle="modal" data-target="#confirmReset">Klik Disini</a>
                                </p>
                            </div>
                          
                        </address> --}}
                    </div>
                    <div class="col-xs-12 col-md-8">
                       
                        <form id="ceknim-form" method="post" class="contact-form">
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="text" id="nim" name="nim" placeholder="Nomor Induk Mahasiswa" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="submit" value="CEK NIM" class="btn btn-primary">
                                </div>
                            </div>
                            <br>
                            <div class="row" id="coverConfirmCek">
                                <div class="col-lg-6">
                                    <div id="confirmCek">
                                        <div class="alert alert-info">Tunggu Sebentar ...</div>
                                    </div>
                                </div>
                            </div>

                           
                        </form>

                        <form id="datamhs-form" method="post" class="contact-form" action="{{ url('/pdaftar') }}">
                            <input type="hidden" name="_token" value="{{ Session::token() }}">

                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="text" id="nimx" name="nimx" disabled placeholder="Nomor Induk Mahasiswa" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="text" id="nmMahasiswa" disabled name="nmMahasiswa" placeholder="Nama Mahasiswa" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="text" id="nmpanggilan" name="nmpanggilan" placeholder="Nama Panggilan Mahasiswa" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="password" id="pwd" name="pwd" placeholder="Password SINKAD" class="form-control" required="required">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <select name="tanyalupa" id="" class="form-control">
                                        <option value="">[ Pertanyaan Lupa Password ]</option>
                                        <option value="Siapa Nama Ibumu ?">Siapa Nama Ibumu ?</option>
                                        <option value="Apa Merk Ponsel Pertamamu ?">Apa Merk Ponsel Pertamamu ?</option>
                                        <option value="Siapa Nama Pacar/Sahabatmu ?">Siapa Nama Pacar/Sahabatmu ?</option>
                                    </select>
                                </div>
                                
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="text" id="jawablupa" name="jawablupa" placeholder="Jawaban Lupa Password" class="form-control" required="required">

                                </div>
                            </div>

                            <div class="row" id="coverConfirmDaftar">
                                <div class="col-lg-6">
                                    <div id="confirmDaftar">
                                        <div class="alert alert-info">Tunggu Sebentar ...</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="submit" value="DAFTAR" class="btn btn-primary">
                                </div>
                            </div>
                            <br>
                            

                           
                           
                        </form>
                    
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle">
            <div class="container">
                <div class="row">
                   
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p>&copy;Copyright 2019 Wearnes Education Center</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--Vendor-JS-->
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> --}}
    <script src="{{asset('landing/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('landing/js/vendor/bootstrap.min.js')}}"></script>

    <!--Plugin-JS-->
    <script src="{{asset('landing/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('landing/js/contact-form.js')}}"></script>
    <script src="{{asset('landing/js/jquery.parallax-1.1.3.js')}}"></script>
    <script src="{{asset('landing/js/scrollUp.min.js')}}"></script>
    <script src="{{asset('landing/js/magnific-popup.min.js')}}"></script>
    <script src="{{asset('landing/js/wow.min.js')}}"></script>

    {{-- Datatable --}}
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

    
    {{-- filetree --}}
    <script src="{{asset('ft/jqueryFileTree.js')}}" type="text/javascript"></script>
    <link href="{{asset('ft/jqueryFileTree.css')}}" rel="stylesheet" type="text/css" media="screen" />

    <!--Main-active-JS-->
    <script src="{{asset('landing/js/main.js')}}"></script>

   
    <script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var base_url = $('#base_url').val();

        $('#datamhs-form').hide();
        $('#coverConfirmCek').hide();
        $('#coverConfirmDaftar').hide();

        $('#ceknim-form').on('submit', function(e){
            e.preventDefault();
            $.ajax({
                url:base_url+"/ceknim",
                type:'POST',
                beforeSend:function(){
                    $('#coverConfirmCek').show();
                },
                data:$(this).serialize(),
                success:function(r){
                    console.log(r);
                    if(r == 1){
                        $('#confirmCek').html("<div class='alert alert-danger'>NIM Sudah Terdaftar</div>");
                    }else{
                        var data = JSON.parse(r);
                        console.log(r);
                        if(data.length == 0){
                            console.log("test");
                            $('#confirmCek').html("<div class='alert alert-danger'>NIM Tidak Ditemukan</div>");
                        }else{
                            $('#ceknim-form').hide();
                            $('#datamhs-form').show();

                            $('#nimx').val(data[0]['NIM']);
                            $('#nmMahasiswa').val(data[0]['NAMA']);
                        }
                    }
                },
                error:function(err){
                    console.log(err.responseText());
                }
            });
        });

        $('#datamhs-form').on("submit", function(e){
            e.preventDefault();
            $.ajax({
                url:base_url+"/pdaftar",
                type:'POST',
                data:$(this).serialize(),
                beforeSend:function(){
                    $('#coverConfirmDaftar').show();
                },
                success:function(r){
                    var result = JSON.parse(r);
                    if(result['status'] == 1){
                        window.location.href = base_url+"/mahasiswa/index.html";
                    }else{
                        $('confirmDaftar').html("<div class='alert alert-danger'>Terjadi Kesalahan Di Proses Pendaftaran !</div>")
                    }
                    
                },
                error:function(err){
                    console.log(err.responseText());
                }
            });
        });

    });

  
    </script>
</body>

</html>
