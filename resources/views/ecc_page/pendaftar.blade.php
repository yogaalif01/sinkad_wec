@extends('ecc_page.layout2')
@section('pendaftar','active')
@section('header')
<h1>
Data Pendaftar ECC
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ecc')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Data pendaftar</li>
</ol>
@endsection

@section('body')
<div class="row">
    <div class="col-lg-12">

        <div class="box box-warning direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">
                @if (count($eccData) > 0)
                    <a href="{{url('ecc/cetak.html')}}" target="_blank">
                        <button class="btn btn-primary">C E T A K</button><br><br>
                    </a>
                @endif

                <table id="tbpendaftar" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="8%">#</th>
                            <th width="20%">NIM</th>
                            <th width="50%">NAMA</th>
                            <th width="20%">KELAS</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no=1;
                        @endphp
                        @foreach ($eccData as $row)
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$row['NIM']}}</td>
                            <td>{{$row['NAMA']}}</td>
                            <td>{{$row['KELAS']}}</td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>        
            </div>
        </div>

    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function(){
    $('#tbpendaftar').DataTable();
});
</script>
@endsection
