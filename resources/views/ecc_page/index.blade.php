@extends('ecc_page.layout2')
@section('beranda','active')
@section('header')
<h1>
Beranda
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Beranda</li>
</ol>
@endsection

@section('body')
<div class="row">
    <div class="col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Pendaftar di <br> Jadwal Aktif</span>
                <span class="info-box-number">{{count($ecc)}}</span>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-calendar-check-o"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Jadwal Aktif</span>
                <span class="info-box-number">
                    @if (count($ecc) == 0)
                        -
                    @else
                        {{$ecc[0]["tanggal"]}}
                    @endif
                </span>
            </div>
        </div>
    </div>
</div>
@endsection