@extends('ecc_page.layout2')
@section('jadwal','active')
@section('header')
<h1>
Setting Jadwal ECC
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ecc')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active">Setting jadwal</li>
</ol>
@endsection
@section('body')
<input type="hidden" id="sts_jadwal" data-value="{{Session::get("sts_jadwal")}}" />

    

<div class="row">
    <div class="col-lg-8">
        <div class="box box-success direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">
            
                    <form class="form-horizontal" id="frmJadwal" action="{{url('/ecc/ubahJadwalpost')}}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="method" value="POST">
                        <input type="hidden" name="id_jadwal" value="{{$eccData[0]["id"]}}">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="txttanggal" class="col-sm-2 control-label">Tanggal</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="txttanggal" name="txttanggal" placeholder="Tanggal" value="{{ old("txttanggal") <> null ? old("txttanggal") : $eccData[0]["tanggal"] }}">
                                    @if ($errors->has("txttanggal"))
                                        <small class="text-danger">{{ $errors->first("txttanggal") }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtjam" class="col-sm-2 control-label">Jam</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="txtjam" name="txtjam" placeholder="Jam ( hh:mm )" value="{{ old("txtjam") <> null ? old("txtjam") : $eccData[0]["jam"] }}">
                                    @if ($errors->has("txtjam"))
                                        <small class="text-danger">{{ $errors->first("txtjam") }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtlokasi" class="col-sm-2 control-label">Tempat</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="txtlokasi" name="txtlokasi" placeholder="Tempat" value="{{ old("txtlokasi") <> null ? old("txtlokasi") : $eccData[0]["tempat"] }}">
                                    @if ($errors->has("txtlokasi"))
                                        <small class="text-danger">{{ $errors->first("txtlokasi") }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-default" id="btnBatal">Batal</button>
                            <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>

            </div>
        </div>
    </div>
</div>


@endsection

@section('css')

@endsection

@section('script')
<script type="text/javascript" src="{{asset('js/jquery.mask.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mask.test.js')}}"></script>
<script>
$(document).ready(function(){

$('#txttanggal').mask('0000-00-00');
$('#txtjam').mask('00:00');


$('#btnBatal').click(function(e){
  e.preventDefault();
  window.location.reload();
});

})
</script>

@endsection