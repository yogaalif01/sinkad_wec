<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Peserta ECC {{ date("d F Y", strtotime($eccData[0]["tanggal"])) }}</title>
    <style>
    body{
        margin:auto;
    }
    table, th, td{
        border: 1px solid black;
        border-collapse: collapse;
        
    }
    </style>
</head>
<body>
                
<div id="header" style="margin:auto;text-align: center">
    <p><span style="font-size: 14px;font-weight: bold">DATA PESERTA ENGLISH CONVERSATION CLUB</span> <br>
        {{ date("d F Y", strtotime($eccData[0]["tanggal"])) }}</p>
</div>

<table id="tbpendaftar" class="table table-bordered table-striped" style="width:70%;margin:auto">
    <thead>
        <tr>
            <th width="5%">NO</th>
            <th width="15%">NIM</th>
            <th width="50%">NAMA</th>
            <th width="10%">KELAS</th>
            <th width="10%"></th>
        </tr>
    </thead>
    <tbody>
        @php
            $no=1;
        @endphp
        @foreach ($eccData as $row)
        <tr>
            <td height="25">&nbsp;{{$no}}</td>
            <td>&nbsp;{{$row['NIM']}}</td>
            <td>&nbsp;{{$row['NAMA']}}</td>
            <td>&nbsp;{{$row['KELAS']}}</td>
            <td></td>
        </tr>
        @php
            $no++;
        @endphp
        @endforeach
    </tbody>
</table>


<script>
window.print();
</script>
</body>
</html>