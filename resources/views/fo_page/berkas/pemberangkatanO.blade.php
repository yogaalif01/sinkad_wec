<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pemberangkatan</title>

      <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
      <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('lte2/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <style>
    body{
     /* color:blue;    */
    }
    .bold{
        font-weight: bold !important;
    }

    #body{
       margin:0px 70px;
    }

    @page {
    header: page-header;
    footer: page-footer;
    }

    #datamhs td, th {
        border:1px solid black;
        padding-left: 5px;
        font-size: 14px;
    }

    
    
    </style>
</head>
<body>

<htmlpageheader name="page-header">
    @if (substr(session("th_ajaran"), -3) == "roi")
    <img class="img img-responsive" style="padding-top:30px;width:100%;" src="{{ url("storage/app/berkas/header-surat-roi.jpg") }}" alt="" srcset="">
    @else
    <img class="img img-responsive" style="padding-top:30px;width:100%;" src="{{ url("storage/app/berkas/header-surat.png") }}" alt="" srcset="">
    @endif
</htmlpageheader>
<htmlpagefooter name="page-footer">
    @if (substr(session("th_ajaran"), -3) == "roi")
    <img class="img img-responsive" style="padding-bottom:30px;width:100%;" src="{{ url("storage/app/berkas/footer-surat-roi.jpg") }}" alt="" srcset="">
    @else
    <img class="img img-responsive" style="padding-bottom:30px;width:100%;" src="{{ url("storage/app/berkas/footer-surat.png") }}" alt="" srcset="">
    @endif
</htmlpagefooter>

@php
    $bulanIndo = ["Januari", "Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
@endphp
<div id="body">
<div class="pull-right" style="width:100%;text-align: right">Malang, {{ date("d")." ".$bulanIndo[intval(date("m")-1)]." ".date("Y") }}</div>
<table style="width:60%">
    <tr>
        <td style="width:20%">No</td>
        @php
            $kampus = substr(session("th_ajaran"), -3) == "roi" ? "ROI" : "WEC";
        @endphp
        <td>:</td>
        <td>&nbsp; {{ $kelompok[0]["no_kelompok"]."/".$kampus."/".$kelompok[0]["kode_perusahaan"]."/OJT/".$bulan."/".date("Y") }} </td>
    </tr>
    <tr>
        <td>Lampiran</td>
        <td>:</td>
        <td>&nbsp; 1 (satu) Lembar</td>
    </tr>
    <tr>
        <td style="vertical-align: top">Perihal</td>
        <td style="vertical-align: top">:</td>
        <td>&nbsp; <span class="bold"> Daftar Nama Mahasiswa Peserta &nbsp;&nbsp;OJT/TA 0612</td>
    </tr>
</table>
<br>
<br>

Kepada <br>
Yth. Bapak/Ibu Pimpinan<br>
{{ $kelompok[0]["nama_perusahaan"] }} <br>
{!! $kelompok[0]["up"] <> null ? "u.p. ".$kelompok[0]["up"]."<br>" : ""  !!}
{{ $kelompok[0]["alamat"] }} <br>
<span style="text-transform:uppercase">{{ $kelompok[0]["kota"] }} </span>
<br>
<br>

Dengan Hormat, 
<br><br>
<p style="text-align: justify">Sebelumnya kami sampaikan banyak terima kasih atas diberikannya kesempatan pada Mahasiswa {{ substr(session("th_ajaran"), -3) == "roi" ? "Royal Ocean International Malang Pusat Pendidikan 1 (satu) tahun Perhotelan dan kapal persiar" : "Wearnes Education Center Malang Pendidikan Program Profesi 1 (satu) tahun" }}  untuk melakukan kegiatan On The Job Training (PKL/Magang/Tugas Akhir) di Perusahaan/Instansi yang Bapak/Ibu pimpin.</p>
<br>
<p style="text-align: justify">Berikut adalah nama-nama mahasiswa yang melakukan OJT/TA untuk bulan <span class="bold"> {{ $kelompok[0]["lama"] > 1 ? $bulanIndo[$kelompok[0]["bulan"]-1]." - ".$bulanIndo[($kelompok[0]["bulan"]+($kelompok[0]["lama"]-1))-1] : $bulanIndo[$kelompok[0]["bulan"]-1] }} tahun {{ date("Y") }}  </span> di <span class="bold"> {{ $kelompok[0]["nama_perusahaan"] }} </span></p>

<table id="datamhs" style="width:100%" cellspacing="0">
    <tr style="background-color: #cbd0d6">
        <th style="width:8%;text-align: center">NO</th>
        <th style="width:20%;text-align: center">NIM</th>
        <th style="text-align: center">NAMA MAHASISWA</th>
    </tr>
    @php
        $no = 1;
    @endphp
    @foreach ($kelompok as $itm)
    <tr>
        <td style="text-align: right;padding-right:5px;">{{ $no++ }}</td>
        <td> {{ $itm['NIM'] }} </td>
        <td> {{ $itm['NAMA'] }} </td>
    </tr>
    @endforeach
</table>
<br>
<p style="text-align: justify">Demikian surat pemberitahuan kami, atas perhatian dan kerja sama yang terbina selama ini, kami sampaikan terima kasih.</p>
<br><br><br>
@if (substr(session("th_ajaran"), -3) != "roi")
<img class="img pull-right" style="padding-top:10px;width:45%;" src="{{ url("storage/app/berkas/ttd-pakhor.png") }}" alt="" srcset="">
@else
<div style="text-align: center;margin-left:400px;">
    Hormat Kami, <br><br><br><br>
    <b class="bold">( Shaiful Horry , SE)</b> <br>
    Ketua OJT/TA
</div>
@endif

</div>
</body>
</html>