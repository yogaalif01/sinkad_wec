<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Surat Proposal</title>

      <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
      <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('lte2/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <style>
    body{
     /* color:blue;    */
    }
    .bold{
        font-weight: bold !important;
    }

    #body{
       margin:0px 70px;
    }

    @page {
    header: page-header;
    footer: page-footer;
    }

    
    </style>
</head>
<body>

<htmlpageheader name="page-header">
    @if (substr(session("th_ajaran"), -3) == "roi")
    <img class="img img-responsive" style="padding-top:30px;width:100%;" src="{{ url("storage/app/berkas/header-surat-roi.jpg") }}" alt="" srcset="">
    @else
    <img class="img img-responsive" style="padding-top:30px;width:100%;" src="{{ url("storage/app/berkas/header-surat.png") }}" alt="" srcset="">
    @endif
    
</htmlpageheader>
<htmlpagefooter name="page-footer">
    @if (substr(session("th_ajaran"), -3) == "roi")
    <img class="img img-responsive" style="padding-bottom:30px;width:100%;" src="{{ url("storage/app/berkas/footer-surat-roi.jpg") }}" alt="" srcset="">
    @else
    <img class="img img-responsive" style="padding-bottom:30px;width:100%;" src="{{ url("storage/app/berkas/footer-surat.png") }}" alt="" srcset="">
    @endif
</htmlpagefooter>

@php
    $bulanIndo = ["Januari", "Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
@endphp

<div id="body">
<div class="pull-right" style="width:100%;text-align: right">Malang, {{ date("d")." ".$bulanIndo[intval(date("m"))-1]." ".date("Y") }}</div>
<table style="width:60%">
    <tr>
        <td style="width:30%">No</td>
        @php
            $kampus = substr(session("th_ajaran"), -3) == "roi" ? "ROI" : "WEC";
        @endphp
        <td>: {{ $kelompok[0]->no_kelompok."/OJT/".$kampus."/".$bulan."/".date("Y") }} </td>
    </tr>
    <tr>
        <td>Lampiran</td>
        <td>: 1 (satu) Lembar</td>
    </tr>
</table>
<br>
<br>

Kepada <br>
Yth. Bapak/Ibu Pimpinan<br>
{{ $kelompok[0]->getperusahaan->nama_perusahaan }} <br>
{!! $kelompok[0]->getperusahaan->up <> null ? "u.p. ".$kelompok[0]->getperusahaan->up."<br>" : ""  !!}
{{ $kelompok[0]->getperusahaan->alamat }} <br>
<span style="text-transform:uppercase">{{ $kelompok[0]->getperusahaan->kota }} </span>
<br>
<br>

<span class="bold">Hal : On The Job Training (PKL) 0312</span>
<br><br>
@if (substr(session("th_ajaran"), -3) == "roi")
<p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Guna meningkatkan Sumber Daya Manusia (SDM) yang dimiliki oleh calon tenaga kerja, kami selaku penyelenggara Pusat Pendidikan Satu Tahun Perhotelan & Kapal Persiar Royal Ocean International Malang yang berorientasi pada profesionalisme kerja lapangan mandiri, mengharapkan bantuan Bapak/Ibu agar berkenan memberikan kesempatan kepada mahasiswa kami untuk melaksanakan Praktek Kerja secara langsung atau Magang Kerja di Hotel yang Bapak/Ibu pimpin.</p>

<p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Praktek kerja ini bukan merupakan penelitian ilmiah atau pengumpulan data dalam rangka penyusunan karya ilmiah, melainkan merupakan praktek kerja nyata pada bidang pekerjaan yang diijinkan oleh pihak Hotel yang sesuai dengan bidang pekerjaan yang relevan, di samping itu kiranya dapat membantu melaksanakan tugas - tugas tertentu yang diperlukan oleh pihak Hotel.</p>
@else
<p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Guna meningkatkan sumber daya manusia (SDM) yang dimiliki oleh calon tenaga kerja, kami selaku penyelenggara Program Profesi Satu Tahun Wearnes Education Center Malang yang berorientasi pada profesionalisme kerja lapangan mandiri, mengharapkan bantuan Bapak/Ibu agar berkenan memberikan kesempatan kepada mahasiswa kami untuk melaksanakan Praktek Kerja secara langsung atau Magang Kerja di Perusahaan/Instansi yang Bapak/Ibu pimpin.</p>

<p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Praktek kerja ini bukan merupakan penelitian ilmiah atau pengumpulan data dalam rangka penyusunan karya ilmiah, melainkan merupakan praktek kerja nyata pada bidang pekerjaan yang diijinkan oleh pihak Perusahaan/Instansi yang sesuai dengan bidang pekerjaan yang relevan, di samping itu kiranya dapat membantu melaksanakan tugas-tugas tertentu yang diperlukan oleh pihak Perusahaan/Instansi.</p>
@endif

<p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian permohonan ini kami sampaikan, dan sebelumnya kami mengucapkan terima kasih atas kerja samanya. Harapan kami semoga bermanfaat bagi upaya peningkatan kualitas sumber daya manusia Indonesia di era globalisasi.</p>
<br><br><br>

@if (substr(session("th_ajaran"), -3) != "roi")
<img class="img pull-right" style="padding-top:10px;width:45%;" src="{{ url("storage/app/berkas/ttd-pakhor.png") }}" alt="" srcset="">
@else
<div style="text-align: center;margin-left:400px;">
    Hormat Kami, <br><br><br><br>
    <b class="bold">( Shaiful Horry , SE)</b> <br>
    Ketua OJT/TA
</div>
@endif

</div>
</body>
</html>