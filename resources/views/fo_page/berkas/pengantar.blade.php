<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pengantar</title>
    <style>
    .bold{
        font-weight: bold !important;
    }
    td, th {
        border:1px solid black;
        padding-left: 5px;
        font-size: 14px;
    }
    </style>
</head>
<body>
@php
    $bulanIndo = ["Januari", "Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
@endphp
<span class="bold" style="text-transform:uppercase">hal-hal yang berkaitan dengan on the job training (pkl/magang) adalah sebagai berikut :</span>
<br>
<br>
<span class="bold" style="text-transform:uppercase"><u>bidang pekerjaan</u></span>
<p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dalam pelaksanaan praktek kerja lapangan pada Perusahaan yang Bapak/Ibu pimpin, aktifitas mahasiswa disesuaikan dengan keterampilan dan keahlian yang dimilikinya dan mematuhi segala peraturan kerja yang berlaku pada Perusahaan.</p>

<span class="bold" style="text-transform:uppercase"><u>waktu pelaksanaan</u></span>
@php
    $Slama = [0=>"nol", 1=>"satu",2=>"dua", 3=>"tiga", 4=>"empat", 5=>"lima", 6=>"enam"];
@endphp


<p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Waktu pelaksanaan On The Job Training (PKL/Magang) adalah selama <span class="bold">{{ $kelompok[0]["lama"] }} ( {{ $Slama[$kelompok[0]["lama"]] }} ) bulan</span>, kami mengharap pelaksanaannya di bulan <span class="bold"> {{ $kelompok[0]["lama"] > 1 ? $bulanIndo[$kelompok[0]["bulan"]-1]." - ".$bulanIndo[($kelompok[0]["bulan"]+($kelompok[0]["lama"]-1))-1] : $bulanIndo[$kelompok[0]["bulan"]-1] }} tahun {{ date("Y") }}  </span> alasan kurikulum materi perkuliahan sudah disampaikan {{  substr(session("th_ajaran"), -3) == "roi" ? "100%" : "85%"    }}.</p>

<span class="bold" style="text-transform:uppercase"><u>peserta dan nama-nama peserta</u></span>
@if (substr(session("th_ajaran"), -3) == "roi")
<p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Peserta yang melaksanakan On The Job Training (PKL/Magang) adalah mahasiswa Pusat Pendidikan Satu Tahun Perhotelan & Kapal Pesiar Jurusan {{ $jurusan }} Royal Ocean International yang berpusat di Malang yang telah mengikuti materi Perkuliahan dan Praktek di bidang Perhotelan & Kapal Persiar.</p>
@else
<p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Peserta yang melaksanakan On The Job Training (PKL/Magang) adalah mahasiswa program profesi satu tahun jurusan {{ $jurusan }} Wearnes Education Center yang berpusat di Malang yang telah mengikuti mata kuliah komputer dan mata kuliah non komputer.</p>
@endif

<span class="bold" style="text-transform:uppercase"><u>nama-nama peserta ojt</u></span>
<table style="width:100%" cellspacing="0">
    <tr style="background-color: #cbd0d6">
        <th style="width:8%">NO</th>
        <th style="width:20%">NIM</th>
        <th>NAMA MAHASISWA</th>
    </tr>
    @php
        $no = 1;
    @endphp
    @foreach ($detkelompok as $itm)
    <tr>
        <td style="text-align: right">{{ $no++ }}</td>
        <td> {{ $itm['NIM'] }} </td>
        <td> {{ $itm['NAMA'] }} </td>
    </tr>
    @endforeach
</table>
<br>
<span class="bold" style="text-transform:uppercase"><u>kurikulum untuk masing-masing mata kuliah sebagai berikut</u></span>
<div style="width:50%;float:left">
<table style="width:100%" cellspacing="0">
    <tr style="background-color: #cbd0d6">
        <th>MATAKULIAH KOMPUTER</th>
    </tr>
    @php
        $no = 1;
    @endphp
    @foreach ($komp as $itm)
    <tr>
        <td> {{ $no++.". ".$itm->matakuliah }} </td>
    </tr>
    @endforeach
</table>
</div>

<div style="width:48%;float:right">
    <table style="width:100%" cellspacing="0">
        <tr style="background-color: #cbd0d6">
            <th>MATAKULIAH NONKOMPUTER</th>
        </tr>
        @php
        $no = 1;
        @endphp
        @foreach ($nonkomp as $itm)
        <tr>
            <td> {{ $no++.". ".$itm->matakuliah }} </td>
        </tr>
        @endforeach
        @if (substr(session("th_ajaran"), -3) <> "roi")
        <tr>
            <td> {{ $no++.". Bahasa Inggris" }} </td>
        </tr>
        @if(substr($detkelompok[0]["NIM"], 5, 1) == 3)
        <tr>
            <td> {{ $no++.". Bahasa Jepang" }} </td>
        </tr>
        @endif
        @endif
    </table>
</div>
@if (substr(session("th_ajaran"), -3) == "roi")
<div style="width:50%;float:left;padding-top:10px;">
    <table style="width:100%" cellspacing="0">
        <tr style="background-color: #cbd0d6">
            <th>MATAKULIAH SPECIAL PURPOSE</th>
        </tr>
        @php
        $no = 1;
        @endphp
        @foreach ($sp as $itm)
        <tr>
            <td> {{ $no++.". ".$itm->matakuliah }} </td>
        </tr>
        @endforeach
    </table>
</div>
@endif

</body>
</html>