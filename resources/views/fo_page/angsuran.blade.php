@extends('fo_page.layout2')
@section('angsuran','active')
@section('header')
<h1>RINCIAN ANGSURAN</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Data Angsuran</a></li>
    <li class="active">angsuran</li>
</ol>
@endsection

@section('body')
<div class="row">
        <div class="col-lg-12">
            <div class="box box-primary direct-chat direct-chat-warning">
                <div class="box-body" style="padding:10px">
                    <h4><strong>Data Angsuran</strong></h4>
                    <a class="btn btn-primary" href="{{url('/angsuran/add.html')}}">Tambah Data</a>
                    <br>
                    <br>
                        <table class="table" id="angsuran">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th style="width:30%" class="cari">Jurusan</th>
                              <th style="width:5%" class="cari">Gelombang</th>
                              <th style="width:20%" class="cari">Angsuran Normal</th>
                              <th style="width:20%" class="cari">Angsuran Beasiswa</th>
                              <th style="width:25%">aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                          
                              <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                              </tr>
                          </tbody>
                        </table>
                </div>
            </div>
        </div>
        </div>
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function(){

  //untuk table pertama
    $('#tbkelompok thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var tbkelompok = $('#tbkelompok').DataTable();
    tbkelompok.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    //untuk table kedua
    $('#tbkelompok_diterima thead .cari').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" data-toggle="tooltip" title="'+title+'" style="width:100%" class="form-control txtCari" placeholder="'+title+'" />' );
    } );
    var tbkelompok_diterima = $('#tbkelompok_diterima').DataTable();
    tbkelompok_diterima.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

  $('#angsuran').DataTable();
  $('#tbkelompok_ditolakperusahaan').DataTable();

  if($('#sts_pengajuan').data("value") == 1){
      Swal.fire({
          title: 'Konfirmasi',
          text: 'Query Berhasil Dijalankan',
          type: 'success',
          confirmButtonText: 'OK'
      });
  }

});
</script>
@endsection
