@extends('fo_page.layout2')
@section('angsuran','active')
@section('header')
<h1>RINCIAN ANGSURAN</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/ojt')}}"><i class="fa fa-dashboard"></i> Data Angsuran</a></li>
    <li class="active">angsuran</li>
</ol>
@endsection

@section('body')
<section class="content">
  <div class="row">
    <div class="col-md-7">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Rincian Angsuran</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal">
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Jurusan</label>

              <div class="col-sm-10">
                  <select name="" id="jurusan" class="form-control">
                    <option value="">Pilih Salah Satu Jurusan</option>
                      @foreach ($tmpjursan as $jr)
                      <option value="{{$jr['KD_JURUSAN']}}"> {{$jr['jurusan']}} </option>
                      @endforeach
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Gelombang</label>

              <div class="col-sm-10">
                  <select name="" id="gel" class="form-control">
                    <option value="">Pilih Salah Satu Gelombang</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer" style="text-align: center">
            <button type="button" class="btn btn-primary btn-block" id="rinci">Rincian</button>
        
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
 

    </div>
    <div class="col-md-5">
      <div class="box box-primary">
        <div class="box-body">
          <div class="alert alert-danger alert-dismissible">
                <h4> Harap Diperhatikan!  <i class="icon fa fa-bullhorn"></i></h4>
           </div>
          <div class="callout callout-default">
            <h4>Silahkan Pilih Jurusan dan Klik Rincian untuk melihat rincian</h4>
          </div>
         
        </div>
        <!-- /.box-body -->
      </div>
    </div>

    <div class="modal" id="modalDelete">
      <div class="modal-dialog">
      <div class="modal-content">
  
      <!-- Modal Header -->
      <div class="modal-header">
          <h4 class="modal-title">Konfirmasi</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
  
      <!-- Modal body -->
      <div class="modal-body">
          Anda Yakin ?
      </div>
  
      <!-- Modal footer -->
      <div class="modal-footer">
          <a class="btn btn-danger" href="" id="btnYa">YA</a>
      </div>
  
      </div>
  </div>
  </div>
     
  </div>
  <!-- /.row -->

  <div class="box" id="tmpangsur">
  </div>



</section>
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>

   $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  function datatable() {
    $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  }

  $(document).ready(function() {
      $('#rinci').click(function() {
       var jurusan = $('#jurusan').val();
       var gel = $('#gel').val();
        $.ajax({
        url  : "{{url('/cekangsur')}}" + "/"+ jurusan + "/" + gel,
        type : "GET",
        success:function(data) {
          console.log(data);
          var coba = JSON.parse(data);
          console.log(coba);
          $('#tmpangsur').html(coba['data']);
          datatable();
        },
        error:function(data) {
          console.log(data);
        }

      });
      }); 

  });

</script>
<script>
  function hapusangsuran(idAngs){
    $('#btnYa').attr("href", idAngs);
    $('#modalDelete').modal();
}
</script>
<script>
  var status = "{{session('status')}}"
   if(status == 200){
         Swal.fire({
          title: 'Konfirmasi',
          text: 'Query Berhasil Dijalankan',
          type: 'success',
          confirmButtonText: 'OK'
      });
   }

</script>
<script>
 function deletex(kdjur,gel,angs){  
    Swal.fire({
        title:"apakah anda yakin ?",
        text:"data yang sudah dihapus tidak bisa dikembalikan lagi.",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        confirmButtonText:"Ya, saya yakin"
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url:"{{url('/angsuran/hapus.html/')}}"+"/"+kdjur+"/"+gel+"/"+angs,
                type:"get",
               
            })
            window.location.reload();
        }      
      window.location.reload();
    });
}
  </script>

@endsection
