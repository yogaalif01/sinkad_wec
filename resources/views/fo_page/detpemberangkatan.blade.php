@extends('fo_page.layout2')
@section('pemberangkatan','active')
@section('ojt','active')
@section('header')
<h1>Detail Kelompok OJT</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/fo')}}"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li><a href="{{url('/fo/pemberangkatan.html')}}">Data Kelompok</a></li>
    <li class="active">Detail Kelompok</li>
</ol>
@endsection
@section('body')
<input type="hidden" name="sts_pemberangkatan" id="sts_pemberangkatan" data-value="{{Session::get('sts_pemberangkatan')}}">
  <div class="row">
    <div class="col-lg-8">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">
                <form class="form-horizontal" action="{{url('fo/updatedetailpemberangkatan/'.$kelompok[0]['no_kelompok'])}}">
                   {{ csrf_field() }}
          
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>No Kelompok</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalSuccess" type="email"  class="form-control form-control-success" value="{{$kelompok[0]['no_kelompok']}}" readonly="true" name="no_kelompok">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Tanggal Pengajuan</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{ date("d-M-Y", strtotime($kelompok[0]['created_at'])) }}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Nama Perusahaan</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{$kelompok[0]['nama_perusahaan']}}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Alamat Perusahaan</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{$kelompok[0]['alamat']}}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>u.p.</b></label>
                    <div class="col-sm-9">
                      <input id="txtupperusahaan" style="width:60%;float:left" type="text"  class="form-control form-control-warning" value="{{$kelompok[0]['up']}}"> &nbsp;&nbsp;
                      <a href="{{ url('/fo/updateup/'.$kelompok[0]['no_kelompok']) }}" id="btnUpdateup" class="btn btn-primary btn-sm" style="margin-bottom:5px;">Update UP</a><br>
                      <span class="text-primary">Update data UP jika diperlukan</span>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>OJT di Bulan</b></label>
                    <div class="col-sm-9">
                      @php
                          $bulan = [ "1" => "Januari", "2"=> "Februari","3"=>"Maret","4"=>"April","5"=>"Mei","6"=>"Juni","7"=>"Juli","8"=>"Agustus","9"=>"September","10"=>"Oktober","11"=>"November","12"=>"Desember" ];
                      @endphp
                      <input id="inputHorizontalWarning" type="text"  class="form-control form-control-warning" value="{{ $bulan[$kelompok[0]['bulan']] }}" readonly="true">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b>Lama OJT</b></label>
                    <div class="col-sm-9">
                      <input id="inputHorizontalWarning" type="number" name="lamaojt"  class="form-control form-control-warning" readonly  value="{{ $kelompok[0]['lama'] }}" style="width:10%;float:left" > &nbsp;&nbsp;Bulan
                    </div>
                  </div>
              
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b></b></label>
                    <div class="col-sm-9">
                        <a class="btn btn-primary btn-sm" href=" {{ url("fo/amplop/".$kelompok[0]['no_kelompok']) }} " target="_blank"><i class="fa fa-print"></i> Amplop</a>

                        <a class="btn btn-primary btn-sm" href=" {{ url("fo/suratproposal/".$kelompok[0]['no_kelompok']) }} " target="_blank" id="btnSuratProposal"><i class="fa fa-print"></i> Surat Proposal</a>
                        
                        <a class="btn btn-primary btn-sm" href=" {{ url("fo/pengantar/".$kelompok[0]['no_kelompok']) }} " target="_blank" id="btnPengantar"><i class="fa fa-print"></i> Pengantar</a>
                        
                        <a class="btn btn-primary btn-sm" href=" {{ url("fo/pemberangkatan/".$kelompok[0]['no_kelompok']) }} " target="_blank" id="btnPemberangkatan"><i class="fa fa-print"></i> Pemberangkatan</a>                    
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label"><b></b></label>
                    <div class="col-sm-9">
                      <hr>
                        <a class="btn btn-danger btn-sm" href=" {{ url("fo/suratproposalx/".$kelompok[0]['no_kelompok']) }} " target="_blank" id="btnSuratProposal"><i class="fa fa-print"></i> Surat Proposal (Cetak Beserta Kopnya) </a>
                        
                        <a class="btn btn-danger btn-sm" href=" {{ url("fo/pemberangkatanx/".$kelompok[0]['no_kelompok']) }} " target="_blank" id="btnPemberangkatan"><i class="fa fa-print"></i> Pemberangkatan (Cetak Beserta Kopnya)</a>                    
                    </div>
                  </div>

           
            </form>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary direct-chat direct-chat-warning">
            <div class="box-body" style="padding:10px">

                <h4>Anggota Kelompok</h4>
                <div class="table-responsive" style="margin-left:15px;margin-right: 15px;width: 90%;">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>NIM</th>
                          <th>Nama Mahasiswa</th>
                          <th>Gelombang</th>
                          <th>Absensi</th>
                          <th>Kelas</th>
                          <th>aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no=1 ?>
                        @foreach($detail as $dt)
                        <tr>
                          <td>{{$no++}}</td>
                          <td>{{$dt['nim']}}</td>
                          <td>{{$dt['NAMA']}}</td>
                          <td>{{$dt['GEL']}}</td>
                          <td>{{$dt['persen']}} %</td>
                          <td>{{$dt['KELAS']}}</td>
                          <td>
                            <button class="btn btn-primary btn-sm" onclick="lihatangsuran({{$dt['nim']}})">History Pembayaran</button>
                          </td>
                        </tr>

                        @endforeach
                            
                      </tbody>
                    </table>
                  </div>

            </div>
        </div>
    </div>
</div>
  
{{-- modal lihatnilai --}}
<div class="modal fade" id="Modallihatangsuran">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">History Pembayaran</h4>
      </div>
      <div class="modal-body">
          <div id="tb_lihatangsuranCover">

          </div>
          

      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


@endsection

@section('css')

@endsection

@section('script')
<script>
$(document).ready(function(){

  if($('#sts_pemberangkatan').data("value") == 1){
      Swal.fire({
          title: 'Konfirmasi',
          text: 'Query Berhasil Dijalankan',
          type: 'success',
          confirmButtonText: 'OK'
      });
  }

  $("#btnSuratProposal").click(function(e){
    e.preventDefault();
    var cekpengajuan = "{{ $kelompok[0]['apr_pengajuan'] }}";
    var cekpengajuan2 = "{{ $kelompok[0]['apr_pengajuan2'] }}";
    var url = $(this).attr("href");
    if(cekpengajuan != 1){
      Swal.fire({
          title: 'Kelompok ini belum ke TIM OJT untuk persetujuan pengajuan',
          text: '-',
          type: 'warning',
          confirmButtonText: 'OK'
      });  
    }else{
      if(cekpengajuan2 == 1){
        Swal.fire({
          title:"Dokumen ini sudah pernah dicetak untuk kelompok ini. <br> Ingin Cetak Lagi ?",
          text:"",
          icon:"warning",
          showCancelButton:true,
          confirmButtonColor:"#3085d6",
          cancelButtonColor:"#d33",
          cancelButtonText:"TIDAK",
          confirmButtonText:"YA"
        }).then((result)=>{
          if(result.value){
            window.open(url,"_blank");      
          }
        });
      }else{
        window.open(url,"_blank");
      }
      
    }
  });

  $("#btnPengantar").click(function(e){
    e.preventDefault();
    var cekpengajuan = "{{ $kelompok[0]['apr_pengajuan'] }}";
    var cekpengajuan2 = "{{ $kelompok[0]['apr_pengajuan2'] }}";
    var url = $(this).attr("href");
    if(cekpengajuan != 1){
      Swal.fire({
          title: 'Kelompok ini belum ke TIM OJT untuk persetujuan pengajuan',
          text: '-',
          type: 'warning',
          confirmButtonText: 'OK'
      });  
    }else{
      if(cekpengajuan2 == 1){
        Swal.fire({
          title:"Dokumen ini sudah pernah dicetak untuk kelompok ini. <br> Ingin Cetak Lagi ?",
          text:"",
          icon:"warning",
          showCancelButton:true,
          confirmButtonColor:"#3085d6",
          cancelButtonColor:"#d33",
          cancelButtonText:"TIDAK",
          confirmButtonText:"YA"
        }).then((result)=>{
          if(result.value){
            window.open(url,"_blank");      
          }
        });
      }else{
        window.open(url,"_blank");
      }
      
    }
  });
});

$("#btnPemberangkatan").click(function(e){
  e.preventDefault();
  var cekpemberangkatan   = "{{ $kelompok[0]['apr_pemberangkatan1'] }}";
  var cekpemberangkatan21 = "{{ $kelompok[0]['apr_pemberangkatan21'] }}";
  var cekpemberangkatan22 = "{{ $kelompok[0]['apr_pemberangkatan22'] }}";
  var cekpemberangkatan3  = "{{ $kelompok[0]['apr_pemberangkatan3'] }}";
  var nip2                = "{{ $kelompok[0]['nip2'] }}";
  var url = $(this).attr("href");
  if(cekpemberangkatan != 1){
    Swal.fire({
        title: 'Kelompok ini belum menemui TIM OJT untuk persetujuan pemberangkatan',
        text: '-',
        type: 'warning',
        confirmButtonText: 'OK'
    });  
  }else if(cekpemberangkatan21 != 1){
    Swal.fire({
        title: 'Kelompok ini belum ke Pembimbing 1 untuk persetujuan pemberangkatan',
        text: '-',
        type: 'warning',
        confirmButtonText: 'OK'
    });  
  }else if(nip2 != ""){
    if(cekpemberangkatan22 != 1){
      Swal.fire({
          title: 'Kelompok ini belum ke Pembimbing 2 untuk persetujuan pemberangkatan',
          text: '-',
          type: 'warning',
          confirmButtonText: 'OK'
      });
    }else{
      window.open(url,"_blank");
    }
      
  }else{
    if(cekpemberangkatan3 == 1){
      Swal.fire({
        title:"Dokumen ini sudah pernah dicetak untuk kelompok ini. <br> Ingin Cetak Lagi ?",
        text:"",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        cancelButtonText:"TIDAK",
        confirmButtonText:"YA"
      }).then((result)=>{
        if(result.value){
          window.open(url,"_blank");      
        }
      });
    }else{
      window.open(url,"_blank");
    }
    
  }
});

$("#btnUpdateup").click(function(e){
  e.preventDefault();
  var up = 0;
  if($("#txtupperusahaan").val() != ""){
    up = $("#txtupperusahaan").val();
  }
  var url = $(this).attr("href")+"/"+up;
  window.location = url;
});

function lihatangsuran(nim){
    var formatter = new Intl.NumberFormat(['ban', 'id']);

    $.ajax({
        url:"{{url('')}}/fo/lihatangsuran/"+nim,
        type:"GET",
        success:function(r){
            console.log(r);
            var r = JSON.parse(r);
            $("#tb_lihatangsuranCover").empty()
            var table = "<table id='tb_angsuran' class='table table-responsive table-striped' width='100%'>";
            table += "<thead><tr>";
              table += "<th>#</th>";
            table += "<th>Tanggal</th>";
            table += "<th>NO_KW</th>";
            table += "<th>KETERANGAN</th>";
            table += "<th>JUMLAH</th>";
            table += "<th>OPERATOR</th>";
            table += "</tr></thead>";
            table += "<tbody>";
                for(var i = 0; i < r["dangsuran"].length; i++){
                    var no = i+1;
                    table += "<tr><td>"+ no +"</td><td>"+r["dangsuran"][i]["TGL_ANS"]+"</td><td>"+r["dangsuran"][i]["NO_KW"]+"</td><td>"+r["dangsuran"][i]["KETERANGAN"]+"</td><td>"+ formatter.format(r["dangsuran"][i]["JUMLAH"])+"</td><td>"+r["dangsuran"][i]["OPERATOR"]+"</td></tr>";
                }
            table += "</tbody>";
            table += "</table>";
            $("#tb_lihatangsuranCover").html(table);
            // $("#tb_angsuran").DataTable();
            $("#Modallihatangsuran").modal();
        },
        error:function(e){
            alert("Terjadi Kesalahan !");
            console.log(e.responseText);
        }
    })
}
</script>
@endsection