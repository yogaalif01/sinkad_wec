@extends('mhs_page.layout2')
@section('ojt', 'active')
@section('pengajuan','active')
@section('header')
<h1>
Pengajuan Kelompok OJT / TA
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/mahasiswa/index.html')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Status Pengajuan</li>
</ol>
@endsection
@section('body')
<div class="row">
<div class="col-lg-4">
    <div class="box box-primary alert alert-danger">
        <div class="box-body" style="padding:10px">
            <strong class="text-white">PENGAJUAN GAGAL DILAKUKAN</strong>
        </div>
    </div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
    <div class="box box-danger direct-chat direct-chat-warning">
        <div class="box-body" style="padding:10px">
            <h4><b>Hai {{ session("nama_mhs2") }}, Pengajuan kelompok OJT gagal dilakukan. Silahkan cek kembali absensi dan angsuran kamu. Pastikan sudah sesuai dengan persyaratan OJT. </b></h4>
        </div>
    </div>
</div>
</div>
@endsection