@extends('mhs_page.layout2')
@section('angsuran','active')
@section('header')
<h1>
Angsuran
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/mahasiswa/index.html')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Angsuran</li>
</ol>
@endsection
@section('body')
<div class="row">
    <div class="col-lg-4">
        <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-money"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Total Angsuran Harus Dibayar</span>
            <span class="info-box-number"><strong>Rp{{number_format($angsuran["BYR_BERSIH"],0,".",",")}}</strong></span>
        </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Total Angsuran Sudah Dibayar</span>
            <span class="info-box-number"><strong>Rp{{number_format($angsuran["TOTAL_BYR"],0,".",",")}}</strong></span>
        </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Sisa Angsuran Harus Dibayar</span>
            <span class="info-box-number"><strong>Rp{{number_format($angsuran["PIUTANG"],0,".",",")}}</strong></span>
        </div>
        </div>
    </div>
</div>

<div class="row">
<div class="col-lg-12">
    
    <div class="box box-success">
    <div class="box-body" style="padding:10px">
    
        <h4>Hai {{ session('nama_mhs2') }}, Berikut kami tampilkan riwayat pembayaran anda</h4>
        <div class="table-responsive">                       
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Tanggal</th>
                <th>Total Bayar</th>
                <th>No. Kwitansi</th>
                <th>Keterangan</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($detail->get() as $detail)
            <tr>
                <td>{{$detail["TGL_ANS"]}}</td>
                <td>Rp{{ number_format($detail["JUMLAH"],0,".",",")}}</td>
                <td>{{$detail["NO_KW"]}}</td>
                <td>{{$detail["KETERANGAN"]}}</td>
            </tr>        
            @endforeach
            </tbody>
        </table>
        </div>

    </div>
    </div>


</div>
</div>
@endsection

    