<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistem Informasi Akademik</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('lte2/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('lte2/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('lte2/bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('lte2/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('lte2/dist/css/skins/_all-skins.min.css')}}">

  <link rel="stylesheet" href="{{asset('swal/sweetalert2.css')}}">
  <link rel="shortcut icon" href="{{asset('images/head logo.png')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

@yield('css')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>W</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SINKAD</b>WEC</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{asset('images/user.jpg')}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ session("nama_mhs2") }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{asset('images/user.jpg')}}" class="img-circle" alt="User Image">

                <p>
                  {{ session("nama_mhs") }}
                  <small>( {{session("kelas")." - ".session("th_ajaran")}} )</small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{url('/mahasiswa/pengaturan.html')}}" class="btn btn-default btn-flat">Profil</a>
                </div>
                <div class="pull-right">
                  <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Keluar</a>
                </div>
              </li>
            </ul>
          </li>
         
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('images/user.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ session("nama_mhs2") }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <li class="@yield('index')">
            <a href="{{url('mahasiswa/index.html')}}"> <i class="fa fa-home"></i><span>Beranda</span></a>
        </li>
        <li class="@yield('nilaikelas')">
            <a href="{{url('mahasiswa/nilaikelas.html')}}"> <i class="fa fa-tasks"></i><span>Rata-rata Nilai </span></a>
        </li>
        <li class="@yield('absensi')">
            <a href="{{url('mahasiswa/absensi.html')}}"> <i class="fa fa-file-excel-o"></i><span>Absensi</span> </a>
        </li>
        <li class="@yield('angsuran')">
            <a href="{{url('mahasiswa/angsuran.html')}}"> <i class="fa fa-money"></i><span>Angsuran </span></a>
        </li>
       <!--  <li class="@yield('uploadKeg')">
            <a href="{{url('mahasiswa/kegiatan.html')}}"> <i class="fa fa-users"></i><span>Upload Kegiatan Kelas</span> </a>
        </li> -->
        @if (Session::get("ecc") == 1)
        <li class="@yield('ecc')">
            <a href="{{url('mahasiswa/ecc.html')}}"> <i class="fa fa-users"></i><span>E C C</span></a>
        </li>
        @endif  
        <li class="@yield('ojt') treeview">
          <a href="#">
            <i class="fa fa-exchange"></i> <span>OJT/TA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@yield('pengajuan')">
              <a href="{{url('mahasiswa/pengajuan.html')}}"><i class="fa fa-circle-o"></i> Pengajuan Kelompok</a>
            </li>
            <li class="@yield('statusKelompok')">
              <a href="{{url('mahasiswa/status kelompok.html')}}"><i class="fa fa-circle-o"></i> Status Kelompok</a>
            </li>
          </ul>
        </li>
      
        <li><a href="{{url('/')}}"> <i class="fa fa-reply"></i><span>Landing Page</span> </a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @yield('header')
      {{-- <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol> --}}
    </section>

    <!-- Main content -->
    <section class="content">
    <input type="hidden" name="" id="base_url2" value="{{url('/')}}">
      @yield('body')
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>TIM ASDOS WECMLG</b> 2019
    </div>
    <strong>Wearnes Education Center Malang</strong> 
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('lte2/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('lte2/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('lte2/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('lte2/dist/js/adminlte.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('lte2/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap  -->
<!-- SlimScroll -->
<script src="{{asset('lte2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- ChartJS -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="{{asset('lte2/dist/js/pages/dashboard2.js')}}"></script> --}}
<!-- AdminLTE for demo purposes -->
<script src="{{asset('lte2/dist/js/demo.js')}}"></script>
<script src="{{asset('swal/sweetalert2.js')}}"></script>
@yield('script')
</body>
</html>
