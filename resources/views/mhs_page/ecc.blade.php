@extends('mhs_page.layout2')
@section('ecc','active')
@section('header')
<h1>
English Conversation Club
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/mahasiswa/index.html')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">ECC</li>
</ol>
@endsection

@section('body')	
<div class="row">
<div class="col-lg-12">
	@if(Session::has("status_ecc"))
		@if(session('status_ecc') == 2)
			<div class="alert alert-danger">
				Maaf. Sepertinya anda sudah terdaftar
			</div>
		@elseif(session('status_ecc') == 3)
			<div class="alert alert-info">
				Maaf. Kuota sudah habis
			</div>
		@else
			<div class="alert alert-info">
				Selamat. Pendaftaran anda sebagai anggota ECC telah berhasil.
			</div>
		@endif
	@else
		<div class="alert alert-info">
			* Kuota terbatas. segera daftarkan diri anda. <br>
			* Jadwal ECC Berikutnya :  <b>{{ date("m F Y", strtotime($jadwal[0]["tanggal"]))." &nbsp;".$jadwal[0]["jam"]." WIB &nbsp;( ".$jadwal[0]["tempat"]." )"}}</b>
			
		</div>
	@endif	
</div>
<div class="col-lg-12">
<a href="{{url('/mahasiswa/eccpost')}}">
	<button class="btn btn-primary btn-sm">
		D A F T A R
	</button>
</a>
</div>
</div>
<br>
<div class="row">
<div class="col-lg-12">

	<div class="box box-success">
	<div class="box-body" style="padding:10px">
		<h4>Daftar Peserta ECC</h4>
		@if(count($ecc) == 0)
			<div class="alert alert-warning" style="width: 100%;text-align: center;">Belum ada pendaftar</div>
		@else
			<div class="col-lg-12">
				<div class="table-responsive">                       
			    <table class="table table-striped table-hover" id="tbecc">
			      <thead>
			        <tr>
			          <th width="10%">#</th>
			          <th width="20%">NIM</th>
			          <th width="50%">NAMA</th>
			          <th width="20%">KELAS</th>
			        </tr>
			      </thead>
			      <tbody>
			      @php
			      $no =1;
			      @endphp
			      @foreach($ecc as $dataecc)
			      <tr>
			      	<td>{{$no}}</td>
			      	<td>{{$dataecc['NIM']}}</td>
			      	<td>{{$dataecc['NAMA']}}</td>
			      	<td>{{$dataecc['KELAS']}}</td>
			      </tr>
			      @php
			      $no++;
			      @endphp
			      @endforeach
			      </tbody>
				</table>
		    	</div>
			</div>
		@endif

	</div>
	</div>

</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
$(document).ready(function(){
	$('#tbecc').DataTable();
});
</script>	
@endsection

