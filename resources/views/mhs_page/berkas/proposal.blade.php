<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proposal</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
     <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('lte2/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <style>
    body{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        /* display:inline; */
    }
    @page {
    header: page-header;
    footer: page-footer;
    }

    .headerr{
        color:red;
        vertical-align: bottom;
    }

    .bold{
        font-weight: bold !important;
    }

    p{
        text-align: justify;
        line-height:24pt;
    }

    li{
        text-align: justify;
        line-height:24pt;
    }

    td, th {
        border:1px solid black;
        padding-left: 5px;
        font-size: 13px;
    }

    #dperusahaan td,  {
        border:0px;
        height: 25px;
        font-size: 14px;
        /* padding-left: 5px; */
    }

    .vl {
    border-left: 2px dashed black;
    height: 100%;
    }
    </style>
</head>
<body>

<htmlpageheader name="page-header">
    <div style="text-align:right;padding-top:50px;vertical-align:bottom"> 
        Form Model : 0412 - No Kelompok : {{ $kelompok[0]["no_kelompok"] }}
        <hr>
    </div>
</htmlpageheader>

<htmlpagefooter name="page-footer">
    <div style="height:20px;text-align:left;padding-bottom:40px;"> 
        <hr>
        <i> Proposal OJT Wearnes Education Center</i>
    </div>
</htmlpagefooter>


<div class="row">
<div class="col-lg-12">
    <img class="img img-responsive" width="110%" src="{{ url("storage/app/berkas/cover proposal wec ".substr($kelompok[0]["no_kelompok"], 0, 4).".jpg") }}" alt="" srcset="">
    
</div>
</div>
<pagebreak />
<div class="row" style="padding-top:25px;">
<div class="col-lg-12 text-center">
    <h3 class="bold">
    PROPOSAL <br>
    PELAKSANAAN “ON THE JOB TRAINING“ (MAGANG)
    </h3>
    <br>
    <br>
</div>
<div class="col-lg-12">
<ol type="I">
    <li class="text-uppercase bold" ><h4 class="bold">Latar Belakang</h4></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Era Globalisasi (Pasar Bebas) tahun 2004 lalu dan Pada tahun 2017 ini Indonesia juga telah memasuki MEA (Masyarakat Ekonomi ASEAN). Dengan perkembangan dan keadaan tersebut Wearnes Education Center International School of Computer & Business yaitu Lembaga Pendidikan Profesi 1 Tahun dituntut untuk selalu tanggap dan siap terhadap segala perubaan yang akan terjadi. Indonesia sebagai negara yang sedang berkembang dan mempunyai penduduk yang besar secara kuantitas yang memiliki keunggulan komparatif, tentunya dalam era pasar bebas merupakan pasar yang sangat menjanjikan bagi perkembangan investasi baik Investasi Asing (PMA) maupun Lokal (PMDN) yang akan memacu persaingan tenaga kerja, terutama tenaga kerja dalam negeri.</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tantangan dalam menghadapi persaingan yang tidak bisa dihindarkan ini menuntut kita untuk lebih keras lagi melakukan perubahan di bidang pengembangan Sumber Daya Manusia (SDM) khususnya Sumber Daya Manusia (SDM) yang mampu memiliki jiwa Profesional Character Building, agar tercipta Sumber Daya Manusia yang tidak hanya unggul secara kuantitas, tetapi unggul secara kualitas, sehingga tidak hanya menjadi obyek dalam menghadapi Era Pasar Bebas dan MEA (Masyarakat Ekonomi ASEAN), tetapi benar-benar terlibat secara langsung di Era Pasar Bebas dan MEA (Masyarakat Ekonomi ASEAN) tersebut</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salah satu cara meningkatkan sumber daya yang nantinya diharapkan mampu bersaing dengan tenaga kerja asing, pola yang ditempuh sebagai daya dukung dalam peningkatan kapasitas sumber daya manusia tersebut adalah dengan penerapan system pendidikan ganda yang mengembangkan konsep Link and Match,yang mana konsep ini menerapkan pola sinergi antara pendidikan teoritis dengan aplikatif (praktek).</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pembentukan SDM yang memiliki jiwa Profesional Character Bulidng tidak hanya melalui praktek sesuai dengan kurikulum dalam pendidikan tapi juga membutuhkan paktek kerja langsung berhadapan dengan Dunia Kerja di berbagai macam jenis Usaha, baik Jasa, Perdagangan dan Industri yang sangat heterogen (berbeda satu dengan yang lain).</p>
    
    
    <li class="text-uppercase bold"><h4 class="bold">tujuan</h4></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Tujuan pelaksanaan On the Job Training atau magang di perusahaan atau instansi adalah sebagai berikut:
    </p>
    <ol>
        <li>Memberikan kesempatan kepada mahasiswa untuk menerapkan keterampilan dan keahlian yang didapatkan selama mengikuti perkuliahan teori dan praktik pada suatu bidang kerja yang relevan.</li>
        <li>Meningkatkan kemampuan mahasiswa sebagai calon profesional madya.</li>
        <li>Memperlancar tugas dan pekerjaan yang diberikan perusahaan atau instansi tempat mahasiswa melaksanakan On The job Training atau magang.</li>
        <li>Merintis pola kerja sama dengan bentuk penawaran sumber daya manusia dari lulusan Wearnes Education Center pada instansi atau perusahaan mitra On The Job Training.</li>
    </ol>
    <li class="text-uppercase bold"><h4 class="bold">BENTUK PELAKSANAAN</h4></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Adapun bentuk pelaksanaan On the Job Training atau magang yang dilakukan oleh mahasiswa profesi satu tahun Wearnes Education Center adalah mahasiswa tersebut benar-benar melakukan praktik Kerja pada perusahaan atau instansi sesuai dengan keterampilan dan keahlian yang dimiliki dan mematuhi segala peraturan yang berlaku pada perusahaan atau instansi, bukan melakukan observasi (penelitian) atau pengumpulan data dalam penyusunan tugas akhir (bukan untuk menyusun skripsi seperti program S1). Namun demikian, mahasiswa yang telah mengikuti kegiatan OJT diwajibkan membuat laporan OJT secara konstruktif dan dapat menghasilkan sebuah Karya Program Aplikasi Komputer yang sesuai dengan aturan penyusunan laporan OJT oleh Wearnes Education Center Malang dan OJT di instansi atau perusahaan tersebut.
    </p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Bentuk OJT dapat dilakukan dengan cara bekerja sesuai dengan jam kerja yang ada pada perusahaan atau instansi, akan tetapi dapat diberlakukan cara lain, bila suatu kondisi tidak memungkinkan, misalnya Program/Software/Hardware atau Sarana Prasarana yang dibutuhkan tidak tersedia atau dipergunakan untuk operasional Perushaan/Instansi, maka apabila dalam hal demikian bentuk OJT dapat dilakukan dengan cara mengadakan wawancara atau interview dan Observasi atau survey mengenai Sistem yang ada pada perusahaan dan instansi, untuk proses pekerjaan-nya dapat dilakukan diluar perusahaan/instansi, misalnya kembali ke Kampus.
    </p>
    <li class="text-uppercase bold"><h4 class="bold">WAKTU PELAKSANAAN</h4></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Waktu pelaksanaan On the Job Training (Magang) dilaksanakan selama 1 (satu) sampai dengan 2 (dua) bulan, yang mana pelaksanaannya dapat dimulai antara bulan April, Mei, Juni, Juli, atau Agustus 2021, dengan alasan kurikulum perkuliahan sudah disampaikan 85%.
    </p>
    <li class="text-uppercase bold"><h4 class="bold">PESERTA DAN JUMLAH PESERTA</h4></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Peserta yang melaksanakan On the Job Training adalah mahasiswa program profesi satu tahun dari 5 jurusan yang dimiliki Wearnes Education Center yang telah mengikuti mata kuliah komputer dan matakuliah non komputer. Adapun jumlah peserta On the Job Training adalah maksimal 5 mahasiswa setiap kelompoknya dan jika perusahaan/instansi yang bersangkutan dapat menerima peserta OJT lebih dari 5 mahasiswa, maka kelompoknya akan disesuaikan. Dalam pelaksanaan On the Job Training bisa dengan cara bertahap sesuai dengan jumlah mahasiswa yang disetujui oleh pihak perusahaan/instansi.
    </p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Adapun dalam kelompok ini, anggotanya adalah :
        <table width="80%" class="table">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    <th width="15%">NIM</th>
                    <th width="25%">Nama</th>
                    <th>Jurusan</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach ($kelompok as $item)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $item["NIM"] }}</td>
                    <td>{{ $item["NAMA"] }}</td>
                    <td>{{ $item["jurusan"] }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </p>
    
    <li class="text-uppercase bold"><h4 class="bold">format presensi dan penilaian</h4></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Pada waktu pelaksanaan On the Job Training pihak Wearnes akan menyediakan format presensi dan penilaian yang akan digunakan oleh pihak perusahaan/instansi untuk mengontrol dan menilai aktivitas yang dikerjakan oleh mahasiswa peserta On the Job Taining.
    </p>
    <li class="text-uppercase bold"><h4 class="bold">penutup</h4></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Demikianlah Proposal On the Job Training ini kami buat dan sekiranya dapat dijadikan sebagai tindak lanjut hubungan kerja sama yang baik antara pihak Wearnes Education Center dengan pihak Perusahaan Swasta, BUMN dan Instansi Pemerintah untuk bersama-sama mencerdaskan kehidupan bangsa dan membangun Sumber Daya Manusia Indonesia yang berpotensi, mampu bersaing dan menjadi tuan rumah di negerinya sendiri, hal ini adalah suatu bentuk kepedulian kita bersama demi masa depan gemilang Putra dan Putri Bangsa. Selanjutnya atas perhatian, kerja sama dan kesempatan yang diberikan kami Mengucapkan Terima Kasih.
        <div style="float:left;width:65%">&nbsp;</div>
        <div style="float:right;margin-top:80px;">
            <u>Shaiful Horry, SE.</u><br>
            Ketua Team OJT/TA
        </div>
    </p>
</ol>
</div>
</div>
<pagebreak />
<div class="row" style="padding-top:25px;">
<div style="float:left;width:5%">
    <img style="transform:rotate(90deg);" src="{{ asset("images/guntingmu.png") }}" class="img" alt="" srcset="">
    <div class="vl" style="z-index:-1"></div>
</div>
<div style="width:90%">
    <h4 class="text-center bold">
        LEMBAR JAWABAN <br>
        PENERIMAAN (OJT) ON THE JOB TRAINING
    </h4>
    <br>
    <table class="table" id="dperusahaan">
        <tr>
            <td style="width:25%">Nama Perusahaan</td>
            <td>: {{ $kelompok[0]["nama_perusahaan"] }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>: {{ $kelompok[0]["alamat"] }}</td>
        </tr>
        <tr>
            <td>Telp/Fax</td>
            <td>:</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
        </tr>
        <tr>
            <td>Contact Person</td>
            <td>:</td>
        </tr>
    </table>
    <p style="font-size:14px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Sehubungan dengan Proposal OJT dari Wearnes Education Center, kami telah menyetujui dan menerima permohonan OJT atau Magang untuk mahasiswa/wi Jurusan :    
        <ol>
            @php
                $no = 1;
            @endphp
            @foreach ($jurusan as $item)
            @if ($no == 1)
            <li style="font-size:12px;"> {{ $item->jurusan }} <br> ( ____  Peserta )</li>
            @else
            <li style="font-size:12px;"> {{ $item->jurusan }} &nbsp; ( ____  Peserta )</li>
            @endif
            @php
                $no++;
            @endphp
            @endforeach
        </ol>
    </p>
    <p style="font-size:14px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Pelaksanaan OJT atau Magang akan dilaksanakan dalam periode waktu antara Bulan April, Mei, Juni, Juli atau Agustus. Adapun Persetujuan dan Penerimaan Mahasiswa/wi Wearnes Education Center, berlaku dengan ketentuan atau persyaratan sebagai berikut :
        ______________________________________________________________________________________________________________________________________________
    </p>
    <div style="float:left;width:40%;margin-top:10px;font-size:12px">
        <br><br><br><br><br>
       <strong style="width:70%"> Lembar ini dapat di Fax ke (0341) 588015 <br>
        Atau melalui Email </strong>
    </div>
    <div class="text-center">
        <br>
        ....................... , ...........................
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        ( ................................................. )
    </div>
    
   
</div>
</div>


<!-- jQuery 3 -->
<script src="{{asset('lte2/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('lte2/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
</body>
</html>