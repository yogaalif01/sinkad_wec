<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proposal</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
     <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('lte2/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <style>
    body{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
        /* display:inline; */
    }
    @page {
	header: page-header;
	footer: page-footer;
    }

    .headerr{
        color:red;
        vertical-align: bottom;
    }

    .bold{
        font-weight: bold !important;
    }

    p{
        text-align: justify;
        line-height:24pt;
    }

    li{
        text-align: justify;
        line-height:24pt;
    }

    td, th {
        border:1px solid black;
        padding-left: 5px;
        font-size: 13px;
    }

    #dperusahaan td,  {
        border:0px;
        height: 25px;
        font-size: 15px;
        /* padding-left: 5px; */
    }

     #droi td,  {
        border:0px;
        height: 25px;
        font-size: 15px;
        /* padding-left: 5px; */
    }

    .vl {
    border-left: 2px dashed black;
    height: 100%;
    }
    </style>
</head>
<body>

<htmlpageheader name="page-header">
    <div style="text-align:right;padding-top:50px;vertical-align:bottom;font-size:12px;padding-bottom:20px;"> 
        Form Model : 0412R - No Kelompok : {{ $kelompok[0]["no_kelompok"] }}
        <hr>
    </div>
</htmlpageheader>

<htmlpagefooter name="page-footer">
    <div style="height:20px;text-align:left;padding-bottom:30px;font-size:12px;"> 
        <hr>
        <i> Proposal OJT Royal Ocean International</i>
    </div>
</htmlpagefooter>


<div class="row">
<div class="col-lg-12">
    <img class="img img-responsive" style="padding-top: 50px;" width="120%" src="{{ url("storage/app/berkas/cover proposal roi ".substr($kelompok[0]["no_kelompok"], 0, 4).".jpg") }}" alt="" srcset="">
</div>
</div>
<pagebreak />
<div class="row" style="padding-top:25px;">
<div class="col-lg-12 text-center">
    <h3 class="bold">
    PROPOSAL <br>
    PELAKSANAAN “ON THE JOB TRAINING“ (MAGANG)
    </h3>
    <br>
    <br>
</div>
<div class="col-lg-12">
<ol type="I">
    <li class="text-uppercase bold"><h5 class="bold">Latar Belakang</h5></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berawal dari pengalaman <span class="bold">Wearnes Education Center</span> International School of Computer & Business sebagai sebuah <span class="bold">Lembaga Pendidikan Profesi 1 Tahun</span> yang selama ini telah mampu mencetak Sumber Daya Manusia (SDM) Profesional Mitra Dunia Kerja di Perusahaan/Instansi yang bergerak di bidang Jasa, Perdagangan dan Industri. Tuntutan perusahaan Mitra Kerja terhadap <span class="bold">Wearnes Education Center</span> terutama Industri di Bidang Perhotelan dan Kapal Pesiar agar <span class="bold">Wearnes Education Center</span> mendirikan Pendidikan 1 Tahun yang fokus dalam Bidang Perhotelan dan Kapal Pesiar, maka didirikanlah <span class="bold">Royal Ocean International</span>, sebuah <span class="bold">Lembaga Pusat Pendidikan 1 tahun Perhotelan dan Kapal Pesiar</span> yang mempelajari ilmu perhotelan serta kapal pesiar yang dipadukan dengan <span class="bold">Aplikasi Komputer, Manajemen Keuangan dan Bahasa Inggris</span>. Kehadiran <span class="bold">Royal Ocean International</span> adalah untuk pemenuhan kebutuhan SDM dibidang perhotelan dan kapal pesiar yang sampai saat ini masih terbuka lebar karena kebutuhannya lebih besar dari pada ketersediaannya. Setiap tahunnya kebutuhan SDM untuk perhotelan dan kapal pesiar adalah sebesar 2800 orang namun yang terpenuhi tidak lebih dari 1200 orang.</p>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Untuk menjaga kualitas SDM dan lancarnya penyerapan SDM di dunia kerja, <span class="bold">Royal Ocean International</span> telah menjalin kerjasama dalam bentuk penandatanganan MoU (Memorandum of Understanding) dengan hotel-hotel berbintang yang tersebar di Seluruh Indonesia.</p>
    
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selain itu kami, <span class="bold">Royal Ocean International</span> juga menjalin kerja sama dengan agen-agen internasional yang akan memberangkatkan mahasiswa <span class="bold">Royal Ocean International</span> bekerja di kapal pesiar.</p>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Perkuliahan di <span class="bold">Royal Ocean International</span> ( ROI ) dilaksanakan selama 1 tahun dengan <span class="bold">POWERFUL TEACHING METHOD</span> yaitu terdiri dari 6 bulan perkuliahan teori dan praktikum di Kampus secara berimbang, 6 bulan berikutnya OJT/Magang di hotel ber bintang yang menjadi mitra kerja <span class="bold">Royal Ocean International</span>. Perkuliahan di <span class="bold">Royal Ocean International</span> diberikan oleh dosen-dosen pilihan dibidang perhotelan dan kapal pesiar serta dosen praktisi dari perhotelan dan kapal pesiar juga yang rata-rata memiliki pengalaman kerja diatas 10 tahun dibidangnya masing-masing.</p>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salah satu cara meningkatkan sumber daya yang nantinya diharapkan mampu bersaing dengan tenaga kerja asing, pola yang ditempuh sebagai daya dukung dalam   
    peningkatan kapasitas sumber daya manusia tersebut adalah dengan penerapan system pendidikan ganda 
    yang mengembangkan konsep Link and Match, yang mana konsep ini menerapkan pola sinergi antara pendidikan teoritis dengan aplikatif (praktek).</p>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pembentukan SDM yang memiliki jiwa Professional Character Bulidng tidak hanya melalui tori sesuai dengan kurikulum dalam pendidikan tapi juga membutuhkan praktek kerja langsung berhadapan dengan Dunia Kerja terutama di Bidang Perhotelan dan Kapal Pesiar yang sangat heterogen (berbeda satu dengan yang lain).</p>
    
    </ol>
    <ol type="I" start="2">
    <li class="text-uppercase bold"><h5 class="bold">tujuan</h5></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Tujuan pelaksanaan On the Job Training atau magang di perusahaan atau instansi adalah sebagai berikut:
    </p>
    <ol type="a">
        <li>Memberikan kesempatan kepada mahasiswa untuk menerapkan keterampilan dan keahlian yang didapatkan selama mengikuti perkuliahan teori dan praktik pada suatu bidang kerja yang relevan.</li>
        <li>Meningkatkan kemampuan mahasiswa sebagai calon profesional madya.</li>
        <li>Memperlancar tugas dan pekerjaan yang diberikan Hotel Mitra Kerja tempat mahasiswa melaksanakan On The job Training atau magang.</li>
        <li>Merintis pola kerja sama dengan bentuk penawaran sumber daya manusia dari lulusan <span class="bold">Royal Ocean International</span> pada instansi atau perusahaan mitra On The Job Training.</li>
    </ol>
    <li class="text-uppercase bold"><h5 class="bold">BENTUK PELAKSANAAN</h5></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Adapun bentuk pelaksanaan On the Job Training atau magang yang dilakukan oleh mahasiswa <span class="bold">Pusat Pendidikan 1 tahun Perhotelan dan Kapal Pesiar Royal Ocean International </span> adalah mahasiswa tersebut benar-benar melakukan praktek Kerja pada Industri Perhotelan sesuai dengan keterampilan dan keahlian yang dimiliki dan mematuhi segala peraturan yang berlaku pada Hotel Mitra Kerja, bukan melakukan observasi (penelitian) atau pengumpulan data dalam penyusunan tugas akhir (bukan untuk menyusun skripsi seperti program S1). Namun demikian, mahasiswa yang telah mengikuti kegiatan OJT diwajibkan membuat laporan OJT secara konstruktif dan dapat menghasilkan sebuah Laporan Kegiatan Pekerjaan selama melaksanakan On The Job Training dengan aturan penyusunan laporan OJT yang telah ditentukan oleh <span class="bold">Royal Ocean International</span>.
    </p>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    Bentuk OJT dapat dilakukan dengan cara bekerja sesuai dengan jam kerja yang ada pada Industri Pehotelan. <span class="bold">Royal Ocean International</span> sebagai <span class="bold">Pusat Pendidikan 1 tahun Perhotelan dan Kapal Pesiar</span> memiliki 4 Jurusan yaitu terdiri dari:
    <ol>
        @foreach ($jurusan as $item)
        <li>{{ $item->jurusan }}</li>
        @endforeach
    </ol>
    </p>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    Penempatan Mahasiswa dalam hal pelaksanaan On The Job Traning/Magang Hotel Mitra Kerja dapat disesuaikan dengan jurusan dari Mahasiswa peserta OJT/Magang, akan tetapi <span class="bold">Royal Ocean International</span> menyerahkan sepenuhnya mekanisme pelaksanaan OJT/Magang berdasarkan kebijakan maupun peraturan yang berlaku di Hotel Mitra Kerja.
    </p>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <span class="bold">Royal Ocean International</span> juga tidak menutup kemungkinan apabila pada saat pelaksanaan On The Job Training (magang) hotel-hotel mitra kerja menginginkan untuk <span class="bold"> melakukan perekrutan</span> Sumber Daya Manusia terhadap para mahasiswa Royal Ocean International yang sedang melakukan OJT tersebut.
    </p>

    @php
    $bulanIndo = ["Januari", "Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
@endphp
    <li class="text-uppercase bold"><h5 class="bold">waktu pelaksanaan</h5></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    Waktu pelaksanaan On the Job Training (Magang) dilaksanakan selama 6 (enam) bulan, yang mana pelaksanaannya dimulai pada bulan <span class="bold"> {{ $kelompok[0]["lama"] > 1 ? $bulanIndo[$kelompok[0]["bulan"]-1]." sampai dengan bulan ".$bulanIndo[($kelompok[0]["bulan"]+($kelompok[0]["lama"]-1))-1] : $bulanIndo[$kelompok[0]["bulan"]-1] }} tahun {{ date("Y") }}  </span>, dengan kondisi kurikulum perkuliahan dan praktek sudah disampaikan ke mahasiswa telah mencapai 100%.
    </p>

    <li class="text-uppercase bold"><h5 class="bold">PESERTA DAN JUMLAH PESERTA</h5></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Peserta yang melaksanakan On the Job Training adalah mahasiswa program Pendidkan 1 Tahun dibidang Perhotelan dan Kapal Pesiar dari ke 4 jurusan yang dimiliki <span class="bold">Royal Ocean International</span> yang telah mengikuti mata kuliah dan praktek di Kampus <span class="bold">Royal Ocean International</span>. Adapun jumlah peserta On the Job Training adalah maksimal 5 mahasiswa setiap kelompoknya dan jika Hotel Mitra Kerja yang bersangkutan dapat menerima peserta OJT lebih dari 5 mahasiswa, maka kelompoknya akan disesuaikan dengan jumlah mahasiswa yang disetujui oleh pihak Hotel Mitra Kerja.
    </p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Adapun dalam kelompok ini, anggotanya adalah :
        <table style="width:95%;font-size: 12px;" class="table">
            <thead>
                <tr>
                    <th width="5%"><p> NO</p></th>
                    <th width="13%"><p>NIM</p></th>
                    <th width="30%"><p>NAMA</p></th>
                    <th width="50%"><p>JURUSAN</p></th>
                </tr>
            </thead>
            <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach ($kelompok as $item)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $item["NIM"] }}</td>
                    <td>{{ $item["NAMA"] }}</td>
                    <td>{{ $item["jurusan"] }}</td>
                </tr>

                @endforeach
            </tbody>
        </table>
    </p>

    <li class="text-uppercase bold"><h5 class="bold">format presensi dan penilaian</h5></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Pada waktu pelaksanaan On the Job Training pihak <span class="bold">Royal Ocean International</span> akan menyediakan format presensi dan penilaian yang akan digunakan oleh pihak Hotel untuk mengontrol dan menilai aktivitas yang dikerjakan oleh mahasiswa peserta On the Job Taining/Magang.
    </p>
    <li class="text-uppercase bold"><h5 class="bold">penutup</h5></li>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Demikianlah Proposal On the Job Training ini kami buat dan sekiranya dapat dijadikan sebagai tindak lanjut hubungan kerja sama yang baik antara pihak <span class="bold">Royal Ocean International</span> dengan pihak Hotel Mitra Kerja untuk bersama-sama mencerdaskan kehidupan bangsa dan membangun Sumber Daya Manusia Indonesia yang berpotensi, mampu bersaing dan menjadi tuan rumah di negerinya sendiri, hal ini adalah suatu bentuk kepedulian kita bersama demi masa depan gemilang Putra dan Putri Bangsa. Selanjutnya atas perhatian, kerja sama dan kesempatan yang diberikan kami Mengucapkan Terima Kasih.
        <div style="float:left;width:65%">&nbsp;</div>
        <div style="float:right;margin-top:120px;">
            <u>Shaiful Horry, SE.</u><br>
            Ketua Team OJT/Magang
        </div>
    </p>
</ol>
</div>
</div>
<pagebreak/>
<div class="row" style="padding-top:25px;">
<div style="float:left;width:5%">
    <img style="transform:rotate(90deg);" src="{{ asset("images/guntingmu.png") }}" class="img" alt="" srcset="">
    <div class="vl" style="z-index:-1"></div>
</div>
<div style="width:90%">
    <h4 class="text-center bold">
        LEMBAR JAWABAN <br>
        PENERIMAAN (OJT) ON THE JOB TRAINING
    </h4>
    <br>
    <table class="table" id="dperusahaan">
        <tr>
            <td style="width:30%">Nama Perusahaan</td>
            <td>: {{ $kelompok[0]["nama_perusahaan"] }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>: {{ $kelompok[0]["alamat"] }}</td>
        </tr>
        <tr>
            <td>Telp/Fax</td>
            <td>:</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
        </tr>
        <tr>
            <td>Contact Person</td>
            <td>:</td>
        </tr>
    </table>
    <p>
        Sehubungan dengan Proposal OJT dari <span class="bold">Royal Ocean International</span>, kami telah menyetujui dan menerima permohonan OJT atau Magang untuk Mahasiswa/wi Jurusan 
        <ol>
            @foreach ($jurusan as $item)
            <li class="bold"> {{ $item->jurusan }} <br> ( _______  Peserta )</li>
            @endforeach
        </ol>
    </p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Pelaksanaan OJT atau Magang akan dilaksanakan dalam periode waktu antara Bulan <span class="bold"> {{ $kelompok[0]["lama"] > 1 ? $bulanIndo[$kelompok[0]["bulan"]-1]." sampai dengan bulan ".$bulanIndo[($kelompok[0]["bulan"]+($kelompok[0]["lama"]-1))-1] : $bulanIndo[$kelompok[0]["bulan"]-1] }} tahun {{ date("Y") }}  </span>. Adapun Persetujuan dan Penerimaan Mahasiswa/wi <span class="bold">Royal Ocean International</span>, berlaku dengan ketentuan atau persyaratan sebagai berikut :
        ______________________________________________________________________________________________________________________________________________   
    </p>
    <div style="float:left;width:60%;margin-top:10px;font-size:12px">
        <br><br><br><br><br><br>
       <b style="width:70%"> Lembar ini dapat di Fax ke (0341) 588015 <br>
        Atau melalui Email </b>
    </div>
    <div class="text-center">
         <br>
        .................. , .............................
        <br>
        <br>
        <br>
        <br>
        ( ................................................. )
    </div>
</div>
</div>
<pagebreak/>
<div class="row" style="padding-top:25px;">
<div class="col-lg-12">
    <h4 class="text-center bold">
        Form Quisioner
    </h4>
    <br>
    
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    Menindak lanjuti Proposal kami tentang Pemaparan serta Penjelasan Pelaksanaan Program OJT/Magang dari Training Center of Hotel & Cruiseline yang telah didirikan oleh <span class="bold">Wearnes Education Center</span> Group yaitu <span class="bold">Royal Ocean International</span>, kami sangat mengharapkan adanya respon berupa kritik & saran dari <span class="bold">Hotel Mitra Kerja</span> di bidang <span class="bold">Perhotelan</span>, demi terlaksananya Program Pendidikan dan Pelatihan yang diselenggarakan oleh <span class="bold">Royal Ocean International</span> sehingga apa yang telah menjadi komitmen kami, sebagai <span class="bold">Pencetak Sumber Daya Mitra Kerja Profesional Mitra Dunia Kerja</span> dapat terwujud dan sesuai dengan <span class="bold">Standart Kompetensi SDM</span> yang dibutuhkan oleh <span class="bold">Perusahaan Mitra Kerja</span>. <br>
    <span class="bold"><u>Saran & Catatan</u></span> :
    </p>

    <div style="border:2px solid black;height:200px;width:100%;border-radius:7px;padding:10px;">
        <p>Adapun bentuk Kerja Sama yang ingin kami tawarkan adalah sebagai berikut :</p>
        <div>
            <ol>
                <li>Kerja Sama dalam Pengembangan Kurikulum dan Pengajaran.</li>
                <li>Kerja Sama dalam Penempatan Training Mahasiswa/wi (OJT/Magang)</li>
                <li>Kerja Sama dalam Perekrutan Karyawan/SDM</li>
            </ol>
            Catatan : Kerja Sama yang kami tawarkan tidak mengikat dan sangat disesuaikan kebutuhan Hotel Mitra Kerja.
        </div>
    </div>
<br>
    <div style="border:2px solid black;height:200px;width:100%;border-radius:7px;padding:10px;">
    <p>
        Untuk Informasi Lebih Lanjut Bapak/Ibu dapat menghubungi Kampus Royal Ocean International di :
        <table class="table" id="droi">
            <tr>
                <td style="width:30%">Alamat</td>
                <td>: Jl. Jakarta No. 38 Malang</td>
            </tr>
            <tr>
                <td style="width:30%">Telp</td>
                <td>: 0341 - 587844 (hunting) fax. 0341 - 588015</td>
            </tr>
            <tr>
                <td style="width:30%">http</td>
                <td>: www.royaloceancollege.com</td>
            </tr>
            <tr>
                <td style="width:30%">e-mail</td>
                <td>: wecmail@wearneseducation.com</td>
            </tr>
            <tr>
                <td style="width:30%">Contact Person</td>
                <td>: Bp. Shaiful Horry ( 081 796 355 10 )</td>
            </tr>
        </table>
    </p>
    </div>
<br>
    <div style="border:2px solid black;height:200px;width:100%;border-radius:7px;padding:10px;">
    HOTEL MITRA KERJA
    <table class="table" id="droi">
        <tr>
            <td style="width:30%">Nama Hotel</td>
            <td>: _________________________________________________</td>
        </tr>
        <tr>
            <td style="width:30%">Alamat</td>
            <td>: _________________________________________________</td>
        </tr>
        <tr>
            <td style="width:30%">Telp</td>
            <td>: _________________________________________________</td>
        </tr>
        <tr>
            <td style="width:30%">http</td>
            <td>: _________________________________________________</td>
        </tr>
        <tr>
            <td style="width:30%">e-mail</td>
            <td>: _________________________________________________</td>
        </tr>
    </table>
    </div>
    
</div>
</div>


<!-- jQuery 3 -->
<script src="{{asset('lte2/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('lte2/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
</body>
</html>