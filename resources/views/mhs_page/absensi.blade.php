@extends('mhs_page.layout2')
@section('absensi','active')
@section('header')
<h1>
Absensi
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/mahasiswa/index.html')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Absensi</li>
</ol>
@endsection
@section('body')
<div class="row">
  <div class="col-lg-4">

      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{$persen}}<sup style="font-size: 20px">%</sup></h3>
          <p>Prosentase Absensi</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" class="small-box-footer"></a>
      </div>

  </div>
</div>

<div class="row">
  <div class="col-lg-12">

    <div class="box box-danger">
    <div class="box-body" style="padding:10px">
      <h4>
        Hai {{ session("nama_mhs2") }}, Berikut kami tampilkan rekap absensi anda
      </h4>
        <div class="table-responsive">                       
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Mata Kuliah</th>
                  <th>Alpha</th>
                  <th>Ijin</th>
                  <th>Sakit</th>
                </tr>
              </thead>
              <tbody>
                  @php
                      $totA = 0;
                      $totI = 0;
                      $totS = 0;
                  @endphp
               @foreach ($absensi as $itemAbsensi)
               @php
                   $totA = $totA + $itemAbsensi["alpha"];
                   $totI = $totI + $itemAbsensi["ijin"];
                   $totS = $totS + $itemAbsensi["sakit"];
               @endphp
                <tr>
                 <td>{{ $itemAbsensi["tgl"] }}</td>
                 <td>{{ $itemAbsensi["matakuliah"] }}</td>
                 <td>{{ $itemAbsensi["alpha"] }}</td>
                 <td>{{ $itemAbsensi["ijin"] }}</td>
                 <td>{{ $itemAbsensi["sakit"] }}</td>
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                  <tr class="text-primary">
                  <th></th>
                  <th>TOTAL</th>
                  <th>{{$totA}}</th>
                  <th>{{$totI}}</th>
                  <th>{{$totS}}</th>
                  </tr>
              </tfoot>
            </table>
        </div>
    </div>
    </div>

  </div>
</div>
@endsection
@section('konten')
<div class="col-lg-6" style="margin-top:1.5em">
     <div class="statistic d-flex align-items-center bg-white has-shadow">
    <div class="icon bg-green"><i class="fa fa-line-chart"></i></div>
    <div class="text"><strong>{{ $persen }}%</strong><br><small>Prosentase Absensi</small></div>
    </div>
</div>


<section class="tables">   
<div class="container-fluid">
    <div class="row">
    
        <div class="col-lg-12">
                  <div class="card">
                   
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Data Rekap Absensi Anda</h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">                       
                        <table class="table table-striped table-hover">
                          <thead>
                            <tr>
                              <th>Tanggal</th>
                              <th>Mata Kuliah</th>
                              <th>Alpha</th>
                              <th>Ijin</th>
                              <th>Sakit</th>
                            </tr>
                          </thead>
                          <tbody>
                              @php
                                  $totA = 0;
                                  $totI = 0;
                                  $totS = 0;
                              @endphp
                           @foreach ($absensi as $itemAbsensi)
                           @php
                               $totA = $totA + $itemAbsensi["alpha"];
                               $totI = $totI + $itemAbsensi["ijin"];
                               $totS = $totS + $itemAbsensi["sakit"];
                           @endphp
                            <tr>
                             <td>{{ $itemAbsensi["tgl"] }}</td>
                             <td>{{ $itemAbsensi["matakuliah"] }}</td>
                             <td>{{ $itemAbsensi["alpha"] }}</td>
                             <td>{{ $itemAbsensi["ijin"] }}</td>
                             <td>{{ $itemAbsensi["sakit"] }}</td>
                            </tr>
                            @endforeach
                          </tbody>
                          <tfoot>
                              <tr class="text-primary">
                              <th></th>
                              <th>TOTAL</th>
                              <th>{{$totA}}</th>
                              <th>{{$totI}}</th>
                              <th>{{$totS}}</th>
                              </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
    
    </div>
</div>
</section>
@endsection
@section('css')
@endsection
@section('script')
    
@endsection