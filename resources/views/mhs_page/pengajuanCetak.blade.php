<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Pengajuan</title>
     <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('lte2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">

    <style type="text/css" media="print">
        @page 
        {
            size: auto;   /* auto is the initial value */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }

        #box{
            width: 100%;
            /* height: 300px; */
            padding:10px;
            /* border-style: solid; */
            /* border: 1em; */
        }
    </style>
</head>
<body>

<div id="box">

<div class="container-fluid">
<div class="row">
<div class="col-xs-3" style="border-style:solid" id="untukMHS">
<h5>
{{ $ojt[0]["sts_pencarian"] == 2 ? "Bukti Pendaftaran TA" : "Form Pengambilan Surat OJT" }} 
</h5>
<hr>
NO : {{$ojt[0]["no_kelompok"]}}<br>
Tanggal : <br>
@php
    echo Date("d F Y");
@endphp <br><br>
@if ($ojt[0]["sts_pencarian"] <> 2)
Tempat OJT <br>
{{$ojt[0]["sts_pencarian"] == 2 ? "TA" : $ojt[0]["nama_perusahaan"]}}
@endif <br><br>
No Surat : <br>


</div>
<div class="col-xs-9">
        <div class="container-fluid" style="border-style:solid;" id="untukFO">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 text-center">
                    <h5>FORMULIR PENGAJUAN {{ $ojt[0]["sts_pencarian"] == 2 ? "TA" : "OJT" }}</h5>
                    </div>
                </div>
                <div class="row" style="margin-top:10px;">

                    <table width="100%" style="margin-left:15px;">
                        <tr>
                            <td width="30%">No Kelompok</td>
                            <td>: {{$ojt[0]["no_kelompok"]}}</td>
                        </tr>
                        <tr>
                            <td width="30%" style="vertical-align:top">Nama Perusahaan</td>
                            <td>: {{$ojt[0]["sts_pencarian"] == 2 ? "TA" : $ojt[0]["nama_perusahaan"]}}</td>
                        </tr>
                        <tr>
                            <td width="30%" style="vertical-align:top">Alamat Perusahaan</td>
                            <td>: {{$ojt[0]["sts_pencarian"] == 2 ? "-" : $ojt[0]["alamat"]}}</td>
                        </tr>
                        <tr>
                            <td width="30%">Penghubung</td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="row" style="margin-top:15px;">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                        <strong>Data Anggota Kelompok</strong><br>
                        <table style="width:100%;" border="1">
                            <thead>
                            <tr class="text-center">
                                <th style="width:5%;" class="text-center">NO</th>
                                <th style="width:10%;" class="text-center">NIM</th>
                                <th style="width:40%;" class="text-center">NAMA</th>
                                <th style="width:10%;" class="text-center">KELAS</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                            @foreach ($ojt as $ojt)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{$ojt["NIM"]}}</td>
                                <td>{{$ojt["NAMA"]}}</td>
                                <td>{{$ojt["KELAS"]}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                       Malang, {{ Date("d F Y") }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        TIM OJT
                        <br><br><br>
                        ____________ 
                    </div>
                    <div class="col-xs-3 col-sm-3 col-lg-3 col-md-3">
                        Sekretariat / FO
                        <br><br><br>
                        ____________
                    </div>
                </div>
            <br>
                
            </div>
</div>

</div>
</div>

</div>

<br>
<center> --------------------------- potong disini --------------------------- </center>

<!-- jQuery 3 -->
<script src="{{asset('lte2/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('lte2/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script>
$(document).ready(function(){
    var height;
    height = $("#untukFO").height();
    $("#untukMHS").height(height);
});

window.print();
</script>
</body>
</html>