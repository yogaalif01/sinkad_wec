@extends('mhs_page.layout2')
@section('ojt', 'active')
@section('statusKelompok','active')
@section('header')
<h1>
Status Kelompok
</h1>
<ol class="breadcrumb">
    <li><a href="{{url('/mahasiswa/index.html')}}"><i class="fa fa-home"></i> Beranda</a></li>
    <li class="active">Status Kelompok / Pengajuan</li>
</ol>
@endsection
@section('body')
<div class="row">
    <div class="col-lg-4">

        <div class="box box-success direct-chat direct-chat-warning">
        <div class="box-body" style="padding:10px">
        <h4>Status Terakhir Kelompok Anda :</h4>
        <div class="text-primary">
        @php
            switch ($kelompokojt[0]['sts_kelompok']) {
                case 'pengajuan disetujui':
                    echo "<b>".strtoupper($kelompokojt[0]['sts_kelompok'])."</b><br><br>";

                    echo "<a href='".url('mahasiswa/download+proposal+'.$kelompokojt[0]['no_kelompok'])."' class='btn btn-warning btn-sm' id='btncetak' target='_blank'>Download Proposal Pengajuan</a> &nbsp;&nbsp;";

                    // echo "<a href='".url('download/+berkas+PROPOSAL OJT WEC.pdf')."' class='btn btn-warning btn-sm' id='btncetak' target='_blank'>Download Proposal Pengajuan</a> &nbsp;&nbsp;";
                    
                    // echo "<a href='".url('mahasiswa/pemberangkatanCetak/'.$kelompokojt[0]['no_kelompok'])."' target='_blank' class='btn btn-primary btn-sm'>Cetak Formulir Pemberangkatan</a>";
                     
                    echo "<br><a href='#' id='termOJT2' data-toggle='modal' data-target='#modal_termOJT2'><small class='text-danger'>* 
                        Pelajari syarat pemberangkatan OJT
                    </small></a>";
                    break;
                case 'proses pengajuan':
                    echo "<b>".strtoupper($kelompokojt[0]['sts_kelompok'])."</b>";
                    break;
                case 'pengajuan ditolak':
                    echo "<b>".strtoupper($kelompokojt[0]['sts_kelompok'])."</b>";
                    break;
                case 'pengajuan ditolak perusahaan':
                    echo "<b>".strtoupper($kelompokojt[0]['sts_kelompok'])."</b>";
                    break;
                
                default:
                    if($kelompokojt[0]['pulang'] == 0){
                        echo "<b>".strtoupper($kelompokojt[0]['sts_kelompok'])."</b>";
                    }else{
                        if($kelompokojt[0]['sts_pencarian'] == 2){
                            echo "<b>".strtoupper("Proposal Tugas Akhir anda telah disetujui. Silahkan cetak berkas-berkas pendukung dibawah ini : ")."</b>
                            <br> <button class='btn btn-default' data-toggle='modal' data-target='#modal_dokumenOJT'> <i class='fa fa-print'></i> CETAK</button>
                            ";
                        }else{
                           echo "<b>".strtoupper("Anda Telah Menyelesaikan Proses OJT. Yang harus dilakukan setelah ini adalah selesaikan bimbingan dan lewati ujian kompre dengan nilai yang memuaskan. GOOD LUCK ! ")."</b>";
                        }
                    }
                    break;
            }
        @endphp
        </div>
        </div>
        </div>
    </div>
</div>
<div class="row">
<div class="col-lg-12">

    <div class="box box-success direct-chat direct-chat-warning">
    <div class="box-body" style="padding:10px">
    
        <h4>Riwayat Pengajuan Kelompok OJT</h4>
        <div class="table-responsive">                       
            <table class="table table-striped table-hover" id="tbkelompok">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="15%">NO KELOMPOK</th>
                        <th width="20%">TANGGAL PENGAJUAN</th>
                        <th width="30%">NAMA PERUSAHAAN</th>
                        <th width="20%">STATUS</th>
                        <th width="10%"></th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($kelompokojt as $kelompokojt)
                    @if ($kelompokojt['sts_kelompok'] == "pengajuan ditolak" || $kelompokojt['sts_kelompok'] == "pengajuan ditolak perusahaan")
                    <tr class="text-danger">
                    @else
                    <tr class="text-primary">
                    @endif
                        <td>{{$no++}}</td>
                        <td>{{$kelompokojt['no_kelompok']}}</td>
                        <td>{{$kelompokojt['created_at']}}</td>
                        <td>{{$kelompokojt['nama_perusahaan']}}</td>
                        <td>
                            @if ($kelompokojt['sts_kelompok'] == "menunggu persetujuan")
                                {{ $kelompokojt['apr_pemberangkatan3'] == null ? "menunggu persetujuan dari FO" : "menunggu persetujuan dari dosen pembimbing"}}
                            @else
                                {{ $kelompokojt['sts_kelompok'] }}
                            @endif
                        </td>
                        <td><a href="{{url('mahasiswa/detail kelompok.html?nokelompok='.$kelompokojt['no_kelompok'])}}"><button class="btn btn-info btn-sm">Detail</button></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    </div>

</div>
</div>

<div class="modal fade" id="modal_dokumenOJT">
<div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Cetak Dokumen Akhir</h4>
    </div>
    <div class="modal-body">
            <ol>
                <li class="text-uppercase"> <a style="color:black" target="_blank" href="{{url('mahasiswa/cetak berkas bimbingan/'.$kelompokojt["no_kelompok"])}}">kartu kendali bimbingan</a> </li>
                <br>
                <li class="text-uppercase"><a style="color:black" target="_blank" href="{{url('mahasiswa/cetak form ujian/'.$kelompokojt["no_kelompok"])}}">form ujian komprehensif ( *dicetak jika proses bimbingan telah selesai) </a></li>
                <br>
                <li class="text-uppercase"><a style="color:black" target="_blank" href="{{url('mahasiswa/cetak lampiran ujian/'.$kelompokojt["no_kelompok"])}}">lampiran form ujian ( *dicetak jika proses bimbingan telah selesai)</a></li>
            </ol>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
    </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal_termOJT2">
<div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Persyaratan Pengajuan OJT</h4>
    </div>
    <div class="modal-body">
            <ol>
                <li class="text-uppercase">Semua anggota kelompok sudah lunas administrasi di bulan berjalan.</li>
                <br>
                <li class="text-uppercase">Semua anggota kelompok sudah lulus semua mata kuliah.</li>
                <br>
                <li class="text-uppercase">Semua anggota kelompok memiliki prosentasi ketidakhadiran < 20%</li>
            </ol>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Baik, Saya Mengerti</button>
    </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

@endsection

@section('css')
<link rel="stylesheet" href="{{asset('lte2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('script')
<script src="{{asset('lte2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function(){

$('#tbkelompok').DataTable();

});
</script>
@endsection