<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tb_hasil extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "tb_hasil";
    protected $guarded      = [];
    public $timestamps      = false;

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }
}
