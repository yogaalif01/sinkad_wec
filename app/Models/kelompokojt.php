<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletingTrait;

class kelompokojt extends Model
{
    //
    // use SoftDeletingTrait;
    protected $connection   = "db2019";
    protected $table        = "o_kelompokojt";
    protected $guarded      = [];
    // protected $dates = ['deleted_at'];

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }


    public function gettbkelompok(){
        return $this->belongsTo("App\\Models\\tbkelompokojt", "no_kelompok", "kode_kelompok");
    }

    public function getperusahaan(){
        return $this->belongsTo("App\\Models\\perusahaan", "kd_perusahaan", "kode_perusahaan");
    }

    public function getdetail(){
        return $this->hasMany("App\\Models\\detkelompokojt", "no_kelompok","no_kelompok");
    }

    public function getpembimbing1(){
        return $this->belongsTo("App\\Models\\tbpegawai", "nip", "NIP");
    }

    public function getpembimbing2(){
        return $this->belongsTo("App\\Models\\tbpegawai", "nip2", "NIP");
    }
}
