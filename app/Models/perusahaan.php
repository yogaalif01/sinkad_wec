<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class perusahaan extends Model
{
    //

    // use SoftDeletes;

    protected $connection   = "db2019";
    protected $table        = "tbperusahaan";
    protected $guarded      = [];
    public $timestamps      = false;
    protected $primaryKey   = "kode_perusahaan";

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }

    public function getkelompok(){
        return $this->hasMany("App\\Models\\kelompokojt", "kd_perusahaan","kode_perusahaan");
    }
}
