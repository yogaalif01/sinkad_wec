<?php

namespace App\Models\sync;

use Illuminate\Database\Eloquent\Model;

class tb_sinkad extends Model
{
    //
    protected $connection   = "db2020";
    protected $guarded      = [];
    protected $table        = "tbsinkads";
    public $timestamps      = false;
}
