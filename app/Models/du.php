<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class du extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "du";
    protected $guarded      = [];
    public $timestamps      = false;

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }

    public function getmhsdaft(){
        return $this->belongsTo("App\\Models\\mhsdaft","NPM","NPM");
    }

    public function gettbhasil(){
        return $this->belongsTo("App\\Models\\tb_hasil","NIM","NIM");
    }

    public function gethistoryangsuran(){
        return $this->hasMany("App\\Models\\tb_angsuran", "NIM", "NIM");
    }

    public function getdetkelompokojt(){
        return $this->hasMany("App\\Models\\detkelompokojt", "nim", "NIM");
    }

    public function getnilai(){
        return $this->hasMany("App\\Models\\tbnilai", "NIM", "NIM");
    }

    public function getnilaiOJTA(){
        return $this->hasMany("App\\Models\\tbnilai", "NIM", "NIM")->where("kd_matkul", "like", "%OJT%");
    }
}
