<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class wisuda extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "w_bayar";
    protected $guarded      = [];
    public $timestamps      = false;

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }

    public function getdu(){
        return $this->belongsTo("App\\Models\\du", "NPM", "NPM");
    }

    public function getmhsdaft(){
        return $this->belongsTo("App\\Models\\mhsdaft", "NPM", "NPM");
    }
}
