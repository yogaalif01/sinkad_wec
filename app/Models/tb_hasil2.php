<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tb_hasil2 extends Model
{
    //
    protected $connection   = "db2020";
    protected $table        = "tb_hasil";
    protected $guarded      = [];
    public $timestamps      = false;
}
