<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class pegawai extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "tbpegawai";
    protected $guarded      = [];
    public $timestamps      = false;

    public function __construct(){
        parent::__construct();
        $this->connection = "db".session("th_ajaran");
    }

    public function getkelompok(){
        return $this->hasMany("App\\Models\\kelompokojt", "nip","NIP");
    }

    public function getkelompok2(){
        return $this->hasMany("App\\Models\\kelompokojt", "nip2","NIP");
    }
}
