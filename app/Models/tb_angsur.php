<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tb_angsur extends Model
{
    //
    protected $connection   = "db2019";
    protected $table        = "tb_angsur";
    protected $guarded      = [];

    public function getjurusan(){
        return $this->hasMany("App\\Models\\tb_jurusan", "KD_JURUSAN", "KD_JURUSAN");
    }

}
