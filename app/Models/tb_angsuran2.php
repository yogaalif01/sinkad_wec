<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tb_angsuran2 extends Model
{
    //
    protected $connection   = "db2020";
    protected $table        = "tb_angsuran";
    protected $guarded      = [];
    public $timestamps      = false;
}
