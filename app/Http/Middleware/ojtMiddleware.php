<?php

namespace App\Http\Middleware;

use Closure;

class ojtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('login_ojt') <> 1){
            return redirect()->to('/admin/login.html');
        }
        return $next($request);
    }
}
