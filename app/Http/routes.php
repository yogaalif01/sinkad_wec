<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Auth;


Route::get('/', "MainController@index");
Route::get('/testing',"MainController@testing");
Route::get('/download/{path}', "MainController@download");

Route::get('/logout', function(){
    session()->flush();

    Auth::logout();
    // dd(Auth::check());
    return redirect()->back();
});

Route::get("/daftar", "MainController@daftar");
Route::post("/ceknim", "MainController@ceknim");
Route::post("/pdaftar", "MainController@pdaftar");

Route::post("/login", "MainController@login");
Route::post('/lupa', "MainController@lupa");
Route::post('/jawab', "MainController@jawab");

Route::get("/miracle", "angsuranController@sync");

Route::group(['middleware' => 'mahasiswa'], function(){
    Route::get('/mahasiswa/index.html', "mhsControler@index");
    Route::get('/mahasiswa/absensi.html', "mhsControler@absensi");
    Route::get('/mahasiswa/angsuran.html', "mhsControler@angsuran");
    Route::get('/mahasiswa/kegiatan.html', "mhsControler@uploadKeg");
    Route::get('/mahasiswa/pengaturan.html', "mhsControler@pengaturan");
    Route::post('/mahasiswa/gantipwd', "mhsControler@gantipwd");
    Route::post('/mahasiswa/gantipwd2', "mhsControler@gantipwd2");
    Route::get('/mahasiswa/nilaikelas.html', 'mhsControler@nilaikelas');
    Route::get('/mahasiswa/devcom.html', function(){
        return view('mhs_page.devcom');
    });
    Route::get('/mahasiswa/cariKelasByJurusan/{id}', "mhsControler@cariKelasByJurusan");
    Route::post('/mahasiswa/nilaiperkelas', "mhsControler@nilaiperkelas");

    //route ojt
    Route::get('/mahasiswa/pengajuan.html', "mhsControler@pengajuanPage");
    Route::get('/mahasiswa/pengajuan/buatkelompok', "mhsControler@pengajuan_buatkelompok");
    Route::get('/mahasiswa/pengajuan+gagal.html',"mhsControler@pengajuanGagal");
    Route::get('/mahasiswa/status kelompok.html', "mhsControler@statusKelompokPage");
    Route::get('/mahasiswa/anggota/{nim}/{nokelompok}', 'mhsControler@tambahAnggota');
    Route::post('/mahasiswa/tambahAnggotaPost', 'mhsControler@tambahAnggotaPost');
    Route::get('/mahasiswa/hapusAnggota/{nim}/{no_kelompok}', 'mhsControler@hapusAnggota');
    Route::post('/mahasiswa/ubahPengajuan', 'mhsControler@ubahPengajuan');
    Route::get('/mahasiswa/pengajuanBatal/{no_kelompok}', 'mhsControler@hapusNomor');
    Route::post('/mahasiswa/tambahPerusahaan', 'mhsControler@tambahPerusahaan');
    Route::get('/mahasiswa/cekPerusahaan/{kdperusahaan}/{no_kelompok}', 'mhsControler@cekPerusahaan');
    Route::get('/mahasiswa/pengajuanCetak/{no_kelompok}', 'mhsControler@pengajuanCetak');
    Route::get('/mahasiswa/detail kelompok.html', "mhsControler@detailkelompok");
    Route::get('/mahasiswa/pemberangkatanCetak/{no_kelompok}', 'mhsControler@pemberangkatanCetak');
    Route::get('/mahasiswa/cetak berkas bimbingan/{no_kelompok}', 'mhsControler@cetakbimbingan');
    Route::get('/mahasiswa/cetak form ujian/{no_kelompok}', 'mhsControler@cetakform');
    Route::get('/mahasiswa/cetak lampiran ujian/{no_kelompok}', 'mhsControler@cetaklampiran');
    Route::get('/mahasiswa/download+proposal+{no_kelompok}', "mhsControler@downloadProposal");
    Route::get('/mahasiswa/roi/download+proposal+{no_kelompok}', "mhsControler@downloadProposalRoi");
    //end of route ojt

    //route ta
    Route::get('/mahasiswa/pengajuan ta/buatkelompok', "mhsControler@pengajuan_buatkelompokTA");
    // end of route ta

    Route::get('/mahasiswa/ecc.html', 'mhsControler@ecc');
    Route::get('/mahasiswa/eccpost', 'mhsControler@eccpost');



});


// admin route
Route::get("/admin", function(){
    return redirect()->to("admin/login.html");
});
Route::get('admin/login.html', "AdminController@login");
Route::post('admin/loginPost', "AdminController@loginPost");

// route untuk tim ojt
Route::group(["middleware" => "ojt"], function(){
    Route::get('ojt', "ojtController@index");
    Route::get('ojt/pengajuan.html', "ojtController@accpengajuan");
    Route::get('ojt/pemberangkatan.html', "ojtController@accpemberangkatan");
    Route::get('ojt/konfirmasi.html', "ojtController@konfirmasi");

    Route::get('ojt/detailpengajuan/{no_kelompok}',"ojtController@detailpengajuan");
    Route::get('/ojt/cekPerusahaan/{kdperusahaan}/{no_kelompok}', 'ojtController@cekPerusahaan');
    Route::get('/ojt/ubahPerusahaan/{kdperusahaan}/{up}/{no_kelompok}/{lama}/{bulan}', 'ojtController@ubahPerusahaan');

    Route::get('ojt/updatedetail/{no_kelompok}',"ojtController@updatepengajuan");
    Route::get('ojt/tolakdetail/{no_kelompok}',"ojtController@tolakpengajuan");
    Route::get('ojt/tolak/{no_kelompok}',"ojtController@tolakpengajuan2");

    Route::get('ojt/detailpemberangkatan/{no_kelompok}',"ojtController@detailpemberangkatan");
    Route::get('ojt/updatedetailpemberangkatan/{no_kelompok}',"ojtController@updatepemberangkatan");
    Route::get('ojt/tambahanggota/{nim}/{nokelompok}', 'ojtController@ceknim');
    Route::post('ojt/tambahAnggotaPost/', 'ojtController@tambahAnggota');
    Route::get('ojt/pemberangkatanCetak/{no_kelompok}', 'ojtController@pemberangkatanCetak');
    Route::get('ojt/lihatnilai/{nim}', 'ojtController@lihatnilai');
    Route::get('ojt/hapusanggota/{nim}/{nokelompok}','ojtController@hapusanggota');
    Route::get('ojt/download+proposal+{no_kelompok}','ojtController@downloadProposal');
    Route::get('/ojt/cekPembimbing/{nip}/{pengajar}', 'ojtController@cekPembimbing');
    Route::get('/ojt/konfirmasi-terima/{no_kelompok}', 'ojtController@konfirmasiterima');

    Route::get('ojt/kelompok fix.html', 'ojtController@ojtfix');
    Route::get('ojt/kelompok/{no_kelompok}',"ojtController@detailkelompok");
    Route::get('ojt/selesai/{no_kelompok}', 'ojtController@selesai');
    Route::get('ojt/Data TA.html', 'ojtController@tafix');

    Route::get("ojt/kebijakan/data.html","kebijakanController@index");
    Route::get("ojt/kebijakan/tambah.html","kebijakanController@create");
    Route::post("ojt/kebijakan/store","kebijakanController@store");
    Route::get("ojt/kebijakan/edit-{id}.html", "kebijakanController@edit");
    Route::put("ojt/kebijakan/update/{id}", "kebijakanController@update");
    Route::delete("ojt/kebijakan/delete-{id}", "kebijakanController@destroy");

    Route::get("ojt/perusahaan/data.html","perusahaanController@index");
    Route::any("ojt/perusahaan/getjson", "perusahaanController@getjson");
    Route::get("ojt/perusahaan/tambah.html","perusahaanController@create");
    Route::post("ojt/perusahaan/store","perusahaanController@store");
    Route::get("ojt/perusahaan/edit-{id}.html", "perusahaanController@edit");
    Route::put("ojt/perusahaan/update/{id}", "perusahaanController@update");
    Route::delete("ojt/perusahaan/delete-{id}", "perusahaanController@destroy");
    Route::delete("ojt/perusahaan/softdelete-{id}", "perusahaanController@softdestroy");

    Route::get("ojt/matkul+syarat.html","matkulSyaratController@index");
    Route::post("ojt/matkul+syarat/add", "matkulSyaratController@store");
    Route::post("ojt/matkul+syarat/hapus", "matkulSyaratController@destroy");

    Route::get("ojt/all.html", "ojtController@all");
    Route::get("ojt/all/{nokelompok}", "ojtController@alldetail");
    Route::get("ubahketa/{nokelompok}", "ojtController@ubahketa");

    Route::get("ojt/mahasiswa.html", "ojtController@mahasiswa");

    //route menu kelompok
    Route::get("/ojt/new/kelompok/buat.html", "kelompokController@buat");
    Route::get("/ojt/new/kelompok/edit.html", "kelompokController@edit");
    Route::post("/ojt/new/kelompok/buat-2", "kelompokController@buat2");
    Route::get("/ojt/new/kelompok/edit/detail/{nokelompok}", "kelompokController@detail");
    Route::get("/ojt/new/kelompok/pengajuan+gagal2.html", "kelompokController@gagal2");
    Route::post("ojt/new/kelompok/detail/simpan", "kelompokController@simpan");
    Route::get("ojt/pengajuanBatal/{nokelompok}", "kelompokController@hapusKelompok");
    Route::get("ojt/hapuskelompok/{nokelompok}", "kelompokController@hapusKelompok2");
    // end of route menu kelompok

    // route report perusahaan
    Route::get("/ojt/perusahaan+menerima+ojt.html", "ojtController@perusahaanPenerima");
    Route::get("/ojt/perusahaan+menolak+ojt.html", "ojtController@perusahaanPenolak");
    // end of route report perusahaan

    // route data bimbingan
    Route::get("ojt/data+bimbingan.html", "bimbinganController@index");
    // end of route data bimbingan

    // route penilaian
    Route::get("ojt/input-nilai.html", "penilaianController@inputNilai");
    Route::get("ojt/input-nilai/cari/{nokelompok}", "penilaianController@cari");
    Route::get("ojt/input-nilai/new", "penilaianController@baru");
    Route::get("ojt/input-nilai/update", "penilaianController@update");
    Route::get("ojt/input-nilai/saveJudul", "penilaianController@saveJudul");
    Route::get("ojt/input-nilai/updateJudul", "penilaianController@updateJudul");
    Route::get("ojt/lihat-nilai.html", "penilaianCOntroller@lihatNilai");
    // end of route penilaian


});
// end of route untuk tim ojt

//route untuk pengajar
Route::group(["middleware"=>"pengajar"], function(){
    Route::get('pengajar', "pengajarController@index");
    Route::get('pengajar/pemberangkatan.html', "pengajarController@accpemberangkatan");
    Route::get('pengajar/detailpemberangkatan/{no_kelompok}', "pengajarController@detailpemberangkatan");
    Route::get('pengajar/updatedetailpemberangkatan/{no_kelompok}',"pengajarController@updatepemberangkatan");
    //cetak berkas
    Route::get('/pengajar/cetak berkas bimbingan/{no_kelompok}', 'pengajarController@cetakbimbingan');
    Route::get('/pengajar/cetak form ujian/{no_kelompok}', 'pengajarController@cetakform');
    Route::get('/pengajar/cetak lampiran ujian/{no_kelompok}', 'pengajarController@cetaklampiran');
    //end of cetak berkas
    Route::get('pengajar/acc ujian kompre.html', "pengajarController@accujian");
    Route::get('pengajar/bolehujian/{no_kelompok}', "pengajarController@accujian");

    Route::get('pengajar/wisuda/bayar.html', "wisudaController@index");
    Route::get("pengajar/wisuda/tambah.html","wisudaController@create");
    Route::post("pengajar/wisuda/store","wisudaController@store");
    Route::get("pengajar/wisuda/edit-{id}.html", "wisudaController@edit");
    Route::put("pengajar/wisuda/update/{id}", "wisudaController@update");
    Route::delete("pengajar/wisuda/delete-{id}", "wisudaController@destroy");
    Route::get("pengajar/wisuda/setting.html", "wisudaController@setting");
    Route::post("pengajar/wisuda/updatesetting", "wisudaController@updatesetting");

    Route::get("pengajar/kebijakan/data.html","kebijakanController@index");
    Route::get("pengajar/kebijakan/tambah.html","kebijakanController@create");
    Route::post("pengajar/kebijakan/store","kebijakanController@store");
    Route::get("pengajar/kebijakan/edit-{id}.html", "kebijakanController@edit");
    Route::put("pengajar/kebijakan/update/{id}", "kebijakanController@update");
    Route::get("pengajar/kebijakan/delete-{id}", "kebijakanController@destroy");

    Route::get("pengajar/matkul+syarat.html","matkulSyaratController@index");
    Route::post("pengajar/matkul+syarat/add", "matkulSyaratController@store");
    Route::post("pengajar/matkul+syarat/hapus", "matkulSyaratController@destroy");

});
//end of route untuk pengajar

//route untuk ecc
Route::group(["middleware"  => "ecc"], function(){
    Route::get('ecc/', 'eccController@index');
    Route::get('ecc/pendaftar.html', "eccController@pendaftar");
    Route::get('ecc/jadwal.html', "eccController@jadwal");
    Route::post('ecc/tambahJadwal', "eccController@tambahJadwal");
    Route::get('ecc/status/{status}/{id}', "eccController@ubahStatus");
    Route::get('ecc/hapusJadwal/{id}', "eccController@hapusJadwal");
    Route::get('ecc/ubah-jadwal/{id}', "eccController@ubahJadwal");
    Route::post('ecc/ubahJadwalpost', "eccController@ubahJadwalpost");
    Route::get('ecc/loadPendaftar/{jadwal}', "eccController@loadPendaftar");
    Route::get('ecc/cetak.html', "eccController@cetak");

});
//end of route untuk ecc

 
//route untuk fo
Route::group(["middleware" => "fo"], function(){
    Route::get('fo/', 'foController@index');
    Route::get('/angsuran', 'foController@angsuran');
    Route::get('/cekangsur/{KD_JURUSAN}/{GEL}','foController@cekrincian');

    Route::get('/angsuran/add.html/{KD_JURUSAN}/{GEL}', 'foController@add_angsuran');
    Route::post('/angsuran/save.html', 'foController@save_angsuran');
    Route::get('/angsuran/edit.html/{KD_JURUSAN}/{GEL}/{ANGS}', 'foController@edit_angsuran');
    Route::post('/angsuran/edit.html', 'foController@edit_angsurann');
    Route::get("angsuran/hapus.html/{KD_JURUSAN}/{GEL}/{ANGS}", "foController@delete_angsuran");
    Route::get('fo/pemberangkatan.html', 'foController@pemberangkatan');
    Route::get('fo/detailpemberangkatan/{no_kelompok}', 'foController@detpemberangkatan');
    Route::get('fo/updatedetailpemberangkatan/{no_kelompok}',"foController@updatepemberangkatan");
    Route::get('fo/amplop/{no_kelompok}', "foController@amplop");
    Route::get('fo/suratproposal/{no_kelompok}', "foController@suratproposal");
    Route::get('fo/suratproposalx/{no_kelompok}', "foController@suratproposalx");
    Route::get('fo/pengantar/{no_kelompok}', "foController@pengantar");
    Route::get('fo/pemberangkatan/{no_kelompok}', "foController@pemberangkatanx");
    Route::get('fo/pemberangkatanx/{no_kelompok}', "foController@pemberangkatanx2");
    Route::get('fo/lihatangsuran/{no_kelompok}', "foController@lihatangsuran");
    Route::get('fo/updateup/{no_kelompok}/{up}', "foController@updateup");

    Route::get("fo/penyerahan+laporan.html", "foController@penyerahan");
    Route::get("fo/penyerahan+laporan+{nokelompok}", "foController@penyerahanUpdate");
    Route::get("fo/penyerahan+laporan2+{nokelompok}", "foController@penyerahanUpdate2");

    Route::get("fo/pembayaran", "foController@pembayaran");
    Route::get("fo/pembayaran/cari/{kategori}/{subjek}", "foController@pembayaranCari");
    Route::get("fo/sinkronisasi/{kategori}/{subjek}", "foController@sinkronisasi");
});

// Route::get('/fo', 'foController@index')->middleware("auth:fo");
//end of route untuk fo


// end of admin route

