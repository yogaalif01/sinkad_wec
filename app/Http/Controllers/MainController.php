<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Exception;
use Fpdf;

class MainController extends Controller
{
    //
    public function index(){
        $pengajar   = DB::table("asuinkad_dos")
                    ->orderBy("jbt");
        $perusahaan = DB::table("tbperusahaan");
        $event      = DB::table("asuinkad_event");

        $source = glob(storage_path("/app/img_galeri")."/*.jpg");
        $galeri = "";
        for ($i=0; $i < count($source); $i++) { 
            $img = explode("app/img_galeri/", $source[$i]);
            $url = url("/storage/app/img_galeri/")."/".$img[1];
            $galeri .= "<a href='#' class='popup' alt='".$url."'>
                    <div class='col-lg-3' >
                        <img src='".$url."' srcset='' class='img img-responsive img_galeri' width='100%' style='padding-top:25px;'> 
                    </div>
                    </a>";
        }
        
        $data = [
            "pengajar"      => $pengajar,
            "perusahaan"    => $perusahaan,
            "galeri"        => $galeri,
            "event"         => $event
        ];
        return view("landing.index", $data);
    }

    public function download($file){
        $path = str_replace("+", "/", $file);
        // dd($path);
        $path = storage_path("/app/$path");
        return response()->download($path);
    }

    public function login(Request $req){
        // result 0 = "nim salah";
        // result 1 = "berhasil";
        // result 2 = "tahun akademik salah";
        // result 3 = "Password salah";
        $result = "";
        $nim = $req->nim;
        $pwd = $req->pwd;
        $pwd = md5("@masgall".$pwd);
        session()->put("th_ajaran", substr($nim, 0, 4));
        try {
            DB::connection("db".session("th_ajaran"));
        } catch (\Exception $th) {
            $result = 2;
        }
        if($result <> 2){
            $ceknim = DB::connection("db".session("th_ajaran"))
                    ->table("du")->where("NIM", $nim)->count();
            if($ceknim == 0){
                $result = 0;
            }else{
                session()->put("nim", $nim);
                $mhs = substr($nim, 5, 1);
                if($mhs <= 6 || $mhs <> 0 ){
                    $mhs = "W.E.C";
                }else{
                    $mhs = "R.O.I";
                }
                session()->put("mhs", $mhs);
                $cekpwd = DB::connection("db".session("th_ajaran"))
                    ->table("asuinkad")->where("pass", $pwd)->where('nim', $nim);
                if($cekpwd->count() == 0){
                    $result = 3;
                }else{
                    $result = 1;
                }
            }
        }   
        session()->put('login', $result);
        return $result;
    }

    public function daftar(){
        return view('landing.daftar');
    }
    
    public function ceknim(Request $req){
        $nim = $req->nim;
        session()->put("nimx", $nim);
        session()->put("th_ajaran", substr($nim, 0, 4));

        $cek = DB::connection("db".session("th_ajaran"))
                ->table("asuinkad")->where("NIM", $nim)->count();

        
        if($cek > 0){
            return "1";
        }else{
            $ceknim = DB::connection("db".session("th_ajaran"))
                        ->table("du")->join("mhsdaft", "du.NPM","=","mhsdaft.NPM")
                        ->select("du.*","mhsdaft.*")
                        ->where("du.nim",$nim)
                        ->get();
            return json_encode($ceknim);
        }

    }

    public function pdaftar(Request $req, $status = "", $msg = ""){
        $nim            = session("nimx");
        $nmpanggilan    = $req->nmpanggilan;
        $pwd            = $req->pwd;
        $tanyalupa      = $req->tanyalupa;
        $jawablupa      = $req->jawablupa;
        $pwd            = md5("@masgall".$pwd);
        // dd("test");
        try {
            $insert         = DB::connection("db".session("th_ajaran"))
                            ->table("asuinkad")
                            ->insert([
                                "nim"       => $nim,
                                "nama"      => $nmpanggilan,
                                "pass"      => $pwd,
                                "tanya"     => $tanyalupa,
                                "jawab"     => $jawablupa,
                            ]);
            $status = 1;
            $msg    = "sukses";
            $mhs = substr($nim, 5, 1);
                if($mhs <= 6 || $mhs <> 0 ){
                    $mhs = "W.E.C";
                }else{
                    $mhs = "R.O.I";
                }
            session()->put("mhs", $mhs);
            session()->put("nim", $nim);
            session()->put('login', $status);
        } catch (exception $e) {
            $status = 0;
            $msg    = "gagal";
        }

        $result = [
            "status"    => $status,
            "msg"       => $msg
        ];

        return json_encode($result);
    
    }

    public function lupa (Request $req){
        // result :
        // 1. sukses
        // 0. nim tidak ada
        // 3. database tidak ditemukan
        $nim = $req->nim;
        $th_ajaran = substr($nim, 0, 4);
        $status = 0;
        $conn = '';

        try {
            $conn = DB::connection("db".$th_ajaran);
        } catch (\Exception $th) {
            $status = 3;
        }

        if($status <> 3){
            $ceknim = $conn->table('asuinkad')->where('nim', $nim);
            if($ceknim){
                $status = 1;
                $data = $ceknim->get();
                $tanya = $data[0]['tanya'];
                $jawab = $data[0]['jawab'];
                $pass = $data[0]['pass'];
            }
        }

        $result = [
            'status'    => $status,
            'tanya'     => $tanya,
            'jawab'     => $jawab,
            'pass'      => $pass,
        ];

        return json_encode($result);
    }

    public function jawab(Request $req, $status = 0, $msg = ""){
        $nim        = $req->nim;
        $jawaban    = $req->jawaban;
        session()->put("th_ajaran", substr($nim, 0, 4));
        $cek        = DB::connection("db".session("th_ajaran"))
                    ->table("asuinkad")
                    ->where("nim", $nim)
                    ->where("jawab", $jawaban)
                    ->count();
        if($cek == 1){
            $status = 1;
            $msg = "sukses";
            $mhs = substr($nim, 5, 1);
                if($mhs <= 6 || $mhs <> 0 ){
                    $mhs = "W.E.C";
                }else{
                    $mhs = "R.O.I";
                }
            session()->put("mhs", $mhs);
            session()->put("nim", $nim);
            session()->put('login', $status);
        }else{
            $status = 0;
            $msg = "gagal";
        }

        $result = [
            "status"    => $status,
            "msg"       => $msg
        ];

        return json_encode($result);
    }

    public function testing(){
        $pdf = new Fpdf();
        $pdf::AddPage();
        $pdf::SetFont('Arial','B',16);
        $pdf::Cell(40,10,'Hello World!');
        $pdf::Output();
        exit;
    }
}
