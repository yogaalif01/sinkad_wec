<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

//model
use App\Models\wisuda;
use App\Models\du;

class wisudaController extends Controller
{
    //
    public function index()
    {
        //
        $result = [
            "wisudabayar" => wisuda::with("getdu","getmhsdaft")->get()
        ];
        return view("pengajar_page.wisuda.index", $result);
    }

    public function create()
    {
        //
        // dd(env("BIAYA_WISUDA"));
        $result = [
            "du"    => du::all()
        ];
        return view("pengajar_page.wisuda.create", $result);
    }

    public function store(Request $req)
    {
        //
        $this->validate($req, [
            "npm"           => "required",
            "tglbayar"      => "required" 
        ], [
            "required"      => "Tidak boleh kosong"
        ]);

        $tb             = new wisuda;
        $tb->NPM        = $req->npm;
        $tb->no_bayar   = "wsd".$req->npm;
        $tb->totalbayar = env("BIAYA_WISUDA");
        $tb->tgl_bayar  = $req->tglbayar;
        $tb->save();
        return redirect()->to("/pengajar/wisuda/bayar.html")->with(["stsWisuda"=>1]);
    }

    public function edit($id)
    {
        //
        $wisuda = wisuda::find($id);
        // dd($wisuda);
        $result = [
            "wisuda"    => $wisuda,
            "du"        => du::all()
        ];
        return view("pengajar_page.wisuda.edit", $result);
    }

    public function update(Request $req, $id){
        $this->validate($req, [
            "npm"           => "required",
            "tglbayar"      => "required" 
        ], [
            "required"      => "Tidak boleh kosong"
        ]);

        $tb             = wisuda::find($id);
        $tb->NPM        = $req->npm;
        $tb->totalbayar = env("BIAYA_WISUDA");
        $tb->tgl_bayar  = $req->tglbayar;
        $tb->save();
        return redirect()->to("/pengajar/wisuda/bayar.html")->with(["stsWisuda"=>1]);
    }

    public function destroy($id)
    {
        //
        try {
            wisuda::find($id)->delete();
            $msg = "berhasil";
            $sts = 1;
        } catch (Exception $e) {
            //throw $th;
            $msg = $e;
            $sts = 0;
        }
        return json_encode(["sts"=>$sts, "msg"=>$msg]);
    }

    public function setting(){
        return view("pengajar_page.wisuda.setting");
    }

    public function updatesetting(Request $req){
        $this->validate($req, [
            "biaya"     => "required",
        ],[
            "required"  => "Tidak boleh kosong"
        ]);

       $this->putPermanentEnv("BIAYA_WISUDA", $req->biaya);
        return redirect()->back()->with(["stsWisuda"=>1]);

    }

    public function putPermanentEnv($key, $value)
    {
        $path = app()->environmentFilePath();

        $escaped = preg_quote('='.env($key), '/');

        file_put_contents($path, preg_replace(
            "/^{$key}{$escaped}/m",
            "{$key}={$value}",
            file_get_contents($path)
        ));
    }

}
