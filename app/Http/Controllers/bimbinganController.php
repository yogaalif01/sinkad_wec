<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\pegawai;

class bimbinganController extends Controller
{
    //

    public function index(){
      $getDosen = pegawai::with(["getkelompok"=>function($q){
        $q->with(["getdetail"=>function($q){
          $q->with(["getdu"=>function($q){
            $q->with("getmhsdaft")->get();
          }])->get();
        }, "getperusahaan"])->get();
      }])->has("getkelompok")->get();

      $getAsdos = pegawai::with(["getkelompok2"=>function($q){
        $q->with(["getdetail"=>function($q){
          $q->with(["getdu"=>function($q){
            $q->with("getmhsdaft")->get();
          }])->get();
        }, "getperusahaan","getpembimbing1"])->get();
      }])->has("getkelompok2")->get();

      return view("ojt_page.bimbingan.index", compact("getDosen", "getAsdos"));
    }
}
