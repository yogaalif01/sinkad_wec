<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

//model
use App\Models\kelompokojt;
use App\Models\tbkelompokojt;
use App\Models\detkelompokojt;
use App\Models\matkul;
use App\Models\tb_jurusan;
use App\Models\perusahaan;
use App\Models\du;
use App\Models\tb_angsur;

//additional
use DB;
use PDF;

class foController extends Controller
{
    //

    public function index(){
        return view('fo_page.layout2');
    }
    
    public function angsuran(){
     
        // $tmpjursan = tb_jurusan::all();
        $tmpjursan = DB::connection("db".session("th_ajaran"))->table("tb_jurusan")->get();
        // dd($tmpjursan);

        return view('fo_page.angsuran.index',compact("tmpjursan"));
    }
    public function cekrincian($KD_JURUSAN,$GEL)
    {
        $table = "";
        $tb_angsur = DB::connection("db".session("th_ajaran"))->table("tb_angsur")
                     ->where("KD_JURUSAN",$KD_JURUSAN)->where("GEL",$GEL)->get();
        // $tb_angsur = tb_angsur::where("KD_JURUSAN",$KD_JURUSAN)->where("GEL",$GEL)->get();
        $a = DB::connection("db".session("th_ajaran"))->table("tb_angsur")
        ->where("KD_JURUSAN",$KD_JURUSAN)->where("GEL",$GEL)->sum('ANGSURAN');
        $b = DB::connection("db".session("th_ajaran"))->table("tb_angsur")
        ->where("KD_JURUSAN",$KD_JURUSAN)->where("GEL",$GEL)->sum('ANGS_BEASISWA');
        
        
      
        $table .= '
        <div class="box-header">
        <h3 class="box-title" style="margin-left:10px">Data Angsuran</h3>
        <div class="box-body">
        <a href="angsuran/add.html/'.$KD_JURUSAN.'/'.$GEL.'" class="btn btn-primary">Tambah Data</a>
      </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th style="width:13%;">No Angsuran</th>
            <th>Angsuran</th>
            <th>Angsuran Beasiswa</th>
            <th style="text-align:center;">Aksi</th>
          </tr>
          </thead>
          <tbody>
        ';
        foreach ($tb_angsur as $key) {
 
            $table .= '
               <tr>
       
                    <td>'.$key['NO_ANS'].'</td>
                    <td>'.number_format($key['ANGSURAN'],"0",",",".").'</td>
                    <td>'.number_format($key['ANGS_BEASISWA'],"0",",",".").'</td>
                    <td style="text-align:center;">
                    <a href="angsuran/edit.html/'.$key['KD_JURUSAN'].'/'.$key['GEL'].'/'.$key['NO_ANS'].'" class="btn btn-primary">Edit</a>
                    <button onclick="deletex('.$key['KD_JURUSAN'].','.$key['GEL'].','.$key['NO_ANS'].')" class="btn btn-danger">Hapus</button>
                    </td>
             </tr> 
               '; 
        }
        $table .= '
            </tbody>
            <tfoot>
                <tr>
                <td></td>
                <td><h3> Rp '.number_format($a,"0",",",".") .' </h3></td>
                <td><h3> Rp '.number_format($b,"0",",",".") .'</h3></td>
                <td></td>
                </tr>
            </tfoot>
        </table>
        </div> 
        <script>
    
        </script>  
        ';
        return json_encode(["data" => $table]);
    }
    public function edit_angsuran($KD_JURUSAN,$GEL,$ANGS){
        $EDIT = tb_angsur::where("KD_JURUSAN",$KD_JURUSAN)->where("GEL",$GEL)->where("NO_ANS",$ANGS)->first();
        $jur = tb_jurusan::where("KD_JURUSAN",$KD_JURUSAN)->first();
        return view('fo_page.angsuran.edit',compact("EDIT","jur"));
    }
    public function add_angsuran($KD_JURUSAN,$GEL){
        $tambah = tb_jurusan::where("KD_JURUSAN",$KD_JURUSAN)->get();
        $gel = $GEL;
        return view('fo_page.angsuran.add',compact("tambah","gel"));
    }
    public function save_angsuran(Request $req){       
        $this->validate($req, [
            "jurusan"               => "required",
            "gelombang"             => "required",
            "no_angsuran"           => "required",
            "angsuran_normal"       => "required",
            "angsuran_beasiswa"     => "required"
        ],[
            "required"      => "Tidak boleh kosong"
        ]);
        $sv = tb_angsur::create([
            "KD_JURUSAN"            => $req->jurusan,
            "GEL"                   => $req->gelombang,
            "NO_ANS"                => $req->no_angsuran,
            "ANGSURAN"              => $req->angsuran_normal,
            "ANGS_BEASISWA"         => $req->angsuran_beasiswa
        ]);
        return redirect()->to("/angsuran")->with([
            "status"    => 200,
            "pesan"     => "berhasil"
        ]);
    }
    public function edit_angsurann(Request $req, tb_angsur $upd){         
        $this->validate($req, [
            "jurusan"               => "required",
            "gelombang"             => "required",
            "no_angsuran"           => "required",
            "angsuran_normal"       => "required",
            "angsuran_beasiswa"     => "required"
        ],[
            "required"      => "Tidak boleh kosong"
        ]);
        $update = $upd->where("KD_JURUSAN", $req->jurusan)
        ->where("GEL", $req->gelombang)
        ->where("NO_ANS", $req->no_angsuran)
        ->update([
            "KD_JURUSAN"            => $req->jurusan,
            "GEL"                   => $req->gelombang,
            "NO_ANS"                => $req->no_angsuran,
            "ANGSURAN"              => $req->angsuran_normal,
            "ANGS_BEASISWA"         => $req->angsuran_beasiswa
        ]);
        return redirect()->to("/angsuran")->with([
            "status"    => 200,
            "pesan"     => "berhasil"
        ]);
    }
    public function delete_angsuran($KD_JURUSAN,$GEL,$ANGS, tb_angsur $ad){
        $delete = $ad->where("KD_JURUSAN",$KD_JURUSAN)
        ->where("GEL", $GEL)
        ->where("NO_ANS", $ANGS)->delete();
        return redirect()->to("/angsuran")->with([
            "status"    => 200,
            "pesan"     => "berhasil"
        ]);
}
    public function pemberangkatan(){
        // dd("ds");
        $kelompok = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                    ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                    ->get();

        return view('fo_page.pemberangkatan',["kelompok"=> $kelompok]);
    }

    public function detpemberangkatan($nokelompok){
        $kelompok = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                    ->join("tbperusahaan","tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                    ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                    ->join("du","du.NIM","=","o_detkelompokojt.nim")
                    ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                    ->where("o_kelompokojt.no_kelompok",$nokelompok)
                    ->get();
        
        foreach($kelompok as $dt){
            // cek absensi
            $persen = $this->cek_absensi($dt["nim"]);

            $xdetail[]      = [
                "nim"           => $dt["nim"],
                "NAMA"          => $dt["NAMA"],
                "KELAS"         => $dt["KELAS"],
                "GEL"           => $dt["GEL"],
                "persen"        => $persen
            ];
        }

        return view("fo_page.detpemberangkatan",["kelompok"=>$kelompok, "detail"=>$xdetail]);
    }

    public function updatepemberangkatan($nokelompok){
        $update = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                  ->where("no_kelompok", $nokelompok)->update(["apr_pemberangkatan3" => "1"]);
        
        return redirect()->back()->with(["sts_pemberangkatan" => "1" ]);
    }

    public function amplop($nokelompok){
        $result = [
            "kelompokojt"  => kelompokojt::with("getperusahaan")->where("no_kelompok", $nokelompok)->get()
        ];
        
        $pdf = PDF::loadView("fo_page.berkas.amplop", $result, [], [
            "format"            => [280, 127],
            "margin_left"       => "20",
            "margin_right"      => "10",
            "margin_bottom"     => "2",
            'default_font_size' => '12',
        ]);
        return $pdf->stream("fo_page.berkas.amplop");
    }

    public function suratproposal($nokelompok){
        //cek kelengkapan persyaratan pengajuan kelompok ojt
        $kelompok = $this->cekSyaratPengajuanByNokelompok($nokelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0){
                return view("fo_page.cetakberkasgagal", ["kelompok" => $kelompok]);        
                break;
            }
        }

        $toRomawi = ["-","I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"];
        $bulan    = intval(date("m"));
        // dd($toRomawi[$bulan]);  
        $result = [
            "kelompok"  => kelompokojt::with("getperusahaan")->where("no_kelompok", $nokelompok)->get(),
            "bulan"     => $toRomawi[$bulan]
        ];

        
        $pdf = PDF::loadView("fo_page.berkas.suratproposal", $result, [], [
            "format"       => "legal",
            "margin_top"   => "60",     
            "margin_left"  => "25",
            "margin_right" => "30"
        ]);
        return $pdf->stream("fo_page.berkas.suratproposal");
    }

    public function suratproposalx($nokelompok){
        //cek kelengkapan persyaratan pengajuan kelompok ojt
        $kelompok = $this->cekSyaratPengajuanByNokelompok($nokelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0){
                return view("fo_page.cetakberkasgagal", ["kelompok" => $kelompok]);        
                break;
            }
        }

        $toRomawi = ["-","I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"];
        $bulan    = intval(date("m"));
        // dd($toRomawi[$bulan]);  
        $result = [
            "kelompok"  => kelompokojt::with("getperusahaan")->where("no_kelompok", $nokelompok)->get(),
            "bulan"     => $toRomawi[$bulan]
        ];

        
        $pdf = PDF::loadView("fo_page.berkas.suratproposalO", $result, [], [
            "format"       => "legal",
            "margin_top"   => "60",     
            "margin_left"  => "5",
            "margin_right" => "5"
        ]);
        return $pdf->stream("fo_page.berkas.suratproposal");
    }

    public function pengantar($nokelompok){
        //cek kelengkapan persyaratan pengajuan kelompok ojt
        $kelompok = $this->cekSyaratPengajuanByNokelompok($nokelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0){
                return view("fo_page.cetakberkasgagal", ["kelompok" => $kelompok]);        
                break;
            }
        }
        $kelompok    = kelompokojt::where("no_kelompok", $nokelompok)->get();
        $detkelompok = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
                      ->join("du", "du.NIM", "=","o_detkelompokojt.nim")
                      ->join("mhsdaft", "mhsdaft.NPM", "=" , "du.NPM")
                      ->where("o_detkelompokojt.no_kelompok", $nokelompok)->get();
        $kd_jurusan  = substr($detkelompok[0]['nim'], 5, 1);
        $djurusan    = tb_jurusan::where("KD_JURUSAN", $kd_jurusan)->get();
        $komp        = matkul::where("kd_jurusan", $kd_jurusan)
                       ->where("nilai", 1)->where("kategori", "KOMP")->orderBy("urutan")->get();
        if(substr(session("th_ajaran"), -3) == "roi"){
            $nonkomp    = matkul::where("kd_jurusan", $kd_jurusan)
                          ->where("nilai", 1)->whereIn("kategori", ["NON KOMP","BAHASA INGGRIS"])->orderBy("urutan")->get();
            $sp         = matkul::where("kd_jurusan", $kd_jurusan)
                          ->where("nilai", 1)->where("kategori", "SPECIAL PURPOSE")->orderBy("urutan")->get();
        }else{
            $nonkomp    = matkul::where("kd_jurusan", $kd_jurusan)
                          ->where("nilai", 1)->where("kategori", "NON KOMP")->orderBy("urutan")->get();
            $sp         = "";
        }

        //update o_kelompokojt
        $update = kelompokojt::where("no_kelompok", $nokelompok)->update(["apr_pengajuan2" => 1]);
        
        $result = [
            "kelompok"      => $kelompok,
            "detkelompok"   => $detkelompok,
            "nonkomp"       => $nonkomp,
            "komp"          => $komp,
            "sp"            => $sp,
            "jurusan"       => $djurusan[0]->jurusan
        ];
        $pdf = PDF::loadView("fo_page.berkas.pengantar", $result, [], [
            "format"       => "legal",
            "margin_top"   => "25",     
            "margin_left"  => "25",
            "margin_right" => "25"
        ]);
        return $pdf->stream("fo_page.berkas.pengantar");
    }

    public function pemberangkatanx($nokelompok){
        //cek syarat pemberangkatan OJT
        $kelompok = $this->cekSyaratPemberangkatanByNokelompok($nokelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0 || $kelompok[$j]["sts_nilai"] == 0){
                return view("fo_page.cetakberkasgagal2", ["kelompok" => $kelompok]);        
                break;
            }
        }
        $toRomawi  = ["-","I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"];
        $bulan     = intval(date("m"));
        $ojt       = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                    ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                    ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                    ->join("du","du.NIM","=","o_detkelompokojt.nim")
                    ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                    ->where("o_kelompokojt.no_kelompok", $nokelompok)->get();
        $result = [
            "kelompok"  => $ojt,
            "bulan"     => $toRomawi[$bulan]
        ];

        //update o_kelompokojt
        $update = kelompokojt::where("no_kelompok", $nokelompok)->update(["apr_pemberangkatan3" => 1]);

        $pdf = PDF::loadView("fo_page.berkas.pemberangkatan", $result, [], [
            "format"       => "legal",
            "margin_top"   => "60",     
            "margin_left"  => "25",
            "margin_right" => "30"
        ]);
        return $pdf->stream("fo_page.berkas.pemberangkatan");
    }

    public function pemberangkatanx2($nokelompok){
        //cek syarat pemberangkatan OJT
        $kelompok = $this->cekSyaratPemberangkatanByNokelompok($nokelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0 || $kelompok[$j]["sts_nilai"] == 0){
                return view("fo_page.cetakberkasgagal2", ["kelompok" => $kelompok]);        
                break;
            }
        }
        $toRomawi  = ["-","I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"];
        $bulan     = intval(date("m"));
        $ojt       = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                    ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                    ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                    ->join("du","du.NIM","=","o_detkelompokojt.nim")
                    ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                    ->where("o_kelompokojt.no_kelompok", $nokelompok)->get();
        $result = [
            "kelompok"  => $ojt,
            "bulan"     => $toRomawi[$bulan]
        ];

        //update o_kelompokojt
        $update = kelompokojt::where("no_kelompok", $nokelompok)->update(["apr_pemberangkatan3" => 1]);

        $pdf = PDF::loadView("fo_page.berkas.pemberangkatanO", $result, [], [
            "format"       => "legal",
            "margin_top"   => "60",     
            "margin_left"  => "5",
            "margin_right" => "5"
        ]);
        return $pdf->stream("surat-pemberangkatan.pemberangkatan");
    }

    public function lihatangsuran($nim){
        $angsuran = DB::connection("db".session("th_ajaran"))->table("tb_angsuran")->where("NIM", $nim)->get();    
        return json_encode([
            "sts"       => 1,
            "msg"       => "success",
            "dangsuran"    => $angsuran
        ]);
    }

    public function updateup($no_kelompok, $up){
        if($up != "0"){
            $kelompok       = kelompokojt::where("no_kelompok", $no_kelompok)->get();
            $kd_perusahaan  = $kelompok[0]["kd_perusahaan"];
            $update         = perusahaan::where("kode_perusahaan", $kd_perusahaan)->update(["up"=>$up]);
        }
        return redirect()->back()->with(["sts_pemberangkatan"=>1]);
    }

    public function penyerahan(){
        $kelompok = tbkelompokojt::with("getdu","getperusahaan")->where("setor_lap", "<>", "1")->get();
        $kelompok2 = tbkelompokojt::with("getdu","getperusahaan")->where("setor_lap", "1")->get();
        return view("fo_page.penyerahan", compact("kelompok","kelompok2"));
    }

    public function penyerahanUpdate($nokelompok){
        $init = tbkelompokojt::where("kode_kelompok", $nokelompok);
        $update = $init->update([
            "setor_lap" => "1"
        ]);

        $rows = "";
        $anggota = [];
        foreach ($init->get() as $kel) {
            foreach ($kel->getdu as $du) {
                $rows .= "<tr id='y".$du->NIM."'>
                    <td> 
                    <button class='btn btn-danger' onclick=update2('".$kel->kode_kelompok."')>rollback</button> 
                    </td>
                    <td>".$kel->kode_kelompok."</td>
                    <td>".$kel["getperusahaan"]["nama_perusahaan"]."</td>
                    <td>".$du->NIM."</td>
                    <td>".$du->getmhsdaft->NAMA."</td>
                    <td>".$du->KELAS."</td>
                </tr>";
                $anggota[] = $du->NIM;
            }
        }

        return json_encode([
            "sts"           => 200,
            "msg"           => "success",
            "rows"          => $rows,
            "anggota"       => $anggota
        ]);
    }

    public function penyerahanUpdate2($nokelompok){
        $init = tbkelompokojt::where("kode_kelompok", $nokelompok);
        $update = $init->update([
            "setor_lap" => "0"
        ]);

        $rows = "";
        $anggota = [];
        foreach ($init->get() as $kel) {
            foreach ($kel->getdu as $du) {
                $rows .= "<tr id='x".$du->NIM."'>
                    <td> 
                    <button class='btn btn-primary' onclick=update('".$kel->kode_kelompok."')>apply</button> 
                    </td>
                    <td>".$kel->kode_kelompok."</td>
                    <td>".$kel["getperusahaan"]["nama_perusahaan"]."</td>
                    <td>".$du->NIM."</td>
                    <td>".$du->getmhsdaft->NAMA."</td>
                    <td>".$du->KELAS."</td>
                </tr>";
                $anggota[] = $du->NIM;
            }
        }

        return json_encode([
            "sts"           => 200,
            "msg"           => "success",
            "rows"          => $rows,
            "anggota"       => $anggota
        ]);
    }

    public function pembayaran(){
        $du = du::with("getmhsdaft")->where("PASIF", 0)->get();
        $kelas = DB::connection("db". session('th_ajaran'))
        ->select("SELECT `du`.`KELAS` FROM `du` where `du`.`KELAS` <> ''  GROUP BY `du`.`KELAS` ORDER BY `du`.`KELAS`");
        $jurusan = tb_jurusan::all();
        return view("fo_page.pembayaran.index", compact("du", "kelas", "jurusan"));
    }

    public function pembayaranCari($kategori, $subjek){
        $data = "";
        switch ($kategori) {
            case 'nim':
                $src = du::with("getmhsdaft", "gethistoryangsuran")->where("NIM", $subjek)->first();
                if($src){
                    $data   = '<div class="container">
                    <div class="row">
                       <div class="col-sm-3">
                          NIM
                       </div>
                       <div class="col-sm-9">
                          '.$src->NIM.'
                       </div>
                    </div>
                    <div class="row">
                       <div class="col-sm-3">
                          Nama Mahasiswa
                       </div>
                       <div class="col-sm-9">
                        '.$src->getmhsdaft->NAMA.'
                       </div>
                    </div>
                    <div class="row">
                       <div class="col-sm-3">
                          Kelas
                       </div>
                       <div class="col-sm-9">
                       '.$src->KELAS.'
                       </div>
                    </div>
                    <br>
                    <div class="row">
                       <div class="col-sm-3">
                          <a href="'.url("/fo/sinkronisasi/nim/".$src->NIM).'" class="btn btn-primary btn-block">Sinkronkan Data</a>
                       </div>
                       <div class="col-sm-9">
                       </div>
                    </div>
                    <br>
                    <div class="row">
                       <div class="col-sm-12">
                          <table id="datapembayaran" class="table table-bordered table-hover">
                             <thead>
                             <tr>
                               <th>#</th>			
                               <th style="width:15%">No Kwitansi</th>
                               <th style="width:10%">Angsuran Ke</th>
                               <th class="cari">Jumlah</th>
                               <th class="cari">Keterangan</th>
                               <th class="cari">Operator</th>
                             </tr>
                             </thead>
                             <tbody>';
                $no = 1;
                foreach ($src->gethistoryangsuran as $h) {
                    $data .= '<tr>
                                <th>'.$no++.'</th>			
                                <th style="width:15%">'.$h->NO_KW.'</th>
                                <th style="width:10%">'.$h->NO_ANS.'</th>
                                <th class="cari">'.number_format($h->JUMLAH).'</th>
                                <th class="cari">'.$h->KETERANGAN.'</th>
                                <th class="cari">'.$h->OPERATOR.'</th>
                            </tr>';
                }

                $data .=    '</tbody>
                          </table>
                       </div>
                    </div>
                 </div>
                 
                 <script>
                 $(function(){
                     $("#datapembayaran").DataTable()
                 })
                 </script>
                 ';
                    $status = 200;
                    $msg    = "berhasil";
                }
                break;
            
            case 'kelas':
                $src = du::with("gettbhasil")->where("kelas", $subjek)->get();
                $data = '<div class="container">
                <div class="row">
                    <div class="col-lg-12">
                    <table id="datakelas" class="table table-bordered table-hover" style="width:150%">
                       <thead>
                       <tr>			
                         <th style="width:10%">NIM</th>
                         <th style="width:20%">Nama MHS</th>
                         <th style="width:10%">Jurusan</th>
                         <th style="width:5%">GEL</th>
                         <th style="width:15%">Biaya Asli</th>
                         <th style="width:5%">Dibayar LUNAS</th>
                         <th style="width:5%">Beasiswa</th>
                         <th style="width:15%">Biaya Bersih</th>
                         <th style="width:15%">Sudah Bayar</th>
                         <th style="width:15%">Belum Bayar</th>
                       </tr>
                       </thead>
                       <tbody>';
                foreach ($src as $d) {
                    $data .= '<tr>
                            <td style="width:10%">'.$d->NIM.'</td>
                            <td style="width:20%">'.$d->gettbhasil->NAMA.'</td>
                            <td style="width:10%">'.$d->gettbhasil->JURUSAN.'</td>
                            <td style="width:5%">'.$d->gettbhasil->GEL.'</td>
                            <td style="width:15%">'.$d->gettbhasil->HARGA.'</td>
                            <td style="width:5%">'.$d->gettbhasil->LUNAS.'</td>
                            <td style="width:5%">'.$d->gettbhasil->BEASISWA.'</td>
                            <td style="width:15%">'.$d->gettbhasil->BYR_BERSIH.'</td>
                            <td style="width:15%">'.$d->gettbhasil->TOTAL_BYR.'</td>
                            <td style="width:15%">'.$d->gettbhasil->PIUTANG.'</td>
                        </tr>';
                }
                $data .='
                        </tbody>
                        </table>
                    </div>
                </div>
                
                <script>
                 $(function(){
                     $("#datakelas").DataTable()
                 })
                 </script>
                ';

                $status = 200;
                $msg    = "berhasil";
                break;
            
            case 'jurusan' : 
                $nim = substr(session("th_ajaran"), -3) <> "roi" ? session("th_ajaran")."1".$subjek : substr(session("th_ajaran"), 0, 4)."1".$subjek;
                $src = du::with("gettbhasil")->where("NIM", "like", $nim."%")->get();
                $data = '<div class="container">
                <div class="row">
                    <div class="col-lg-12">
                    <table id="datakelas" class="table table-bordered table-hover table-striped" style="width:150%">
                       <thead>
                       <tr>			
                         <th style="width:10%">NIM</th>
                         <th style="width:27%">Nama MHS</th>
                         <th style="width:5%">Kelas</th>
                         <th style="width:5%">GEL</th>
                         <th style="width:10%">Biaya Asli</th>
                         <th style="width:5%">Dibayar LUNAS</th>
                         <th style="width:5%">Beasiswa</th>
                         <th style="width:10%">Biaya Bersih</th>
                         <th style="width:10%">Sudah Bayar</th>
                         <th style="width:15%">Belum Bayar</th>
                         <th style="width:10%">NIM</th>
                       </tr>
                       </thead>
                       <tbody>';
                foreach ($src as $d) {
                    $data .= '<tr>
                            <td style="width:10%">'.$d->NIM.'</td>
                            <td style="width:27%">'.$d->gettbhasil->NAMA.'</td>
                            <td style="width:5%">'.$d->KELAS.'</td>
                            <td style="width:5%">'.$d->gettbhasil->GEL.'</td>
                            <td style="width:10%;text-align:right">'.number_format($d->gettbhasil->HARGA).'</td>
                            <td style="width:5%">'.$d->gettbhasil->LUNAS.'</td>
                            <td style="width:5%">'.$d->gettbhasil->BEASISWA.'</td>
                            <td style="width:10%;text-align:right">'.number_format($d->gettbhasil->BYR_BERSIH).'</td>
                            <td style="width:10%;text-align:right">'.number_format($d->gettbhasil->TOTAL_BYR).'</td>
                            <td style="width:15%;text-align:right">'.number_format($d->gettbhasil->PIUTANG).'</td>
                            <td style="width:10%">'.$d->NIM.'</td>
                        </tr>';
                }
                $data .='
                        </tbody>
                        </table>
                    </div>
                </div>
                
                <script>
                 $(function(){
                     $("#datakelas").DataTable()
                 })
                 </script>
                ';
                $status = 200;
                $msg    = "berhasil";
                break;
            
            default:
                # code...
                break;
        }

        return json_encode([
            "status"    => $status,
            "message"   => $msg,
            "data"      => $data,
        ]);
    }

    public function sinkronisasi($kategori, $subjek){
        switch ($kategori) {
            case 'nim':
                # code...
                $sync = $this->sinkron($subjek);
                dd($sync);
                $sts = 200;
                $msg = "berhasil";
                break;
            
            default:
                # code...
                break;
        }

        return redirect()->back()->with([
            "status"    => $sts,
            "message"   => $msg
        ]);
    }
}
