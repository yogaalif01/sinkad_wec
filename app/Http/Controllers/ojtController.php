<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

//model
use App\Models\perusahaan;
use App\Models\kelompokojt;
use App\Models\detkelompokojt;
use App\Models\tbkelompokojt;
use App\Models\tb_jurusan;
use App\Models\tbpegawai;
use App\Models\du;

//additional
use DB;
use PDF;
use Exception;

class ojtController extends Controller
{
    //

    public function index(){
        $menungguAccOJT = kelompokojt::with("getperusahaan")->has("getperusahaan")->where("sts_kelompok", "proses pengajuan")->where("kd_perusahaan","<>",null)->count();
        $diterimaPerusahaan = kelompokojt::with("getperusahaan")->has("getperusahaan")->where("konfirmasi_terima", 1)->count();
        $ditolakPerusahaan = kelompokojt::with("getperusahaan")->has("getperusahaan")->where("sts_kelompok", "pengajuan ditolak perusahaan")->count();
        $setPembimbing = kelompokojt::where("nip", "<>", null)->count();
        $berangkatOJT = kelompokojt::where("apr_pemberangkatan3", 1)->where("sts_pencarian","<>","2")->where("pulang", 0)->count();
        $pulang = kelompokojt::where("pulang", 1)->count();
        $semua = kelompokojt::with("getperusahaan")->has("getperusahaan")->get();
        
        return view("ojt_page.index", compact("totalKelompok", "menungguAccOJT","diterimaPerusahaan","ditolakPerusahaan","setPembimbing","berangkatOJT","pulang","semua"));
    }


    public function accpengajuan(){

        $pengajuan = kelompokojt::with("getperusahaan", "getdetail")->has("getperusahaan")
        ->where("sts_kelompok", "proses pengajuan")->get();

        $diterimaTIM = kelompokojt::with("getperusahaan", "getdetail")->has("getperusahaan")
                     ->where("apr_pengajuan", "1")->get();

        $ditolakTIM = kelompokojt::with("getperusahaan")->has("getperusahaan")
                    ->where("sts_kelompok", "pengajuan ditolak")->get();

        $ditolakPerusahaan = kelompokojt::with("getperusahaan")->has("getperusahaan")
                            ->where("sts_kelompok", "pengajuan ditolak perusahaan")->get();

        return view("ojt_page.pengajuan", [
            "tampilpengajuan"   => $pengajuan,
            "diterima"          => $diterimaTIM,
            "ditolak"           => $ditolakTIM,
            "ditolak2"           => $ditolakPerusahaan
        ]);

    }

    public function detailpengajuan(Request $request, $no_kelompok){   
        $xdetail = [];

        $datakelompok = kelompokojt::with(["getperusahaan","getdetail"=>function($q){
            $q->with(["getdu"=>function($qr){
                $qr->with("getmhsdaft")->get();
            }])->get();
        }])->has("getperusahaan")->where("no_kelompok", $no_kelompok)->first();
        
        //cek jumlah kelompok pada perusahaan yang sama
        $kd_perusahaan  = $datakelompok->kd_perusahaan;
        $cekJumlah      = kelompokojt::where("kd_perusahaan", $kd_perusahaan)->count();

        // dd($datakelompok->getdetail);

        foreach($datakelompok->getdetail as $detail){
            // cek absensi
            $persen = $this->cek_absensi($detail->nim);
            $angsuran = $this->cek_administrasi2($detail->nim);

            
            

            $xdetail[]      = [
                "nim"           => $detail->nim,
                "NAMA"          => $detail->getdu->getmhsdaft->NAMA,
                "KELAS"         => $detail->getdu->KELAS,
                "GEL_DAFTAR"    => $detail->getdu->GEL,
                "persen"        => $persen,
                "totalangsuran" => $angsuran["totalangsuran"],
                "totalbayar"    => $angsuran["totalbayar"],
            ];
        }

        

        $data = [
            'datakelompok'      => $datakelompok,
            'detail'            => $xdetail,
            'cekJumlah'         => $cekJumlah,
            "tbperusahaan"      => perusahaan::all(),
            "bulan"             => $this->bulan
        ];

        

        return view("ojt_page.detailpengajuan", $data);
    }

    public function updatepengajuan(Request $request,$no_kelompok)
    {
        //mengambil data dari master tabel o_kelompokojt
        $dkelompok = kelompokojt::with("getdetail")->where("no_kelompok", $no_kelompok)->first();

        //cek kelengkapan persyaratan pengajuan kelompok ojt
        $kelompok = $this->cekSyaratPengajuanByNokelompok($no_kelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0){
                return view("mhs_page.pengajuanGagal2", ["kelompok" => $kelompok]);        
                break;
            }
        }

        $upd = kelompokojt::where("no_kelompok", $no_kelompok)->update([
            'sts_kelompok' => "pengajuan disetujui",
            'apr_pengajuan' => '1',
            'updated_at'    => date("Y-m-d h:i:s")
        ]);

        //update kdojt di tabel du berdasarkan nim anggota
        foreach ($dkelompok->getdetail as $detail) {
            $upd = du::where("nim", $detail->nim)->update([
                "KDOJT" => $no_kelompok
            ]);
        }

        //simpan ke tbkelompokojt
        $arr = [
            "kode_kelompok"     => $dkelompok->no_kelompok,
            "kode_perusahaan"   => $dkelompok->kd_perusahaan,
            "NIP"               => $dkelompok->nip,
            "bulan"             => $dkelompok->bulan,
            "no_prop1"          => $dkelompok->no_kelompok,
            "kode_perusahaan1"  => $dkelompok->kd_perusahaan,
            "tgl1"              => $dkelompok->created_at,
            "cekstatus"         => $dkelompok->sts_pencarian,
            "sts1"              => 0
        ];
        $sv = DB::connection("db".session("th_ajaran"))->table("tbkelompokojt")->insert($arr);
        return redirect()->to('ojt/pengajuan.html')->with(["sts_pengajuan"  => 1]);
    }

    public function tolakpengajuan($no_kelompok)
    {
        $upd = kelompokojt::where("no_kelompok", $no_kelompok)->update([
            'sts_kelompok' => "pengajuan ditolak",
            'updated_at'    => date("Y-m-d h:i:s")
        ]);
        return redirect()->to('ojt/pengajuan.html')->with(["sts_pengajuan" => 1]);
    }

    public function tolakpengajuan2($no_kelompok)
    {
        $upd = kelompokojt::where("no_kelompok", $no_kelompok)->update([
            'sts_kelompok'  => "pengajuan ditolak perusahaan",
            'apr_pengajuan' => null,
            'updated_at'    => date("Y-m-d h:i:s")
        ]);

        //hapus data di tbkelompokojt
        $del = tbkelompokojt::where("kode_kelompok", $no_kelompok)->delete();

        return redirect()->back()->with(["sts_pengajuan" => 1]);
    }

    public function accpemberangkatan(){
       
        $kelompok = kelompokojt::with("getperusahaan", "getdetail")->has("getperusahaan")
                  ->where("sts_kelompok", "pengajuan disetujui")->get();

        $diterimaPerusahaan = kelompokojt::with("getperusahaan", "getdetail")->has("getperusahaan")
                            ->where("sts_kelompok", "pengajuan disetujui")->where("konfirmasi_terima","1")->get();

        $dapatPembimbing = kelompokojt::with("getperusahaan", "getdetail", "getpembimbing1")->has("getperusahaan")
                         ->where("nip", "<>", null)->get();

        return view("ojt_page.pemberangkatan",[
            'tampilpemberangkatan'  => $kelompok,
            'tampilpemberangkatan2' => $dapatPembimbing,
            'tampilpemberangkatan3' => $diterimaPerusahaan
        ]);
    }

    public function detailpemberangkatan(Request $request,$no_kelompok)
    {
        $dkelompok = [];     
        
        $datakelompok = kelompokojt::with(["getperusahaan", "getdetail"=>function($q){
            $q->with(["getdu"=>function($qr){
                $qr->with("getmhsdaft")->get();
            }])->get();
        }])->has("getperusahaan")->where("no_kelompok", $no_kelompok)->first();

        $tpdos = tbpegawai::where("divisi","DOSEN")->where("st_peg","A")->get();
        $tpasdos = tbpegawai::where("divisi","ASDOS")->where("st_peg","A")->get();
        
         //cekabsensi
        foreach ($datakelompok->getdetail as $detail) {
            $persen         = $this->cek_absensi($detail->nim);

            $dkelompok[] = [
                "no_kelompok"       => $detail->no_kelompok,
                "nama_perusahaan"   => $datakelompok->getperusahaan->nama_perusahaan,
                "created_at"        => $datakelompok->created_at,
                "bulan"             => $datakelompok->bulan,
                "lama"              => $datakelompok->lama,
                "sts_kelompok"      => $datakelompok->sts_kelompok,
                "nip"               => $datakelompok->nip,
                "nip2"              => $datakelompok->nip2,
                "nim"               => $detail->nim,
                "NAMA"              => $detail->getdu->getmhsdaft->NAMA,
                "GEL"               => $detail->getdu->GEL,
                "KELAS"             => $detail->getdu->KELAS,
                "apr_pemberangkatan1"   => $datakelompok->apr_pemberangkatan1,
                "sts_kelompok"      => $datakelompok->sts_kelompok,
                "absensi"           => $persen,
            ];
        }

         $data = [
            'kelompok'          => $dkelompok,
            'tpdos'             => $tpdos,
            'tpasdos'           => $tpasdos,
            'bulan'             => $this->bulan
         ];


        return view("ojt_page.detailberangkat", $data);
    }

    public function updatepemberangkatan(Request $request,$no_kelompok)
    {
        $this->validate($request, [
            "pembimbing1"   => "required"
        ]);

        $master =   DB::connection("db".session("th_ajaran"))->table('o_kelompokojt')->where("no_kelompok",$no_kelompok)->first();

        $master = kelompokojt::where("no_kelompok", $no_kelompok)->first();

        //cek syarat pemberangkatan OJT
        $kelompok = $this->cekSyaratPemberangkatanByNokelompok($no_kelompok);
        // dd($kelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0 || $kelompok[$j]["sts_nilai"] == 0){
                return view("mhs_page.pemberangkatanGagal", ["kelompok" => $kelompok]);        
                break;
            }
        }


        if(empty($request->pembimbing2)){
            $upd = kelompokojt::where("no_kelompok", $no_kelompok)->update([
                'sts_kelompok'          => "semua persyaratan telah selesai di ACC",
                'apr_pemberangkatan1'   => '1',
                'bulan'                 => $request->txtbulan,
                'lama'                  => $request->lamaojt,
                'updated_at'            => date("Y-m-d h:i:s"),
                'nip'                   => $request->pembimbing1,
                'pulang'                => $master["sts_pencarian"] == 2 ? 1 : 0,
                "konfirmasi_terima"     => 1
            ]);
        }else{
            $upd = kelompokojt::where("no_kelompok", $no_kelompok)->update([
                'sts_kelompok'          => "semua persyaratan telah selesai di ACC",
                'apr_pemberangkatan1'   => '1',
                'bulan'                 => $request->txtbulan,
                'lama'                  => $request->lamaojt,
                'updated_at'            => date("Y-m-d h:i:s"),
                'nip'                   => $request->pembimbing1,
                'nip2'                  => $request->pembimbing2,
                'pulang'                => $master["sts_pencarian"] == 2 ? 1 : 0,
                "konfirmasi_terima"     => 1
            ]);
        }

        //update data di tbkelompokojt
        $upd = tbkelompokojt::where("kode_kelompok", $no_kelompok)->update([
            "sts1" => 1, "NIP" => $request->pembimbing1
        ]);

         //hapus konfirmasi terima
        kelompokojt::where("no_kelompok", $no_kelompok)->update([
            "konfirmasi_terima"=> 0
        ]);
        
        return redirect()->to('ojt/pemberangkatan.html')->with(["sts_pemberangkatan"  => 1]);
    }

    public function pemberangkatanCetak($no_kelompok){
        $kelompok       = [];
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "mhsdaft.GEL_DAFTAR")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();
        for($i = 0; $i < count($ojt);$i++){
            //absensi
            $absensi    = DB::connection("db".session("th_ajaran"))->table("tbabsensi")
                          ->select(DB::raw("sum('alpha') as alpha"), DB::raw("sum('ijin') as ijin"))
                          ->where("nim", $ojt[$i]["NIM"])->get();
            $jmlpert    = DB::connection("db".session("th_ajaran"))->table("tb_jurusan")
                          ->select("tot_pert")->where("kd_jurusan", $ojt[$i]["KD_JURUSAN"])->get();
            $total_absensi  = $absensi[0]["alpha"] + $absensi[0]["ijin"];
            $persen         = ($total_absensi / $jmlpert[0]["tot_pert"])*100;
            $persen         = intval($persen);

            $kelompok[] = [
                "no_kelompok"       => $ojt[$i]["no_kelompok"],
                "nama_perusahaan"   => $ojt[$i]["nama_perusahaan"],
                "alamat"            => $ojt[$i]["alamat"],
                "up"                => $ojt[$i]["up"],
                "NIM"               => $ojt[$i]["NIM"],
                "NAMA"              => $ojt[$i]["NAMA"],
                "KELAS"             => $ojt[$i]["KELAS"],
                "persen"            => $persen,
            ];
        }

        $result = [
            "ojt"   => $kelompok
        ];
        return view("mhs_page.pemberangkatanCetak", $result);
    }

    public function ceknim($nim, $nokelompok){
        $cek    = "";
        $status = "";
        $gel        = 1;
        $jurusan    = 1;
        $absensix   = 1;
        $adminis    = 1;
        $status     = 1;
        $totbayar   = 0;
        $totangsuran = 0;
        $mhsdiajukan = "";

         $cek = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("o_detkelompokojt", "o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                        ->whereNotIn("o_kelompokojt.sts_kelompok",["pengajuan ditolak","pengajuan ditolak perusahaan"])
                        ->where("o_detkelompokojt.nim", $nim)->count();

        $cek2 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("o_detkelompokojt", "o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                        ->where("o_kelompokojt.sts_kelompok","<>","pengajuan ditolak perusahaan")
                        ->where("o_detkelompokojt.nim", $nim)->count();
        
        // $cek = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")->where("nim", $nim)->where("no_kelompok", $nokelompok)->count();
        if($cek < 1){
            $mhsdiajukan = DB::connection("db".session("th_ajaran"))->table("du")
                        ->join("mhsdaft", "mhsdaft.NPM","=","du.NPM")
                        ->select("du.NIM","mhsdaft.NAMA","du.KELAS", "du.GEL","mhsdaft.GEL_DAFTAR","mhsdaft.KD_JURUSAN")
                        ->where("du.NIM", $nim)->where("du.nim" ,"<>",session("nim"))->get();
            if(count($mhsdiajukan) > 0){
                $cekkelompok    = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
                                  ->where("no_kelompok", $nokelompok)->where("jabatan", "KETUA")->first();

                $mhslogin       = DB::connection("db".session("th_ajaran"))->table("du")
                                ->join("mhsdaft", "mhsdaft.NPM","=","du.NPM")
                                ->select("du.NIM","mhsdaft.NAMA","du.KELAS", "du.GEL","mhsdaft.GEL_DAFTAR","mhsdaft.KD_JURUSAN")
                                ->where("du.NIM", $cekkelompok["nim"])->get();

                // cek absensi
                $persen         =$this->cek_absensi($nim);
                // end of cek absensi

                // cek administrasi
                $adminis = $this->cek_administrasi($nim);
                //cek kebijakan administrasi
                $kebijakan_admin = $this->kebijakan_admin($nim);
                if($kebijakan_admin == 1){
                    // if($adminis <> 1){
                        $adminis    = 1;
                    // }
                }
                
                if($adminis == 0){
                    $status = 0;
                }
                // end of cek administrasi berjalan

            
                if(count($mhsdiajukan) > 0){
                    // cek gelombang
                    if(substr($mhsdiajukan[0]["GEL"],-1) <> substr($mhslogin[0]["GEL"], -1)){
                        $gel    = 0;
                        $status = 0;
                    }
                    // cek jurusan
                    if($mhsdiajukan[0]["KD_JURUSAN"] <> $mhslogin[0]["KD_JURUSAN"]){
                        $jurusan    = 0;
                        $status     = 0;
                    }
                    // cek absensi
                    if($persen >= 30){
                        $absensix   = 0;
                        $status     = 0;
                    }


                }else{
                    $gel            = 0;
                    $jurusan        = 0;
                    $status         = 0;
                    $mhsdiajukan    = 0;
                } 
            }else{
                $gel            = 0;
                $jurusan        = 0;
                $absensix       = 0;
                $adminis        = 0;
                $status         = 0;
                $mhsdiajukan    = 0;
            }
        }else{
            $status = 2;
        }

        $result = [
            "cek"           => $cek,
            "status"        => $status,
            "gel"           => $gel,
            "jurusan"       => $jurusan,
            "absensi"       => $absensix,
            "adminis"       => $adminis,
            "status"        => $status,
            "datamhs"       => $mhsdiajukan,
            "totbayar"      => $totbayar,
            "totangsuran"   => $totangsuran,
            "mhsdiajukan"   => $mhsdiajukan[0]["GEL"],
            "mhslogin"      => substr($mhslogin[0]["GEL"], -1)
        ];
        return json_encode($result);
    }

    public function tambahAnggota(Request $req){
        $insert = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
                  ->insert([
                      "no_kelompok" => $req->no_kelompok,
                      "nim"         => $req->txtnimadd,
                      "jabatan"     => "ANGGOTA"
                  ]);
        
        return redirect()->back()->with(["sts_pemberangkatan" => 1]);
    }

    public function hapusAnggota($nim, $nokelompok){
        $cari = detkelompokojt::where("nim", $nim)->where("no_kelompok", $nokelompok)->get();
        $jabatan = $cari[0]["jabatan"];
        $hapus = detkelompokojt::where("nim", $nim)->where("no_kelompok", $nokelompok)->delete();
        if($jabatan == "KETUA"){
            $hapusketua = detkelompokojt::where("no_kelompok", $nokelompok)->update(["jabatan" => "ANGGOTA"]);
            $carix = detkelompokojt::where("no_kelompok", $nokelompok)->get();
            $nim  = $carix[0]["nim"];
            // dd($nim);
            $updateketua = detkelompokojt::where("no_kelompok", $nokelompok)->where("nim", $nim)->update([
                "jabatan"   => "KETUA"
            ]);
        }
        return redirect()->back();
    }

    public function lihatnilai($nim){
        $jurusan = substr($nim, 5, 1);
        $matkul = DB::connection('db'.session('th_ajaran'))->table('tbmatkul')->where('kd_jurusan', $jurusan)->where('dicetak', 1)->where('matakuliah','<>','UJIAN KOMPREHENSIF')->where("matakuliah",'<>','NILAI OJT')->where("matakuliah",'<>','IAMW');
        $dnilai = [];
        foreach ($matkul->get() as $drow) {
            $nilai = DB::connection("db".session('th_ajaran'))->table('tbnilai')->where('kd_matkul', $drow['kd_matkul'])->where('nim', $nim)->get();
            if($nilai){
                $dnilai[] = [
                    "nm_matkul"     => $drow['matakuliah'],
                    "nilai_akhir"   => $nilai[0]['nilaiakhir'] 
                ];
            }else{
                $dnilai[] = [
                    "nm_matkul"     => $drow['matakuliah'],
                    "nilai_akhir"   => 0
                ];
            }
        }        
        return json_encode([
            "sts"       => 1,
            "msg"       => "success",
            "dnilai"    => $dnilai
        ]);
    }

    public function konfirmasi(){
        return view("ojt_page.konfirmasi");
    }

    public function ojtfix(){

        $sedangOJT = kelompokojt::with("getperusahaan","getdetail","getpembimbing1")->where("pulang", 0)
                    ->where("sts_pencarian","<>","2")->where("sts_kelompok","berangkat OJT")->get();
        
        $selesaiOJT = kelompokojt::with("getperusahaan","getdetail","getpembimbing1")->has("getperusahaan")->where("pulang", 1)
                    ->where("sts_pencarian","<>","2")->get();

        return view("ojt_page.ojtfix", [
            "kelompok"=>$sedangOJT,
            "kelompok2"=>$selesaiOJT
            ]);
    }

    public function detailkelompok(Request $request,$no_kelompok)
    {
        // dd("tst");
        $dkelompok = [];
        $kelompok = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                    ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                    ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                    ->join("du","du.NIM","=","o_detkelompokojt.nim")
                    ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                    ->select("o_kelompokojt.no_kelompok", "o_kelompokojt.sts_pencarian","tbperusahaan.nama_perusahaan","o_kelompokojt.created_at","o_kelompokojt.bulan","o_kelompokojt.lama","o_kelompokojt.sts_kelompok","o_kelompokojt.pulang","o_kelompokojt.nip","o_kelompokojt.nip2","du.NIM", "du.GEL","mhsdaft.NAMA","mhsdaft.GEL_DAFTAR","du.KELAS","mhsdaft.KD_JURUSAN","o_kelompokojt.apr_pemberangkatan1")
                    ->where("o_kelompokojt.no_kelompok",$no_kelompok)
                    ->get();        

         $tpdos = DB::connection("db".session("th_ajaran"))->table('tbpegawai')->where("divisi","DOSEN")->where("st_peg","A")->get();

         $tpasdos = DB::connection("db".session("th_ajaran"))->table('tbpegawai')->where("divisi","ASDOS")->where("st_peg","A")->get();
        
         //cekabsensi
        for($i = 0; $i < count($kelompok); $i++){
            $persen         = $this->cek_absensi($kelompok[$i]["NIM"]);

            $dkelompok[] = [
                "no_kelompok"       => $kelompok[$i]["no_kelompok"],
                "nama_perusahaan"   => $kelompok[$i]["nama_perusahaan"],
                "created_at"        => $kelompok[$i]["created_at"],
                "bulan"             => $kelompok[$i]["bulan"],
                "lama"              => $kelompok[$i]["lama"],
                "sts_kelompok"      => $kelompok[$i]["sts_kelompok"],
                "sts_pencarian"     => $kelompok[$i]["sts_pencarian"],
                "nip"               => $kelompok[$i]["nip"],
                "nip2"              => $kelompok[$i]["nip2"],
                "nim"               => $kelompok[$i]["NIM"],
                "NAMA"              => $kelompok[$i]["NAMA"],
                "GEL_DAFTAR"        => $kelompok[$i]["GEL"],
                "KELAS"             => $kelompok[$i]["KELAS"],
                "apr_pemberangkatan1"   => $kelompok[$i]["apr_pemberangkatan1"],
                "sts_kelompok"      => $kelompok[$i]["sts_kelompok"],
                "absensi"           => $persen,
                "pulang"            => $kelompok[$i]["pulang"]
            ];
        }


         $data = [
            'kelompok'          => $dkelompok,
            'tpdos'             => $tpdos,
            'tpasdos'           => $tpasdos 
          
         ];
        return view("ojt_page.ojtfixdetail", $data);
    }

    public function selesai($nokelompok){
        //cek semua syarat OJT
        $kelompok = $this->cekSyaratPemberangkatanByNokelompok($nokelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0 || $kelompok[$j]["sts_nilai"] == 0){
                return view("ojt_page.cekGagal", ["kelompok" => $kelompok, "menu"=>$this->menu]);        
                break;
            }
        }
        DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")->where("no_kelompok", $nokelompok)
        ->update(["pulang" => 1]);

        return redirect()->to("ojt/kelompok fix.html")->with(["sts_kelompok"=>1]);
    }

    public function tafix(){

        $tampilpemberangkatan3 = kelompokojt::with("getdetail", "getpembimbing1")
                                ->where("sts_pencarian", "2")->get();

        return view("ojt_page.tafix", [
            "kelompok2"=>$tampilpemberangkatan3
            ]);
    }

    public function downloadProposal($no_kelompok){
        session()->put("login", 1);
        if(substr(session("th_ajaran"), -3) == "roi"){
            return redirect()->away("http://akademik/sinkad_wec/mahasiswa/roi/download+proposal+".$no_kelompok);
        }else{
            return redirect()->away("http://akademik/sinkad_wec/mahasiswa/download+proposal+".$no_kelompok);
        }
    }

        public function cekPerusahaan($kdperusahaan, $nokelompok){
        $cek = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->where("kd_perusahaan", $kdperusahaan)->where("no_kelompok","<>", $nokelompok);
        
        if($cek->count() == 0){
            $pesan = "Kelompok ini yang pertama kali mengajukan di perusahaan ini.";
        }else{
            $pesan = "Sudah ada ".$cek->count()." kelompok yang mengajukan di perusahaan ini";
        }
        $dataperusahaan = perusahaan::where("kode_perusahaan", $kdperusahaan)->get(); 
        $return = [
            "pesan"             => $pesan,
            "dataperusahaan"    => $dataperusahaan
        ];
        return $return;
    }

    public function ubahPerusahaan($kdperusahaan, $up, $nokelompok, $lama, $bulan){
        // dd($kdperusahaan);
        $update = kelompokojt::where("no_kelompok", $nokelompok)->update([
                    "kd_perusahaan" => $kdperusahaan,
                    "lama"          => $lama,
                    "bulan"         => $bulan
                    ]);
        
        $update2 = DB::connection("db".session("th_ajaran"))->table("tbkelompokojt")
                    ->where("kode_kelompok", $nokelompok)
                    ->update(["kode_perusahaan"=>$kdperusahaan, "kode_perusahaan1"=>$kdperusahaan]);
        if($up == "empty"){
            $up = null;
        }
        // dd($up);

        $updatePerusahaan = perusahaan::where("kode_perusahaan", $kdperusahaan)->update(["up"=>$up]);
        return redirect()->back();
    }

    public function cekPembimbing($nip, $pengajar){
        $cek = kelompokojt::where("nip", $nip)->orWhere("nip2", $nip)->count();
        if($cek == 0){
            $pesan = "<b class='text-danger'>Bimbingan pertama untuk ".$pengajar." ini</b>";
        }else{
            $jml = $cek + 1;
            $pesan = "<b class='text-primary'>Harap diperhatikan. Anda sedang mengatur kelompok bimbingan yang ke-". $jml ." untuk ".$pengajar." ini</b>";
        }
        $return = [
            "pesan" => $pesan,
            "cek"   => $cek
        ];
        return $return;
    }

    public function konfirmasiterima($no_kelompok){
        $update = kelompokojt::where("no_kelompok", $no_kelompok)->update([
            "konfirmasi_terima" => 1
        ]);

        return redirect()->to("ojt/pemberangkatan.html");
    }

    public function all(){
        $semua = kelompokojt::with("getperusahaan")->get();
        return view("ojt_page.all", compact("semua"));
    }

    public function alldetail($nokelompok){
        $bulan = $this->bulan;
        $datakelompok = kelompokojt::with(["getperusahaan", "getdetail"=>function($q){
            $q->with(["getdu"=>function($qr){
                $qr->with("getmhsdaft")->get();
            }])->get();
        }])->where("no_kelompok", $nokelompok)->first();
        return view("ojt_page.alldetail", compact("datakelompok", "bulan"));
    }

    public function ubahketa($nokelompok){
        $upd = kelompokojt::where("no_kelompok", $nokelompok)->update([
            "sts_pencarian" => 2
        ]);

        try {
            //code...
            $upd2 = tbkelompokojt::where("kode_kelompok", $nokelompok)->update([
                "cekstatus" => 2
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        $return = [
            "status"    => 200,
            "pesan"     => "berhasil"
        ];
        return json_encode($return);
    }

    public function mahasiswa(){

        $mahasiswa = du::with(["getdetkelompokojt"=>function($q){
            $q->with("getkelompokojt")->get();
        }, "getmhsdaft"])->where("PASIF", "0")->get();

        $kelompok = kelompokojt::with("getdetail")->where("sts_kelompok", "pengajuan disetujui")->orWhere("sts_kelompok", "berangkat OJT")->get();

        
        return view("ojt_page.mahasiswa", compact("mahasiswa","kelompok"));
    }

    public function suratbatal($no_kelompok){
        $toRomawi  = ["-","I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"];
        $bulan     = intval(date("m"));
        $ojt       = kelompokojt::with(["getperusahaan", "getdetail"=>function($q){
            $q->with(["getdu"=>function($qu){
                $qu->with("getmhsdaft")->get();
            }])->get();
        }])->where("no_kelompok", $no_kelompok)->first();
        $result = [
            "kelompok"  => $ojt,
            "bulan"     => $toRomawi[$bulan]
        ];


        $pdf = PDF::loadView("ojt_page.berkas.pembatalan", $result, [], [
            "format"       => "legal",
            "margin_top"   => "60",     
            "margin_left"  => "5",
            "margin_right" => "5"
        ]);
        return $pdf->stream("surat-pembatalan.pembatalan");
    }

    public function perusahaanPenerima(){
        $dperusahaan = perusahaan::with("getkelompok")->whereHas("getkelompok", function($q){
            $q->where("apr_pemberangkatan1", 1);
        })->where("kode_perusahaan", ">", 1)->get();
        
        // $dperusahaan = perusahaan::where("kode_perusahaan", ">", 1)->get();

        return view("ojt_page.perusahaanpenerima", compact("dperusahaan"));
    }

    public function perusahaanPenolak(){
        $dperusahaan = perusahaan::with("getkelompok")->whereHas("getkelompok", function($q){
            $q->where("sts_kelompok", "pengajuan ditolak perusahaan");
        })->where("kode_perusahaan", ">", 1)->get();
        
        return view("ojt_page.perusahaanpenolak", compact("dperusahaan"));
    }


}
