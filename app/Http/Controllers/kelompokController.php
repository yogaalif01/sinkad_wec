<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

//model
use App\Models\perusahaan;
use App\Models\kelompokojt;
use App\Models\detkelompokojt;
use App\Models\tbkelompokojt;
use App\Models\tb_jurusan;
use App\Models\tbpegawai;
use App\Models\du;

//additional
use DB;
use Exception;

class kelompokController extends Controller
{
    //

    public function buat(){
        return view("ojt_page.kelompok.buat");
    }

    public function buat2(Request $req){
        $nim = $req->nim;
        $abjad = array("A", "B", "C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
        $Iabjad = array("A"=>0,"B"=>1,"C"=>2,"D"=>3,"D"=>4,"E"=>5,"F"=>6,"G"=>7,"H"=>8,"I"=>9,"J"=>10,"K"=>11,"L"=>12,"M"=>13,"N"=>14,"O"=>15,"P"=>16,"Q"=>17,"R"=>18,"S"=>19,"T"=>20,"U"=>21,"V"=>22,"W"=>23,"X"=>24,"Y"=>25,"Z"=>27);
        $th_ajaran = substr($nim, 0, 4);

        //mengambil data dari tabel du dan mhsdaft
        $select0 = du::with("getmhsdaft")->where("NIM", $nim)->first();
        if($select0 == null){
            $gagal = "NIM tidak ditemukan / tidak terdaftar dalam database.";
            return view("ojt_page.kelompok.gagal", compact("gagal"));
        }

        // cek administrasi
        $administrasi = $this->cek_administrasi($nim);
        //cek kebijakan administrasi
        $kebijakan_admin = $this->kebijakan_admin($nim);
        if($kebijakan_admin <> 1){
            if($administrasi <> 1){
                $gagal = "Belum Melunasi Administrasi Berjalan";
                return view("ojt_page.kelompok.gagal", compact("gagal"));
            }
        }

        // cek absensi
        $persen = $this->cek_absensi($nim);
        //cek kebijakan absensi
        $kebijakan_absen = $this->kebijakan_absen($nim);
        if($kebijakan_absen <> 1){
            if($persen >= 30){
                $gagal = "Absensi melebihi 30%";
                return view("ojt_page.kelompok.gagal", compact("gagal"));
            }
        }

        //cek keanggotaan
        $cekkeanggotaan = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("o_detkelompokojt", "o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                        ->whereNotIn("o_kelompokojt.sts_kelompok",["pengajuan ditolak","pengajuan ditolak perusahaan"])->where("o_detkelompokojt.nim", $nim)->get();

        if(count($cekkeanggotaan) > 0){
            $gagal = "Masih Berada Pada Kelompok Aktif";
            return view("ojt_page.kelompok.gagal", compact("gagal"));
        }

        $select1 = detkelompokojt::where("nim", $nim)->where("jabatan", "KETUA")->count();
        $select2 = kelompokojt::where("no_kelompok","like",$th_ajaran.".".substr($select0["GEL"],-1).".%")->count();

        //autonumber utk nomor kelompok
        if($select1 == 0){
            if($select2 == 0){
                $nomor = $select2 + 1;
                $nomor = $th_ajaran.".".$select0['GEL'].".". $nomor .$abjad[$select1];
            }else{
                 //mencari nomor yang sesuai.
                 $select2 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                 ->where("no_kelompok","like",$th_ajaran.".".substr($select0["GEL"],-1).".%")
                 ->orderBy("id","DESC")->first();
                 $nomor = substr($select2["no_kelompok"], 7,strlen($select2["no_kelompok"])-7);
                 $nomor = substr($nomor, 0, strlen($nomor)-1);
                 $nomor = $nomor +1;
                 $dapatNomor = $th_ajaran.".".substr($select0["GEL"],-1).".". $nomor .$abjad[$select1];
                do {
                    $cekKetersediaanNomor = kelompokojt::where("no_kelompok", $dapatNomor)->get();

                    if(count($cekKetersediaanNomor) <> null)
                    {
                        $nomor++;
                        $dapatNomor = $th_ajaran.".".substr($select0["GEL"],-1).".". $nomor .$abjad[$select1];
                        $stop = 1;
                    }else{
                        $stop = 0;
                    }
                } while ($stop == 1);
                $nomor = $dapatNomor;
            }
        }else{
            $select1 = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
            ->where("nim", $nim)->where("jabatan", "KETUA")->orderBy("no_kelompok", "DESC")->first();
            $Snomor = substr($select1["no_kelompok"],-1);
            $Snomor = $abjad[$Iabjad[$Snomor]+1];
            $Inomor = substr($select1["no_kelompok"], 7, strlen($select1["no_kelompok"])-8);
            $nomor = $th_ajaran.".".substr($select0["GEL"],-1).".".$Inomor.$Snomor;
        }
        // end of autonumber untuk nomor kelompok

        // insert ke tbl o_kelompokojt
            $insert0 = kelompokojt::insert([
            "no_kelompok"   => $nomor,
            "sts_kelompok"  => "proses pengajuan"
        ]);
        // end of insert ke tbl o_kelompokojt

        // insert ke detail kelompok
        $insert1 = detkelompokojt::insert([
            "no_kelompok"   => $nomor,
            "nim"           => $nim,
            "jabatan"       => "KETUA"
        ]);
        // end of insert ke detail kelompok

        return redirect("ojt/new/kelompok/edit/detail/{$nomor}");

    }

    public function detail($nokelompok){

        $kelompok = kelompokojt::with(["getdetail"=>function($q){
            $q->with(["getdu"=>function($q){
                $q->with("getmhsdaft")->get();
            }])->get();
        }, "getperusahaan"])->where("no_kelompok", $nokelompok)->first();
        $tbperusahaan = perusahaan::all();
        $bulan = $this->bulan;
        return view("ojt_page.kelompok.detail", compact("kelompok","tbperusahaan","bulan"));
    }

    public function hapusKelompok($nokelompok){
        $hapus1 = kelompokojt::where("no_kelompok", $nokelompok)->delete();
        $hapus2 = detkelompokojt::where("no_kelompok", $nokelompok)->delete();
        return redirect("ojt/new/kelompok/buat.html");
    }

    public function hapusKelompok2($nokelompok){
       try {
           //code...
           $hapus1 = kelompokojt::where("no_kelompok", $nokelompok)->delete();
           $hapus2 = detkelompokojt::where("no_kelompok", $nokelompok)->delete();
           $sts = 200;
           $msg = "success";
       } catch (Exception $th) {
           //throw $th;
           $sts = 500;
           $msg = $th;
       }
        return json_encode([
            "status"    => $sts,
            "message"   => $msg
        ]);
    }

    public function edit(){
        $pengajuan = kelompokojt::with("getperusahaan", "getdetail")->where("sts_kelompok", "proses pengajuan")->get();
        return view("ojt_page.kelompok.edit", compact("pengajuan"));
    }

    public function simpan(Request $req){
        if($req->jenis_pengajuan != 2){
            $validate["txtkdperusahaan"]    = "required";
            $validate["jenis_pengajuan"]    = "required";
            $validate["lamaojt"]            = "required";
        }
        $validate["txtbulan"]               = "required";
        // dd($req->jenis_pengajuan);

        $this->validate($req,$validate,["required"  => "Tidak boleh kosong"]);

        $insert = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
        ->where("no_kelompok", $req->txtnokelompok)
        ->update([
            "kd_perusahaan" => $req->jenis_pengajuan == 2 ? 1 : $req->txtkdperusahaan,
            "bulan"         => $req->txtbulan,
            "lama"          => $req->lamaojt,
            "sts_pencarian" => $req->jenis_pengajuan
            ]);
        return redirect()->back()->with(["sts_pemberangkatan" => 1]);
    }

}
