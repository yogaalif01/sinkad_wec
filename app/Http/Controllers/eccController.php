<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class eccController extends Controller
{
    //

    public function index(){
      $ecc = DB::connection("db2019")->table("ecc_anggota")
              ->join("du", "du.NIM", "=", "ecc_anggota.nim")
              ->join("mhsdaft", "mhsdaft.NPM", "=", "du.NPM")
              ->join("ecc_jadwal","ecc_jadwal.id","=","ecc_anggota.id_jadwal")
              ->select("du.NIM", "du.KELAS","mhsdaft.NAMA","ecc_jadwal.tanggal")
              ->where("ecc_jadwal.status", 1);
      $result = [
        "ecc" => $ecc->get() 
      ];
      return view("ecc_page.index", $result);
    }

    public function pendaftar(){
        $ecc = DB::connection("db2019")->table("ecc_anggota")
              ->join("du", "du.NIM", "=", "ecc_anggota.nim")
              ->join("mhsdaft", "mhsdaft.NPM", "=", "du.NPM")
              ->join("ecc_jadwal","ecc_jadwal.id","=","ecc_anggota.id_jadwal")
              ->select("du.NIM", "du.KELAS","mhsdaft.NAMA")
              ->where("ecc_jadwal.status","1");


      $result = [
        "eccData" => $ecc->get()
      ];
      return view("ecc_page.pendaftar", $result);
    }

    public function jadwal(){
      $jadwal = DB::connection("db2019")->table("ecc_jadwal")->where("hapus", 0)->orderBy("id", "DESC")->get();
      $result = [
        "eccJadwal" => $jadwal
      ];
      return view("ecc_page.setting", $result);
    }

    public function loadPendaftar($jadwal){
        // $ecc = DB::connection("db".session("th_ajaran"))->select("select ecc_anggota.nim as nim, mhsdaft.NAMA, du.KELAS from ecc_anggota inner join (du inner join mhsdaft on du.NPM = mhsdaft.NPM) on ecc_anggota.nim = du.nim");

        $ecc = DB::connection("db".session("th_ajaran"))->table("ecc_anggota")
                ->join("du", "du.NIM", "=", "ecc_anggota.nim")
                ->join("mhsdaft", "mhsdaft.NPM","=","du.NPM")
                ->select("ecc_anggota.nim", "du.KELAS","mhsdaft.NAMA")->get();
        // dd($ecc[41]["KELAS"]);
        $table = "<div class='table-responsive'>                       
        <table class='table table-striped table-hover' id='tbpendaftar'>
          <thead>
            <tr>
              <th width='10%'>#</th>
              <th width='20%'>NIM</th>
              <th width='50%'>NAMA</th>
              <th width='20%'>KELAS</th>
            </tr>
          </thead>
          <tbody>";
            $no = 1;
          for ($i=0; $i < count($ecc); $i++) { 
              $table .= "<tr>
                <td>". $no ."</td>
                <td>".$ecc[$i][0]."</td>
                <td>".$ecc[$i][1]."</td>
                <td>".$ecc[$i][2]."</td>
              </tr>";
              $no++;
          }

        $table .= "</tbody>
        </table>
        </div>";

        return $table;
    }

    public function cetak(){
      $ecc = DB::connection("db2019")->table("ecc_anggota")
      ->join("du", "du.NIM", "=", "ecc_anggota.nim")
      ->join("mhsdaft", "mhsdaft.NPM", "=", "du.NPM")
      ->join("ecc_jadwal","ecc_jadwal.id","=","ecc_anggota.id_jadwal")
      ->select("du.NIM", "du.KELAS","mhsdaft.NAMA","ecc_jadwal.tanggal")
      ->where("ecc_jadwal.status","1")
      ->orderBy("du.NIM");


      
      $result = [
        "eccData"   => $ecc->get()
      ];
      return view("ecc_page.cetak", $result);
    }

    public function tambahJadwal(Request $req){
      $this->validate($req, [
        "txttanggal"  => "required",
        "txtjam"      => "required",
        "txtlokasi"   => "required"
      ],
      [
        "required"    => "Tidak boleh kosong"
      ]);

      $insert = DB::connection("db2019")->table("ecc_jadwal")->insert([
        "tanggal" => $req->txttanggal,
        "jam"     => $req->txtjam,
        "tempat"  => $req->txtlokasi
      ]);

      return redirect()->back()->with([
        "sts_jadwal"  => "1"
      ]);
    }

    public function ubahStatus($status, $id){
      if($status == "aktif"){
        $update = DB::connection("db2019")->table("ecc_jadwal")->update(["status" => 0]);
        $update = DB::connection("db2019")->table("ecc_jadwal")->where("id", $id)->update(["status" => 1]);
      }else{
        $update = DB::connection("db2019")->table("ecc_jadwal")->where("id", $id)->update(["status" => 0]);
      }
      return redirect()->back()->with([
        "sts_jadwal"  => "1"
      ]);
    }

    public function hapusJadwal($id){
      $hapus = DB::connection("db2019")->table("ecc_jadwal")->where("id", $id)->update(["hapus" => 1]);
      return redirect()->back()->with([
        "sts_jadwal"  => "1"
      ]);
    }

    public function ubahJadwal($id){
      $eccData = DB::connection("db2019")->table("ecc_jadwal")->where("id", $id)->get();
      $result = [
        "eccData" => $eccData
      ];
      return view("ecc_page.ubahJadwal", $result);
    }

    public function ubahJadwalpost(Request $req){
      $this->validate($req, [
        "txttanggal"  => "required",
        "txtjam"      => "required",
        "txtlokasi"   => "required"
      ],
      [
        "required"    => "Tidak boleh kosong"
      ]);
      $update = DB::connection("db2019")->table("ecc_jadwal")->where("id", $req->id_jadwal)->update([
        "tanggal" => $req->txttanggal,
        "jam"     => $req->txtjam,
        "tempat"  => $req->txtlokasi
      ]);

      return redirect()->to("ecc/jadwal.html")->with([
        "sts_jadwal"  => "1"
      ]);
    }
}
