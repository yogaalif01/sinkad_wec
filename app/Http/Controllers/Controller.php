<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

//model
use App\Models\kebijakan;
use App\Models\du;
use App\Models\matkulojt;
use App\Models\tbnilai;
use App\Models\sync\tb_sinkad;
use App\Models\sync\angsuranmaba;
use App\Models\sync\detail_angsuranmaba;

//additional
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;


    public $bulan = [
        "1"     => "Januari", 
        "2"     => "Februari",
        "3"     => "Maret",
        "4"     => "April",
        "5"     => "Mei",
        "6"     => "Juni",
        "7"     => "Juli",
        "8"     => "Agustus",
        "9"     => "September",
        "10"    => "Oktober",
        "11"    => "November",
        "12"    => "Desember"
    ];

    public function test(){
        return $_ENV['DB_DATABASE'];
    }

    public function bulan_berjalan($gel, $bulan){
        $bulanke = ""; 

        // utk gel 1
        $gel1   = [ "11" => "5", "12" => "6", "01" => "7", "02" => "8", "03" => "9", "04" => "10", "05" => "11", "06" => "12", "07"=> "13", "08"=>"14", "09"=>"15","10"=>"16"];
        
        //untuk gel 2
        $gel2   = [ "11" => "4", "12" => "5", "01" => "6", "02" => "7", "03" => "8", "04" => "9", "05" => "10", "06" => "11", "07" => "12", "08"=>"13","09"=>"14","10"=>"15"];

        //untuk gel 2
        $gel3   = [ "11" => "2", "12" => "3", "01" => "4", "02" => "5", "03" => "6", "04" => "7", "05" => "8", "06" => "9", "07" => "10", "08" => "11", "09" => "12","10"=>"13"];
        
        switch ($gel) {
            case 1:
                $bulanke = $gel1[$bulan];
                break;
            case 2:
                $bulanke = $gel2[$bulan];
                break;
            case 3:
                $bulanke = $gel3[$bulan];
                break;
            default:
                # code...
                break;
        }

        if(substr(session("th_ajaran"),-3) == "roi")
        {
            $gel1   = [ "12" => "6", "01" => "7", "02" => "8", "03" => "9", "04" => "10", "05" => "11", "06" => "12", "07" => "13", "08" => "14", "09" => "15", "10" => "16", "11" => "17"];
            $bulanke = $gel1[$bulan];
        }

        return $bulanke;
    }

    public function cek_administrasi($nim){
        $return = 1;
        //mengambil data dari tabel du dan mhsdaft
        $select0 = DB::connection("db".session("th_ajaran"))->table("du")
                   ->join("mhsdaft", "mhsdaft.NPM", "=","du.NPM")
                   ->where("NIM", $nim)->first();

        //cek administrasi
        $bulan          = Date("m");
        $mhs_gel        = substr($select0["GEL"],0,1);
        $conv_bulan     = $this->bulan_berjalan($mhs_gel, $bulan);
        $totangsuran    = DB::connection("db".session("th_ajaran"))->table("tb_angsur")
                          ->where("NO_ANS", "<=",$conv_bulan)
                          ->where("KD_JURUSAN", $select0["KD_JURUSAN"])
                          ->where("GEL", $mhs_gel)
                          ->sum("ANGSURAN");

        //lihat di tbhasil
        $mhsdiajukan = DB::connection("db".session("th_ajaran"))->table("tb_hasil")->where("NIM", $nim)->first();
        // dd($mhsdiajukan["LUNAS"]);

        if(substr(session("th_ajaran"), -3) == "roi"){
            // $totangsuran = $totangsuran;

            if($mhsdiajukan["LUNAS"] == "Ya"){
                $totangsuran = $totangsuran - 500000;
            }
            
        }else{
            if($mhsdiajukan["LUNAS"] == "Ya"){
                $totangsuran = $mhsdiajukan["BYR_BERSIH"];
            }
            if($mhsdiajukan["BEASISWA"] == "Ya"){
                $potBeasiswa = $mhsdiajukan["HARGA"] * (20/100);
                $totangsuran = $totangsuran - $potBeasiswa;
            }
        }

        // $totangsuran - $totangsuran - 1000000;

        $totbayar       = DB::connection("db".session("th_ajaran"))->table("tb_angsuran")
                          ->where("NIM", $select0["NIM"])->sum("JUMLAH");
        
                            // dd($totbayar);
        
        if($totbayar < $totangsuran){
            $return = 0;
        }

        // $return = [
        //     "return"           => $return,
        //     "totalangsuran"    => $totangsuran,
        //     "totalbayar"       => $totbayar
        // ];
        return $return;
    }

    public function cek_administrasi2($nim){
        $return = 1;
        //mengambil data dari tabel du dan mhsdaft
        $select0 = DB::connection("db".session("th_ajaran"))->table("du")
                   ->join("mhsdaft", "mhsdaft.NPM", "=","du.NPM")
                   ->where("NIM", $nim)->first();

        //cek administrasi
        $bulan          = Date("m");
        $mhs_gel        = substr($select0["GEL"],0,1);
        $conv_bulan     = $this->bulan_berjalan($mhs_gel, $bulan);
        $totangsuran    = DB::connection("db".session("th_ajaran"))->table("tb_angsur")
                          ->where("NO_ANS", "<=",$conv_bulan)
                          ->where("KD_JURUSAN", $select0["KD_JURUSAN"])
                          ->where("GEL", $mhs_gel)
                          ->sum("ANGSURAN");

        //lihat di tbhasil
        $mhsdiajukan = DB::connection("db".session("th_ajaran"))->table("tb_hasil")->where("NIM", $nim)->first();
        // dd($mhsdiajukan["LUNAS"]);

        if(substr(session("th_ajaran"), -3) == "roi"){
            // $totangsuran = $totangsuran;

            if($mhsdiajukan["LUNAS"] == "Ya"){
                $totangsuran = $totangsuran - 500000;
            }
            
        }else{
            if($mhsdiajukan["LUNAS"] == "Ya"){
                $totangsuran = $mhsdiajukan["BYR_BERSIH"];
            }
            if($mhsdiajukan["BEASISWA"] == "Ya"){
                $potBeasiswa = $mhsdiajukan["HARGA"] * (20/100);
                $totangsuran = $totangsuran - $potBeasiswa;
            }
        }

        $totbayar       = DB::connection("db".session("th_ajaran"))->table("tb_angsuran")
                          ->where("NIM", $select0["NIM"])->sum("JUMLAH");
        
                        

        if($totbayar < $totangsuran){
            $return = 0;
        }

        $return = [
            "return"           => $return,
            "totalangsuran"    => $totangsuran,
            "totalbayar"       => $totbayar
        ];

        return $return;
    }

    public function cek_absensi($nim){
        $alpaijin       = DB::connection("db".session('th_ajaran'))
                        ->select("SELECT Sum(`alpha`) AS `alpha` , Sum(`ijin`) AS `ijin` , Sum(`sakit`) AS `sakit` FROM `tbabsensi` WHERE `tbabsensi`.`NIM` ='".$nim."'");
        $datapersen     = DB::connection("db".session("th_ajaran"))
                        ->select("SELECT `tb_jurusan`.`tot_pert` FROM `mhsdaft` INNER JOIN `tb_jurusan` ON `mhsdaft`.`KD_JURUSAN` = `tb_jurusan`.`KD_JURUSAN` INNER JOIN `du` ON `du`.`NPM` = `mhsdaft`.`NPM` WHERE `du`.`NIM` = '".$nim."'");
        $absensi        = DB::connection("db".session("th_ajaran"))
                        ->select("SELECT `tbabsensi` . * , `tbmatkul`.`matakuliah` FROM `tbabsensi` INNER JOIN `tbmatkul` ON `tbabsensi`.`kd_matkul` = `tbmatkul`.`kd_matkul` WHERE `tbabsensi`.`NIM` ='".$nim."'");
        $total_absensi  = $alpaijin[0]['alpha']+$alpaijin[0]['ijin'];
        $total_pert     = $datapersen[0]["tot_pert"];
        $persen         = ($total_absensi/$total_pert)*100;
        $persen         = intval($persen);
        return $persen;
    }

    public function cek_nilai($nim){
        $kd_jurusan = substr($nim, 5, 1);

        // memanggil matkul syarat ojt
        $matkulSyarat = matkulojt::where("kd_jurusan", $kd_jurusan)->get();
        
        foreach ($matkulSyarat as $k) {
            $nilai = 0;
            $ceknilai = tbnilai::where("kd_matkul", $k->kd_matkul)->where("NIM", $nim);
            if($ceknilai->count() > 0){
                $ceknilai = $ceknilai->first();
                $lihatnilai = $ceknilai->nilaiakhir;
                if($lihatnilai < 60){
                    $nilai = 1;
                    break;
                }
            }else{
                $nilai = 1;
                break;
            }
        }

        
        return $nilai;
    }

    public function kebijakan_admin($nim){
        $now = date("Y-m-d");
        $getdatadu = du::where("NIM", $nim)->first();
        $npm = $getdatadu->NPM; 
        $cekkebijakan = kebijakan::where("NPM", $npm)
                        ->where("bts_kebijakan", ">", $now)
                        ->where("jenis_kebijakan", 1)->count();
        return $cekkebijakan;
    }

    public function kebijakan_nilai($nim){
        $now = date("Y-m-d");
        $getdatadu = du::where("NIM", $nim)->first();
        $npm = $getdatadu->NPM; 
        $cekkebijakan = kebijakan::where("NPM", $npm)
                        ->where("bts_kebijakan", ">", $now)
                        ->where("jenis_kebijakan", 2)->count();
        return $cekkebijakan;
    }

    public function kebijakan_absen($nim){
        $now = date("Y-m-d");
        $getdatadu = du::where("NIM", $nim)->first();
        $npm = $getdatadu->NPM; 
        $cekkebijakan = kebijakan::where("NPM", $npm)
                        ->where("bts_kebijakan", ">", $now)
                        ->where("jenis_kebijakan", 3)->count();
        return $cekkebijakan;
    }

    public function cekSyaratPengajuanByNokelompok($no_kelompok){
        $sts_absen      = 0;
        $sts_nilai      = 0;
        $sts_adminis    = 0;
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "mhsdaft.GEL_DAFTAR")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();
        for($i = 0; $i < count($ojt);$i++){
            //absensi
            $persen = $this->cek_absensi($ojt[$i]["NIM"]);
            $kebijakan_absensi = $this->kebijakan_absen($ojt[$i]["NIM"]);
            if($persen < 30){
                $sts_absen = 1;
            }
            if($kebijakan_absensi == 1){
                $sts_absen = 1;
            }
            //adminis
            $sts_adminis = $this->cek_administrasi($ojt[$i]["NIM"]);
            $kebijakan_adminis = $this->kebijakan_admin($ojt[$i]["NIM"]);
            if($kebijakan_adminis == 1){
                $sts_adminis = 1;
            }

            //nilai
            // $nilai = $this->cek_nilai($ojt[$i]["NIM"]);
            // if($nilai->count() == 0){
            //     $sts_nilai = 1;
            // }
            
            //menyusun kembali datanya
            $kelompok[] = [
                "no_kelompok"       => $ojt[$i]["no_kelompok"],
                "nama_perusahaan"   => $ojt[$i]["nama_perusahaan"],
                "alamat"            => $ojt[$i]["alamat"],
                "NIM"               => $ojt[$i]["NIM"],
                "NAMA"              => $ojt[$i]["NAMA"],
                "KELAS"             => $ojt[$i]["KELAS"],
                "persen"            => $persen,
                "sts_absensi"       => $sts_absen,
                "sts_adminis"       => $sts_adminis
            ];
        }
        return $kelompok;
    }

    public function cekSyaratPemberangkatanByNokelompok($no_kelompok){
        $sts_absen      = 0;
        $sts_nilai      = 0;
        $sts_adminis    = 0;
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "mhsdaft.GEL_DAFTAR")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();

        for($i = 0; $i < count($ojt);$i++){
            //absensi
            $persen = $this->cek_absensi($ojt[$i]["NIM"]);
            $kebijakan_absensi = $this->kebijakan_absen($ojt[$i]["NIM"]);
            if($persen < 30){
                $sts_absen = 1;
            }
            if($kebijakan_absensi == 1){
                $sts_absen = 1;
            }
            //adminis
            $sts_adminis = $this->cek_administrasi($ojt[$i]["NIM"]);
            // dd($sts_adminis);
            $kebijakan_adminis = $this->kebijakan_admin($ojt[$i]["NIM"]);
            if($kebijakan_adminis == 1){
                $sts_adminis = 1;
            }
            
            //nilai
            $nilai = $this->cek_nilai($ojt[$i]["NIM"]);
            if($nilai == 0){
                $sts_nilai = 1;
            }
            if($nilai == 1){
                $sts_nilai = 0;
            }
            
            $kebijakan_nilai = $this->kebijakan_nilai($ojt[$i]["NIM"]);
            if($kebijakan_nilai == 1){
                $sts_nilai = 1;
            }
            
            //menyusun kembali datanya
            $kelompok[] = [
                "no_kelompok"       => $ojt[$i]["no_kelompok"],
                "nama_perusahaan"   => $ojt[$i]["nama_perusahaan"],
                "alamat"            => $ojt[$i]["alamat"],
                "NIM"               => $ojt[$i]["NIM"],
                "NAMA"              => $ojt[$i]["NAMA"],
                "KELAS"             => $ojt[$i]["KELAS"],
                "persen"            => $persen,
                "sts_absensi"       => $sts_absen,
                "sts_adminis"       => $sts_adminis,
                "sts_nilai"         => $sts_nilai
            ];

            
        }
        return $kelompok;
        
    }

    public function sinkron($nim){
        $result = tb_sinkad::all();
        return $result;
    }

    public function nokwitansi($kdcabang, $title){
        $tahun = date("y");
        if($title=="Wearnes"){
            $halfnokw = "W".$tahun.$kdcabang;
        }else{
            $halfnokw = "R".$tahun.$kdcabang;
        }

        //cek di angsuranmaba;
        $cekAngsuranMaba = angsuranmaba::where("no_kwitansi", "like", "{$halfnokw}%");
        if($cekAngsuranMaba->count() < 1){
            $nokwTerakhir1 = 0;
        }else{
            $kwitansi       = $cekAngsuranMaba->orderBy("id","desc")->first();
            $nokwTerakhir1  = intval(substr($kwitansi->no_kwitansi, -4));
        }

        if($nokwTerakhir1 == 0){
            $nokwFinal = 1;
        }else{
            $nokwFinal = $nokwTerakhir1 + 1;
        }

        $nokwFinal = substr("0000".$nokwFinal, -4);
        $nokw = $halfnokw.$nokwFinal;
        
        return $nokw;
    }

    

}
