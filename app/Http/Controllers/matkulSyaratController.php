<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

//models
use App\Models\tb_jurusan;
use App\Models\matkul;
use App\Models\matkulojt;

//additional
use DB;

class matkulSyaratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $url1;
    public function __construct(){
        $this->url1 = request()->segment(1);
    }
    public function index()
    {
        
        $o_matkulojt = matkulojt::with("getmatkul")->get();
        $kd_matkul = [];
        foreach ($o_matkulojt as $k) {
            $kd_matkul[] = $k['kd_matkul'];
        }

        $tbmatkul = matkul::whereNotIn("kd_matkul", $kd_matkul)->get();
        $result = [
            "tb_jurusan"    => tb_jurusan::all(),
            "tbmatkul"      => $tbmatkul,
            "matkulojt"     => $o_matkulojt
        ];
        return view("{$this->url1}_page.matkulsyarat.index", $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        $data = $req->data;

        for ($i=0; $i < count($data); $i++) { 
            $Arrdata = explode("/", $data[$i]);
            $kd_jurusan = $Arrdata[0];
            $kd_matkul = $Arrdata[1];
            matkulojt::insert([
                "kd_jurusan"    => $kd_jurusan,
                "kd_matkul"     => $kd_matkul
            ]);
        }

        return $kd_matkul;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        //
        $data = $req->data;

        for ($i=0; $i < count($data); $i++) { 
            $Arrdata = explode("/", $data[$i]);
            $kd_matkul = $Arrdata[1];
            matkulojt::where("kd_matkul",$kd_matkul)->delete();
        }

        return $kd_matkul;
    }
}
