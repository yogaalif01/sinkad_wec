<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//model
use App\Models\tb_jurusan;
use App\Models\perusahaan;
use App\Models\kelompokojt;
use App\Models\pegawai;

//additional
use DB;
use PDF;
use App\Http\Requests;

class mhsControler extends Controller
{
    
    
    public function index(){
        $cekjadwalECC = DB::connection("db".session("th_ajaran"))->table("ecc_jadwal")->where("status", "1")->get();
        if(count($cekjadwalECC) > 0){
            session()->put("ecc",1);
        }else{
            session()->put("ecc",0);
        }
        $datamhs = DB::connection("db".session("th_ajaran"))
                    ->select("select * from mhsdaft inner join du on mhsdaft.NPM = du.NPM where du.NIM = '".session("nim")."'");
        $datamhs2 = DB::connection("db".session("th_ajaran"))->table("asuinkad")->where("nim",session("nim"))->get();
        // dd($datamhs);
        session()->put("nama_mhs", $datamhs[0]["NAMA"]);
        session()->put("nama_mhs2", $datamhs2[0]["nama"]);
        session()->put("kelas", $datamhs[0]["KELAS"]);
        $jml_matkul = DB::connection("db".session("th_ajaran"))->table("tbmatkul")->where("kd_jurusan",$datamhs[0]["KD_JURUSAN"])->count();
        $sudah      = DB::connection("db".session("th_ajaran"))->table("tbnilai")->where("NIM", session('nim'))->count();
        $lulus      = DB::connection("db".session("th_ajaran"))->table("tbnilai")->where("NIM", session('nim'))->where("nilaiakhir",">=",60)->count();
        $ulang      = DB::connection("db".session("th_ajaran"))->table("tbnilai")->where("NIM", session('nim'))->where("nilaiakhir","<",60)->count();
        // $datanilai  = DB::connection("db".session("th_ajaran"))
        //                 ->select("select * from tbnilai inner join tbmatkul on tbnilai.kd_matkul = tbmatkul.kd_matkul where tbnilai.NIM = '".session('nim')."'");
        
        $jurusan = substr(session('nim'), 5, 1);
        $matkul = DB::connection('db'.session('th_ajaran'))->table('tbmatkul')->where('kd_jurusan', $jurusan)->where('dicetak', 1)->where('matakuliah','<>','UJIAN KOMPREHENSIF')->where("matakuliah",'<>','NILAI OJT')->where("matakuliah",'<>','IAMW');
        $i = 0;
        $row = '';
        foreach ($matkul->get() as $drow) {
            $nilai = DB::connection("db".session('th_ajaran'))->table('tbnilai')->where('kd_matkul', $drow['kd_matkul'])->where('nim', session('nim'))->get();
            $cekpol = DB::connection("db".session('th_ajaran'))->table('polling_dosen')->where('kd_matkul', $drow['kd_matkul'])->where('nim', session('nim'))->count();
            $cekpol2 = DB::connection("db".session('th_ajaran'))->table('polling_ast')->where('nim', session('nim'))->count();
            if($nilai){
                if($nilai[0]['nilaiakhir'] < 60){
                    $row .= "<tr class='text-danger'>";
                }else{
                    $row .= "<tr class='text-success'>";
                }
                // if($cekpol > 0 and $cekpol2 > 0){
                    $row .= "<td>".$drow['matakuliah']."</td>
                        <td>".$drow['kategori']."</td>
                        <td>".$nilai[0]['nilaiutama']."</td>
                        <td>".$nilai[0]['nilaiulang']."</td>
                        <td>".$nilai[0]['nilaibersama']."</td>
                        <td>".$nilai[0]['nilaiakhir']."</td>
                    </tr>";
                // }else{
                //     $row .= "<td>".$drow['matakuliah']."</td>
                //         <td colspan='5'>Isilah polling <b>dosen & asisten dosen</b> terlebih dulu untuk memunculkan nilai. <b><a href='http://akademik/polling' target='_blank'> Klik disini</a></b></td>
                //     </tr>";
                // }
            }else{
                $row .= "<tr>
                    <td>".$drow['matakuliah']."</td>
                    <td>".$drow['kategori']."</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                </tr>";
            }
        }

        
        $data = [
            "jml_matkul" => $jml_matkul,
            "sudah"      => $sudah,
            "lulus"      => $lulus,
            "ulang"      => $ulang,
            "datanilai"  => $row
        ];
        return view("mhs_page.index", $data);
    }

    public function angsuran(){
        $cekjadwalECC = DB::connection("db".session("th_ajaran"))->table("ecc_jadwal")->where("status", "1")->get();
        if(count($cekjadwalECC) > 0){
            session()->put("ecc",1);
        }else{
            session()->put("ecc",0);
        }
        $angsuran = DB::connection("db".session("th_ajaran"))->table("tb_hasil")->where("NIM", session("nim"))->first();
        $detailangsur = DB::connection("db".session("th_ajaran"))->table("tb_angsuran")->where("NIM", session("nim"));
        // dd($angsuran);
        $data = [
            "angsuran"  => $angsuran,
            "detail"    => $detailangsur
        ];
        return view("mhs_page.angsuran", $data);
    }

    public function absensi(){
        $cekjadwalECC = DB::connection("db".session("th_ajaran"))->table("ecc_jadwal")->where("status", "1")->get();
        if(count($cekjadwalECC) > 0){
            session()->put("ecc",1);
        }else{
            session()->put("ecc",0);
        }
        $alpaijin = DB::connection("db".session('th_ajaran'))
                        ->select("SELECT Sum(`alpha`) AS `alpha` , Sum(`ijin`) AS `ijin` , Sum(`sakit`) AS `sakit` FROM `tbabsensi` WHERE `tbabsensi`.`NIM` ='".session('nim')."'");
        $datapersen = DB::connection("db".session("th_ajaran"))
                        ->select("SELECT `tb_jurusan`.`tot_pert` FROM `mhsdaft` INNER JOIN `tb_jurusan` ON `mhsdaft`.`KD_JURUSAN` = `tb_jurusan`.`KD_JURUSAN` INNER JOIN `du` ON `du`.`NPM` = `mhsdaft`.`NPM` WHERE `du`.`NIM` = '".session('nim')."'");
        $absensi    = DB::connection("db".session("th_ajaran"))
                        ->select("SELECT `tbabsensi` . * , `tbmatkul`.`matakuliah` FROM `tbabsensi` INNER JOIN `tbmatkul` ON `tbabsensi`.`kd_matkul` = `tbmatkul`.`kd_matkul` WHERE `tbabsensi`.`NIM` ='".session('nim')."'");
        $total_absensi = $alpaijin[0]['alpha']+$alpaijin[0]['ijin'];
        $total_pert =   $datapersen[0]["tot_pert"];
        $persen=($total_absensi/$total_pert)*100;
        $persen=round($persen, 2);
        
        $data = [
            "persen"    =>$persen,
            "absensi"   => $absensi
        ];
        return view("mhs_page.absensi", $data);
    }

    public function pengaturan(){
        $cekjadwalECC = DB::connection("db".session("th_ajaran"))->table("ecc_jadwal")->where("status", "1")->get();
        if(count($cekjadwalECC) > 0){
            session()->put("ecc",1);
        }else{
            session()->put("ecc",0);
        }
        $profil = DB::connection("db".session("th_ajaran"))
                ->select("SELECT`du`.`NIM`, du.KELAS, `mhsdaft`.`NAMA`, `mhsdaft`.`TMP_LAHIR`, `mhsdaft`.`TGL_LAHIR`, `mhsdaft`.`ALAMAT1`, `mhsdaft`.`ALAMAT2`, `mhsdaft`.`TELEPON`,`mhsdaft`.`GEL_DAFTAR`, `tb_jurusan`.`jurusan` FROM `du` INNER JOIN `mhsdaft` ON `du`.`NPM` = `mhsdaft`.`NPM` INNER JOIN `tb_jurusan` ON `mhsdaft`.`KD_JURUSAN` = `tb_jurusan`.`KD_JURUSAN`where du.NIM = '".session('nim')."'");
        $data = [
            "profil"    => $profil
        ];
        return view("mhs_page.pengaturan", $data);
    }

    public function uploadKeg(){
        $cekjadwalECC = DB::connection("db".session("th_ajaran"))->table("ecc_jadwal")->where("status", "1")->get();
        if(count($cekjadwalECC) > 0){
            session()->put("ecc",1);
        }else{
            session()->put("ecc",0);
        }
        return view("mhs_page.uploadKegiatan");
    }

    public function gantipwd(Request $req){
        $pwdlama = md5("@masgall".$req->pwdlama);
        session()->put("pwdlama", $pwdlama);
        $nim = session('nim');
        $cekpwd = DB::connection("db".session("th_ajaran"))->table("asuinkad")->where("nim", $nim)->where("pass", $pwdlama)->get();
        $result = 0;
        if($cekpwd){
            $result = 1;
        } 
        return $result;
    }

    public function gantipwd2(Request $req){
        $pwdbaru    = md5("@masgall".$req->pwdBaru);
        $pwdlama    = session("pwdlama");
        $nim        = session("nim");
        $result     = 0;
        try {
            $update = DB::connection("db".session('th_ajaran'))->table("asuinkad")
                    ->where("NIM", $nim)->where("pass", $pwdlama)
                    ->update(["pass" => $pwdbaru]);
            $result = 1;
        } catch (exception $e) {
            //throw $th;
            $result = 0; 
        }
        return $result;
    }

    public function nilaikelas(){
        $cekjadwalECC = DB::connection("db".session("th_ajaran"))->table("ecc_jadwal")->where("status", "1")->get();
        if(count($cekjadwalECC) > 0){
            session()->put("ecc",1);
        }else{
            session()->put("ecc",0);
        }
        $jurusan = substr(session('nim'), 5, 1);
        if($jurusan < 6 || $jurusan <> 0){
            $djurusan = DB::connection("db".session('th_ajaran'))->table('tbjurusan')->where('kd_jurusan','<=', 6)->where('kd_jurusan','<>',0);
        }else{

        }
        
        $data = [
            'jurusan' => $djurusan
        ];
        return view('mhs_page.nilaikelas', $data);
    }

    public function cariKelasByJurusan($idjurusan){
        $kelas = DB::connection("db". session('th_ajaran'))
        ->select("SELECT `du`.`KELAS` FROM `du` INNER JOIN `mhsdaft` ON `du`.`NPM` = `mhsdaft`.`NPM`  WHERE `mhsdaft`.`KD_JURUSAN` = '".$idjurusan."' GROUP BY `du`.`KELAS` ORDER BY `du`.`KELAS`");

        $matkul = DB::connection("db". session('th_ajaran'))
        ->select("SELECT kd_matkul, matakuliah from tbmatkul where kd_jurusan = '{$idjurusan}' and matakuliah <> 'UJIAN KOMPREHENSIF' and matakuliah <> 'NILAI OJT' and matakuliah <> 'IAMW'");
        
        
        $slc_kelas = "<select name='slcKelas' id='slcKelas' class='form-control'>";
        $slc_kelas .= "<option value='' disabled selected>[ pilih ]</option>";
        foreach($kelas as $kelas){
            if($kelas[0] <> ''){
                $slc_kelas .= "<option value='{$kelas[0]}'>{$kelas[0]}</option>"; 
            }
        }
        $slc_kelas .= "</select>";

        $slc_matkul = "<select name='slcMatkul' id='slcMatkul' class='form-control'>";
        $slc_matkul .= "<option value='' disabled selected>[ pilih ]</option>";
        foreach($matkul as $matkul){
            $slc_matkul .= "<option value='{$matkul[0]}'>{$matkul[1]}</option>"; 
        }
        $slc_matkul .= "</select>";


        $return = [
            'kelas'     => $slc_kelas,
            'matkul'    => $slc_matkul
        ];
        return json_encode($return);
    }

    public function nilaiperkelas(Request $req){
        $kd_jurusan = $req->jurusan;
        $kd_matkul  = $req->matkul;
        $kelas      = $req->kelas;
        $nilai = DB::connection("db". session('th_ajaran'))
        ->select("SELECT `du`.`NIM`, `mhsdaft`.`NAMA`, `tbnilai`.`nilaiakhir` as na, `tbnilai`.`nilaiulang` as nu, `tbnilai`.`nilaibersama` as nb FROM `du` INNER JOIN `mhsdaft` ON `du`.`NPM` = `mhsdaft`.`NPM` INNER JOIN `tbnilai` ON `du`.`NIM` = `tbnilai`.`NIM` INNER JOIN
        `tbjurusan` ON `mhsdaft`.`KD_JURUSAN` = `tbjurusan`.`kd_jurusan` WHERE `tbjurusan`.`kd_jurusan` = {$kd_jurusan} AND `tbnilai`.`kd_matkul` = '{$kd_matkul}' AND `du`.`KELAS` = '{$kelas}'");
        
        $jml_data = count($nilai);
        $tbnilai = "";
        $ratarata = "";
        $nRata = 0;
        if($jml_data < 1){
            $tbnilai .= "<div class='alert alert-danger'>Data Tidak Tersedia</div>";
            $ratarata .= "
            <div class='recent-updates card'>
                <div class='card-header'>
                    <h3 class='h4'></h3>
                </div>
                <div class='card-body no-padding text-center' style='font-size:1em;margin:20px;'>
                    <div class='alert alert-danger'>Data Tidak Tersedia</div>
                </div>
            </div>";
        }else{
        $tbnilai .= "
        <div class='card-body'>
        <div class='table-responsive'>                       
        <table class='table table-striped table-hover'>
            <thead>
            <tr>
                <th>#</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>N. Ulang</th>
                <th>N. Bersama</th>
                <th>N. Akhir</th>
            </tr>
            </thead>
            <tbody>";
        $no = 1;
        foreach($nilai as $nilai){
            if($nilai[2] < 60){
                $tbnilai .= "<tr class='text-danger'>";
            }else{
                $tbnilai .= "<tr>";
            }
            $tbnilai .= "
                <th>{$no}</th>
                <th>{$nilai[0]}</th>
                <th>{$nilai[1]}</th>
                <th>{$nilai[3]}</th>
                <th>{$nilai[4]}</th>
                <th>{$nilai[2]}</th>
            </tr>
            ";
            $no++;
            $nRata = $nRata + $nilai[2];
        }      
        $tbnilai .= "</tbody>
        </table>
        ";
        $nRata = round($nRata/$no,2);
        $ratarata .= "
            <div class='recent-updates card'>
                <div class='card-header'>
                    <h3 class='h4'>Rata-rata</h3>
                </div>
                <div class='card-body no-padding text-center' style='font-size:2em;margin:20px;'>
                        {$nRata}
                </div>
            </div>";
        }
        $result = [
            "tbnilai"   => $tbnilai,
            "ratarata"  => $ratarata
        ];
        
        return json_encode($result);
    }

    public function pengajuanPage(){
        $jml_pengajuan = 0;
        // dd(session("th_ajaran"));
        $cekjadwalECC = DB::connection("db".session("th_ajaran"))->table("ecc_jadwal")->where("status", "1")->get();
        if(count($cekjadwalECC) > 0){
            session()->put("ecc",1);
        }else{
            session()->put("ecc",0);
        }

        $kelompokojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                        ->select("o_kelompokojt.*","o_detkelompokojt.*")
                        ->where("o_detkelompokojt.nim", session("nim"))
                        ->where("o_kelompokojt.apr_pengajuan","1")->get();
        // dd(count($kelompokojt));
        if(count($kelompokojt) > 0){
            return redirect()->to("/mahasiswa/status kelompok.html");
        }

        $kelompokojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                        ->select("o_kelompokojt.*","o_detkelompokojt.*")
                        ->where("o_detkelompokojt.nim", session("nim"))
                        ->where("o_kelompokojt.sts_kelompok","proses pengajuan");
        
        $kelompokojtx = $kelompokojt->orderBy("o_kelompokojt.no_kelompok","DESC")->get();
        $no_kelompok = count($kelompokojtx) == 0 ? "0" : $kelompokojtx[0]['no_kelompok'];  
        //master
        $master = kelompokojt::where("no_kelompok", $no_kelompok)->first();

        $detkelompokojt = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
            ->join("du", "du.NIM","=","o_detkelompokojt.nim")
            ->join("mhsdaft", "mhsdaft.NPM","=","du.NPM")
            ->select("du.NIM","mhsdaft.NAMA",DB::raw('substr(du.GEL, LENGTH(du.GEL), 1) as GEL_DAFTAR'),"du.KELAS","o_detkelompokojt.jabatan")
            ->where("no_kelompok", $no_kelompok)->get();

        $tbperusahaan = DB::connection("db".session("th_ajaran"))->table("tbperusahaan");

        $return = [
            "master"            => $master,
            "kelompokojt"       => $kelompokojtx,
            "jmlkelompokojt"    => count($kelompokojtx),
            "detkelompokojt"    => $detkelompokojt,
            "tbperusahaan"      => $tbperusahaan->get(),
            "bulan"             => $this->bulan
        ];
        return view("mhs_page.pengajuan", $return);
    }

    public function statusKelompokPage(){
        $cekjadwalECC = DB::connection("db".session("th_ajaran"))->table("ecc_jadwal")->where("status", "1")->get();
        if(count($cekjadwalECC) > 0){
            session()->put("ecc",1);
        }else{
            session()->put("ecc",0);
        }

        $kelompokojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("tbperusahaan","tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                        ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                        ->where("o_detkelompokojt.nim",session("nim"))
                        ->orderBy("o_kelompokojt.id","DESC")->get();
        if(count($kelompokojt) < 1){
            return redirect()->to("mahasiswa/pengajuan.html");
        }
        
        // dd($kelompokojt);

        $result = [
            "kelompokojt"   => $kelompokojt
        ];
        if(count($kelompokojt) > 0){
            return view("mhs_page.statusKelompok", $result);
        }else{
            return redirect()->to("mahasiswa/pengajuan.html");
        }
    }

    public function pengajuan_buatkelompok(){
        // dd("test");
        $abjad = array("A", "B", "C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
        $Iabjad = array("A"=>0,"B"=>1,"C"=>2,"D"=>3,"D"=>4,"E"=>5,"F"=>6,"G"=>7,"H"=>8,"I"=>9,"J"=>10,"K"=>11,"L"=>12,"M"=>13,"N"=>14,"O"=>15,"P"=>16,"Q"=>17,"R"=>18,"S"=>19,"T"=>20,"U"=>21,"V"=>22,"W"=>23,"X"=>24,"Y"=>25,"Z"=>27);
        $nim = session()->get('nim');
        $th_ajaran = intval(substr($nim, 0, 4));
        
        //mengambil data dari tabel du dan mhsdaft
        $select0 = DB::connection("db".session("th_ajaran"))->table("du")
                   ->join("mhsdaft", "mhsdaft.NPM", "=","du.NPM")
                   ->where("NIM", $nim)->first();

        // cek administrasi
        $administrasi = $this->cek_administrasi($nim);
        //cek kebijakan administrasi
        $kebijakan_admin = $this->kebijakan_admin($nim);
        if($kebijakan_admin <> 1){
            if($administrasi <> 1){
                return redirect()->to("/mahasiswa/pengajuan+gagal.html");
            }
        }
        
        // cek absensi
        $persen = $this->cek_absensi($nim);
        //cek kebijakan absensi
        $kebijakan_absen = $this->kebijakan_absen($nim);
        if($kebijakan_absen <> 1){
            if($persen >= 30){
                return redirect()->to("/mahasiswa/pengajuan+gagal.html");
            }
        }
        
        $select1 = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
                    ->where("nim", $nim)->where("jabatan", "KETUA")->count();
        
        $select2 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                    ->where("no_kelompok","like",$th_ajaran.".".substr($select0["GEL"],-1).".%")->count();
        
        //autonumber utk nomor kelompok
        if($select1 == 0){
            if($select2 == 0){
                $nomor = $select2 + 1;
                $nomor = $th_ajaran.".".$select0['GEL'].".". $nomor .$abjad[$select1];
            }else{
                 //mencari nomor yang sesuai.
                 $select2 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                 ->where("no_kelompok","like",$th_ajaran.".".substr($select0["GEL"],-1).".%")
                 ->orderBy("id","DESC")->first();
                 $nomor = substr($select2["no_kelompok"], 7,strlen($select2["no_kelompok"])-7);
                 $nomor = substr($nomor, 0, strlen($nomor)-1);
                 $nomor = $nomor +1;
                 $dapatNomor = $th_ajaran.".".substr($select0["GEL"],-1).".". $nomor .$abjad[$select1];
                do {
                    $cekKetersediaanNomor = kelompokojt::where("no_kelompok", $dapatNomor)->get();

                    if(count($cekKetersediaanNomor) <> null)
                    {
                        $nomor++;
                        $dapatNomor = $th_ajaran.".".substr($select0["GEL"],-1).".". $nomor .$abjad[$select1];
                        $stop = 1;
                    }else{
                        $stop = 0; 
                    }
                } while ($stop == 1);
                $nomor = $dapatNomor;
            }
        }else{
            $select1 = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
            ->where("nim", $nim)->where("jabatan", "KETUA")->orderBy("no_kelompok", "DESC")->first();
            $Snomor = substr($select1["no_kelompok"],-1);
            $Snomor = $abjad[$Iabjad[$Snomor]+1];
            $Inomor = substr($select1["no_kelompok"], 7, strlen($select1["no_kelompok"])-8);
            $nomor = $th_ajaran.".".substr($select0["GEL"],-1).".".$Inomor.$Snomor;
        }
        // end of autonumber untuk nomor kelompok

        // insert ke tbl o_kelompokojt
        $insert0 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")->insert([
            "no_kelompok"   => $nomor,
            "sts_kelompok"  => "proses pengajuan"
        ]);
        $insert1 = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")->insert([
            "no_kelompok"   => $nomor,
            "nim"           => $nim,
            "jabatan"       => "KETUA"
        ]);
        // end of insert ke tbl o_kelompokojt

        return redirect()->back();   
    }

    public function pengajuanGagal(){
        return view("mhs_page.pengajuanGagal");
    }

    public function tambahAnggota($nim, $nokelompok){
        $gel        = 1;
        $jurusan    = 1;
        $absensix   = 1;
        $adminis    = 1;
        $status     = 1;
        $totbayar   = 0;
        $totangsuran = 0;

        $master     = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                    ->where("no_kelompok",   $nokelompok)->first();

        $master2     = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
                    ->where("no_kelompok",   $nokelompok)->get();

        $mhsdiajukan = DB::connection("db".session("th_ajaran"))->table("du")
                     ->join("mhsdaft", "mhsdaft.NPM","=","du.NPM")
                     ->join("tb_hasil", "tb_hasil.NPM","=","du.NPM")
                     ->select("du.NIM","mhsdaft.NAMA","du.KELAS", "du.GEL","mhsdaft.GEL_DAFTAR","mhsdaft.KD_JURUSAN","tb_hasil.LUNAS","tb_hasil.BEASISWA", "tb_hasil.BYR_BERSIH", "tb_hasil.HARGA")
                     ->where("du.NIM", $nim)->where("du.nim" ,"<>",session("nim"))->get();
        
         $cekkeanggotaan = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("o_detkelompokojt", "o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                        ->whereNotIn("o_kelompokojt.sts_kelompok",["pengajuan ditolak","pengajuan ditolak perusahaan"])
                        ->where("o_detkelompokojt.nim", $nim)->get();

       
       
        if(count($master2) <> 5){    
            if(count($cekkeanggotaan) == 0){
                if(count($mhsdiajukan) > 0){
                    $mhslogin   = DB::connection("db".session("th_ajaran"))->table("du")
                                    ->join("mhsdaft", "mhsdaft.NPM","=","du.NPM")
                                    ->select("du.NIM","mhsdaft.NAMA","du.KELAS","du.GEL","mhsdaft.GEL_DAFTAR","mhsdaft.KD_JURUSAN")
                                    ->where("du.NIM", session("nim"))->get();

                    // cek absensi
                    $persen = $this->cek_absensi($mhsdiajukan[0]["NIM"]);
                    //cek kebijakan absensi
                    $kebijakan_absen = $this->kebijakan_absen($mhsdiajukan[0]["NIM"]);
                    if($kebijakan_absen == 1){
                        // if($persen >= 20){
                            $persen = 10;
                        // }
                    }

                    // cek administrasi
                    $adminis = $this->cek_administrasi($mhsdiajukan[0]["NIM"]);
                    //cek kebijakan administrasi
                    $kebijakan_admin = $this->kebijakan_admin($mhsdiajukan[0]["NIM"]);
                    if($kebijakan_admin == 1){
                        // if($adminis <> 1){
                            $adminis    = 1;
                        // }
                    }

                
                    if(count($mhsdiajukan) > 0){
                        // cek gelombang
                        if(substr($mhsdiajukan[0]["GEL"],-1) <> substr($mhslogin[0]["GEL"], -1)){
                            $gel    = 0;
                            $status = 0;
                        }
                        // cek jurusan
                        if($mhsdiajukan[0]["KD_JURUSAN"] <> $mhslogin[0]["KD_JURUSAN"]){
                            $jurusan    = 0;
                            $status     = 0;
                        }
                        
                        // cek absensi
                        $persen = $master["sts_pencarian"] == 2 ? 20 : $persen;
                        if($persen >= 30){
                            $absensix   = 0;
                            $status     = 0;
                        }
                        
                        //cek administrasi
                        if($adminis == 0){
                            $status     = 0;
                        }

                        // dd($status);
                    }else{
                        $gel            = 0;
                        $jurusan        = 0;
                        $status         = 0;
                        $mhsdiajukan    = 0;
                    } 
                }else{
                    $gel            = 0;
                    $jurusan        = 0;
                    $absensix       = 0;
                    $adminis        = 0;
                    $status         = 0;
                    $mhsdiajukan    = 0;
                }
            }else{
                $gel            = 0;
                $jurusan        = 0;
                $absensix       = 0;
                $adminis        = 0;
                $status         = 0;
                $mhsdiajukan    = 1;
            }
        }else{
            $gel            = 0;
            $jurusan        = 0;
            $absensix       = 0;
            $adminis        = 0;
            $status         = 0;
            $mhsdiajukan    = 2;   
        }

        

        $result = [
            "gel"           => $gel,
            "jurusan"       => $jurusan,
            "absensi"       => $master["sts_pencarian"] == 2 ? 1 : $absensix,
            // "adminis"       => $master["sts_pencarian"] == 2 ? 1 : $adminis,
            "adminis"       => $adminis,
            "status"        => $status,
            "datamhs"       => $mhsdiajukan,
            "totbayar"      => $totbayar,
            "totangsuran"   => $totangsuran,
        ];


        return $result;
    }

    public function tambahAnggotaPost(Request $req){
        $insert1 = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")->insert([
            "no_kelompok"   => $req->no_kelompok,
            "nim"           => $req->txtnimadd,
            "jabatan"       => "ANGGOTA"
        ]);

        return redirect()->back()->with(["sts_pengajuan" => 1]);
    }

    public function hapusAnggota($nim, $no_kelompok){
        // dd($nim);
        $delete = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
                ->where("nim", $nim)->where("no_kelompok", $no_kelompok)->delete();
        return redirect()->back()->with(["sts_pengajuan" => 1]);
    }

    public function ubahPengajuan(Request $req){
        if($req->jenis_pengajuan != 2){
            $validate["txtkdperusahaan"]    = "required";
            $validate["jenis_pengajuan"]    = "required";
            $validate["lamaojt"]            = "required";
        }
        $validate["txtbulan"]               = "required";
        // dd($req->jenis_pengajuan);
        
        $this->validate($req,$validate,["required"  => "Tidak boleh kosong"]);

        $insert = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
        ->where("no_kelompok", $req->txtnokelompok)
        ->update([
            "kd_perusahaan" => $req->jenis_pengajuan == 2 ? 1 : $req->txtkdperusahaan,
            "bulan"         => $req->txtbulan,
            "lama"          => $req->lamaojt,
            "sts_pencarian" => $req->jenis_pengajuan
            ]);
        return redirect()->back()->with(["sts_pengajuan" => 1]);
        
    }

    public function hapusNomor($no_kelompok){
        $detkelompok    = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
                          ->where("no_kelompok", $no_kelompok);
        $hapus1         = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                          ->where("no_kelompok", $no_kelompok)->delete();
        $hapus2         = $detkelompok->delete();
        
        return redirect()->back()->with(["sts_pengajuan" => 1]);
    }

    public function tambahPerusahaan(Request $req){
        $tbperusahaan = DB::connection("db".session("th_ajaran"))->table("tbperusahaan")
                        ->orderBy("kode_perusahaan", "DESC")->first();
        $nomor = $tbperusahaan["kode_perusahaan"]+1;
        $insert = DB::connection("db".session("th_ajaran"))->table("tbperusahaan")->insert([
            "kode_perusahaan"   => $nomor,
            "nama_perusahaan"   => $req->txtnmperusahaan,
            "alamat"            => $req->txtalamatperusahaan,
            "kota"              => $req->txtkota,
            "telp"              => $req->txttelp
        ]);
        $no_kelompok = $req->no_kelompok;
        $update = kelompokojt::where("no_kelompok", $no_kelompok)->update(["kd_perusahaan" => $nomor]);
        return redirect()->back()->with(["sts_pengajuan" => 1]);
    }

    public function pengajuanCetak($no_kelompok){
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)
                ->get();    
        
        //cek kelengkapan persyaratan
        $kelompok = $this->cekSyaratPengajuanByNokelompok($no_kelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0){
                return view("mhs_page.pengajuanGagal2", ["kelompok" => $kelompok]);        
                break;
            }
        }


        $result = [
            "ojt"   => $ojt
        ];
        // dd($result);
        return view("mhs_page.pengajuanCetak", $result);
    }

    public function detailkelompok(){
        $nokelompok = $_GET["nokelompok"];
        // dd($nokelompok);

        $kelompokojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                        ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                        ->select("o_kelompokojt.*","o_detkelompokojt.*")
                        ->where("o_detkelompokojt.nim", session("nim"))
                        ->where("o_kelompokojt.no_kelompok",$nokelompok);
        
        $kelompokojtx = $kelompokojt->orderBy("o_kelompokojt.no_kelompok","DESC")->get();
        $no_kelompok = count($kelompokojtx) == 0 ? "0" : $kelompokojtx[0]['no_kelompok'];  
        // dd($no_kelompok);
        $detkelompokojt = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
            ->join("du", "du.NIM","=","o_detkelompokojt.nim")
            ->join("mhsdaft", "mhsdaft.NPM","=","du.NPM")
            ->select("du.NIM","mhsdaft.NAMA","mhsdaft.GEL_DAFTAR","du.KELAS", "du.GEL","o_detkelompokojt.jabatan")
            ->where("no_kelompok", $no_kelompok)->get();

        $tbperusahaan = DB::connection("db".session("th_ajaran"))->table("tbperusahaan");

        $return = [
            "kelompokojt"       => $kelompokojtx,
            "jmlkelompokojt"    => count($kelompokojtx),
            "detkelompokojt"    => $detkelompokojt,
            "tbperusahaan"      => $tbperusahaan->get(),
            "bulan"             => $this->bulan,
            "tbpegawai"         => pegawai::all()
        ];
        return view("mhs_page.pemberangkatanDetail", $return);
    }

    public function pemberangkatanCetak($no_kelompok){

        //cek syarat pemberangkatan
        $kelompok = $this->cekSyaratPemberangkatanByNokelompok($no_kelompok);
        for($j = 0; $j<count($kelompok);$j++){
            if($kelompok[$j]["sts_absensi"] == 0 || $kelompok[$j]["sts_adminis"] == 0 || $kelompok[$j]["sts_nilai"] == 0){
                return view("mhs_page.pemberangkatanGagal", ["kelompok" => $kelompok]);        
                break;
            }
        }
        
        $result = [
            "ojt"   => $kelompok
        ];
        return view("mhs_page.pemberangkatanCetak", $result);
    }


    public function cekPerusahaan($kdperusahaan, $nokelompok){
        $cek = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->where("kd_perusahaan", $kdperusahaan)->where("no_kelompok","<>", $nokelompok);
        
        if($cek->count() == 0){
            $pesan = "Anda yang pertama kali mengajukan di perusahaan ini.";
        }else{
            $pesan = "Sudah ada ".$cek->count()." kelompok yang mengajukan di perusahaan ini";
        }
        $dataperusahaan = perusahaan::where("kode_perusahaan", $kdperusahaan)->get(); 
        $return = [
            "pesan"             => $pesan,
            "dataperusahaan"    => $dataperusahaan
        ];
        return $return;
    }


    public function ecc(){
        $ecc_jadwal = DB::connection("db".session("th_ajaran"))->table("ecc_jadwal")->where("status", 1)->get();
        if(count($ecc_jadwal) == 0){
            return redirect()->to("/mahasiswa/index.html");
        }
        $ecc = DB::connection("db".session("th_ajaran"))->table("ecc_anggota")
        ->join("du", "du.NIM", "=", "ecc_anggota.nim")
        ->join("mhsdaft", "mhsdaft.NPM", "=", "du.NPM")
        ->join("ecc_jadwal","ecc_jadwal.id","=","ecc_anggota.id_jadwal")
        ->select("du.NIM", "du.KELAS","mhsdaft.NAMA")
        ->where("ecc_jadwal.status", 1)
        ->orderBy("ecc_anggota.noanggota", "DESC");

        $result = [
            "ecc"       => $ecc->get(),
            "jadwal"    => $ecc_jadwal
        ];
        return view('mhs_page.ecc', $result);
    }
    public function eccpost(){
        $ecc_jadwal = DB::connection("db".session("th_ajaran"))->table("ecc_jadwal")->where("status", 1)->get();
        if(count($ecc_jadwal) == 0){
            return redirect()->to("/mahasiswa/index.html");
        }

        
        $nim = session('nim');
        $data_ecc = DB::connection("db".session("th_ajaran"))->table("ecc_anggota");
        $ecc = DB::connection("db".session("th_ajaran"))->table("ecc_anggota")
        ->join("du", "du.NIM", "=", "ecc_anggota.nim")
        ->join("mhsdaft", "mhsdaft.NPM", "=", "du.NPM")
        ->join("ecc_jadwal","ecc_jadwal.id","=","ecc_anggota.id_jadwal")
        ->select("du.NIM", "du.KELAS","mhsdaft.NAMA","ecc_jadwal.tanggal")
        ->where("ecc_jadwal.status", 1);

        $no = "";
        $status = 1; 
        if($ecc->where("ecc_anggota.nim", $nim)->count() > 0){
            $status = 2;
        }else if($ecc->count() == 60){
            $status = 3;
        }else{
            if($data_ecc->count() == 0){
                $no = "ECC01";
            }else{
                $data_eccx = $data_ecc->orderBy("noanggota", "DESC")->get();
                $no = $data_eccx[0]['noanggota'];
                $no = intval(substr($no, 3, 2))+1;
                $no = "ECC".substr("00".$no, -2);
                // dd($no);
            }            
            $insert = $data_ecc->insert([
                "noanggota"     => $no,
                "nim"           => $nim,
                "id_jadwal"     => $ecc_jadwal[0]['id']
                ]);
        }

        return redirect()->back()->with([
            "status_ecc"    => $status
            ]);
    }

    public function cetakbimbingan($no_kelompok){
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "mhsdaft.GEL_DAFTAR", "o_kelompokojt.nip", "o_kelompokojt.nip2", "o_kelompokojt.sts_pencarian", "o_kelompokojt.bulan")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();

        $pembimbing = DB::connection("db".session("th_ajaran"))->table("tbpegawai")
                    ->where("NIP", $ojt[0]["nip"])->orWhere("NIP", $ojt[0]["nip2"])->orderBy("jabatan","DESC")->get();
        $result = [
            "kelompok"      => $ojt,
            "pembimbing"    => $pembimbing
        ];
        $pdf = PDF::loadView("mhs_page.berkas.formbimbingan", $result, [], [
            "format"            => "legal",
            "margin_left"       => "20",
            "margin_right"       => "20"
        ]);
        return $pdf->stream("mhs_page.berkas.formbimbingan");
    }

    public function cetakform($no_kelompok){
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "mhsdaft.GEL_DAFTAR", "o_kelompokojt.nip", "o_kelompokojt.nip2", "o_kelompokojt.sts_pencarian", "o_kelompokojt.bulan")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();
        $pembimbing = DB::connection("db".session("th_ajaran"))->table("tbpegawai")
        ->where("NIP", $ojt[0]["nip"])->Orwhere("NIP", $ojt[0]["nip"])->get();
        $pembimbing1 = DB::connection("db".session("th_ajaran"))->table("tbpegawai")
                    ->where("NIP", $ojt[0]["nip"])->get();
        $pembimbing2 = DB::connection("db".session("th_ajaran"))->table("tbpegawai")
        ->where("NIP", $ojt[0]["nip2"])->get();
        $result = [
            "kelompok"      => $ojt,
            "pembimbing"     => $pembimbing,
            "pembimbing1"    => $pembimbing1,
            "pembimbing2"    => $pembimbing2,
        ];
        $pdf = PDF::loadView("mhs_page.berkas.formujian", $result, [], [
            "format"            => [215, 250],
            "margin_left"       => "20",
            "margin_right"       => "20"
        ]);
        return $pdf->stream("mhs_page.berkas.formujian");
    }

    public function cetaklampiran($no_kelompok){
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "mhsdaft.GEL_DAFTAR", "o_kelompokojt.nip", "o_kelompokojt.nip2", "o_kelompokojt.sts_pencarian", "o_kelompokojt.bulan")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();
        $result = [
            "kelompok"      => $ojt,
        ];

        $pdf = PDF::loadView("mhs_page.berkas.lampiranformujian", $result, [], [
            "format"            => "legal",
            "margin_left"       => "12",
            "margin_right"       => "12"
        ]);
        return $pdf->stream("mhs_page.berkas.lampiranformujian");
    }

    // ------------------------- TA ------------------------------------
    public function pengajuan_buatkelompokTA(){
        $abjad = array("A", "B", "C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
        $Iabjad = array("A"=>0,"B"=>1,"C"=>2,"D"=>3,"D"=>4,"E"=>5,"F"=>6,"G"=>7,"H"=>8,"I"=>9,"J"=>10,"K"=>11,"L"=>12,"M"=>13,"N"=>14,"O"=>15,"P"=>16,"Q"=>17,"R"=>18,"S"=>19,"T"=>20,"U"=>21,"V"=>22,"W"=>23,"X"=>24,"Y"=>25,"Z"=>27);
        $nim = session()->get('nim');
        $th_ajaran = intval(substr($nim, 0, 4));
        
        //mengambil data dari tabel du dan mhsdaft
        $select0 = DB::connection("db".session("th_ajaran"))->table("du")
                   ->join("mhsdaft", "mhsdaft.NPM", "=","du.NPM")
                   ->where("NIM", $nim)->first();
              

        $select1 = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
                    ->where("nim", $nim)->where("jabatan", "KETUA")->count();
        
        $select2 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                    ->where("no_kelompok","like",$th_ajaran.".".substr($select0["GEL"],-1).".%")->count();
        
        //autonumber utk nomor kelompok
        if($select1 == 0){
            if($select2 == 0){
                $nomor = $select2 + 1;
                // $th_ajaran = Date("Y");
                $nomor = $th_ajaran.".".substr($select0["GEL"],-1).".". $nomor .$abjad[$select1];
            }else{
                $select2 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->where("no_kelompok","like",$th_ajaran.".".substr($select0["GEL"],-1).".%")
                ->orderBy("id","DESC")->first();
                $nomor = substr($select2["no_kelompok"], 7,strlen($select2["no_kelompok"])-7);
                $nomor = substr($nomor, 0, strlen($nomor)-1);
                $nomor = $nomor +1;
                // $th_ajaran = Date("Y");
                $nomor = $th_ajaran.".".substr($select0["GEL"],-1).".". $nomor .$abjad[$select1];
            }
        }else{
            $select1 = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")
            ->where("nim", $nim)->where("jabatan", "KETUA")->orderBy("no_kelompok", "DESC")->first();
            $Snomor = substr($select1["no_kelompok"],-1);
            $Snomor = $abjad[$Iabjad[$Snomor]+1];
            $Inomor = substr($select1["no_kelompok"], 7,strlen($select1["no_kelompok"])-7);
            $Inomor = substr($Inomor, 0, strlen($Inomor)-1);
            // $th_ajaran = Date("Y");
            $nomor = $th_ajaran.".".substr($select0["GEL"],-1).".".$Inomor.$Snomor;
        }
        // end of autonumber untuk nomor kelompok

        // insert ke tbl o_kelompokojt
        $insert0 = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")->insert([
            "no_kelompok"   => $nomor,
            "sts_kelompok"  => "proses pengajuan",
            "kd_perusahaan" => 1,
            "sts_pencarian" => 2
        ]);
        $insert1 = DB::connection("db".session("th_ajaran"))->table("o_detkelompokojt")->insert([
            "no_kelompok"   => $nomor,
            "nim"           => $nim,
            "jabatan"       => "KETUA"
        ]);
        // end of insert ke tbl o_kelompokojt

        return redirect()->back();   
    }

    public function downloadProposal($no_kelompok){
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->join("tb_jurusan", "tb_jurusan.KD_JURUSAN","=","mhsdaft.KD_JURUSAN")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "tb_jurusan.jurusan", "mhsdaft.GEL_DAFTAR", "o_kelompokojt.nip", "o_kelompokojt.nip2", "o_kelompokojt.sts_pencarian", "o_kelompokojt.bulan", "o_kelompokojt.lama")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();
        
       
        $result = [
            "kelompok"      => $ojt,
            "jurusan"       => tb_jurusan::all()
        ];
        $pdf = PDF::loadView("mhs_page.berkas.proposal", $result, [], [
            "format"            => "a4",
            "margin_left"       => "15",
            "margin_right"      => "18",
            "margin_top"        => "25",
            "margin_bottom"     => "20",
            'default_font_size'    => '12',
            'default_font'         => 'arial',
        ]);
        return $pdf->stream("proposal.pdf");
    }

    public function downloadProposalRoi($no_kelompok){
        $ojt = DB::connection("db".session("th_ajaran"))->table("o_kelompokojt")
                ->join("tbperusahaan", "tbperusahaan.kode_perusahaan","=","o_kelompokojt.kd_perusahaan")
                ->join("o_detkelompokojt","o_detkelompokojt.no_kelompok","=","o_kelompokojt.no_kelompok")
                ->join("du","du.NIM","=","o_detkelompokojt.nim")
                ->join("mhsdaft","mhsdaft.NPM","=","du.NPM")
                ->join("tb_jurusan", "tb_jurusan.KD_JURUSAN","=","mhsdaft.KD_JURUSAN")
                ->select("o_kelompokojt.no_kelompok","tbperusahaan.nama_perusahaan","tbperusahaan.alamat","du.NIM","mhsdaft.NAMA","du.KELAS","mhsdaft.KD_JURUSAN", "tb_jurusan.jurusan", "mhsdaft.GEL_DAFTAR", "o_kelompokojt.nip", "o_kelompokojt.nip2", "o_kelompokojt.sts_pencarian", "o_kelompokojt.bulan", "o_kelompokojt.lama")
                ->where("o_kelompokojt.no_kelompok",$no_kelompok)->get();
        
       
        $result = [
            "kelompok"      => $ojt,
            "jurusan"       => tb_jurusan::all()
        ];
        $pdf = PDF::loadView("mhs_page.berkas.proposal-roi", $result, [], [
            "format"            => "a4",
            "margin_left"       => "15",
            "margin_right"      => "18",
            "margin_top"        => "25",
            "margin_bottom"     => "20",
            'default_font_size'    => '12',
            'default_font'         => 'arial',
        ]);
        return $pdf->stream("proposal.pdf");
    }
}
